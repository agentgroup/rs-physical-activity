/**
 * Created by sm (stefano.mariani@unimore.it) on Mar 13, 2019 3:27:11 PM
 */
package eu.connecare.rs.spurious;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class QuickTImeZone {

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(
                OffsetDateTime.now(ZoneId.ofOffset("", ZoneOffset.ofHours(1))) // TODO configurable per clinical site
                        .minusHours(1).withNano(0).format(DateTimeFormatter // FIXME .minusHours(1) is workaround for time zones issue
                                .ofPattern("uuuu-MM-dd'T'HH:mm:ssxx")));
    }

}
