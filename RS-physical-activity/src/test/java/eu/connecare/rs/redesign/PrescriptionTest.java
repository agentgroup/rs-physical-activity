/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 16, 2018 9:56:48 AM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class PrescriptionTest {

    private UUID id1;
    private UUID id2;
    private UUID id3;
    private ClinicalEvent p1;
    private ClinicalEvent p2;
    private ClinicalEvent p3;
    private UUID patId1;
    private UUID patId2;
    private UUID patId3;
    private Patient pat1;
    private Patient pat2;
    private Patient pat3;
    private UUID cId1;
    private UUID cId2;
    private UUID cId3;
    private Clinician c1;
    private Clinician c2;
    private Clinician c3;
    private LocalDate sd1;
    private LocalDate sd2;
    private LocalDate sd3;
    private LocalDate dd1;
    private LocalDate dd2;
    private LocalDate dd3;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> m1;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> m2;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> m3;
    private ActivityMetric am1;
    private ActivityMetric am2;
    private ActivityMetric am3;
    private ActivityLevel<? extends Comparable<?>> al1;
    private ActivityLevel<? extends Comparable<?>> al2;
    private ActivityLevel<? extends Comparable<?>> al3;

    /**
     */
    @Before
    public void setUp() {
        this.id1 = UUID.randomUUID();
        this.patId1 = UUID.randomUUID();
        this.pat1 = new Patient(this.patId1);
        this.cId1 = UUID.randomUUID();
        this.c1 = new Clinician(this.cId1);
        this.sd1 = LocalDate.now();
        this.dd1 = LocalDate.now().plusWeeks(1);
        this.m1 = new HashMap<>();
        this.am1 = new ActivityMetric("physical", "steps");
        this.al1 = new DoubleLevel(1000.0);
        this.m1.put(this.am1, this.al1);
        this.am2 = new ActivityMetric("physical", "sedentary");
        this.al2 = new DurationLevel(Duration.ofHours(3));
        this.m1.put(this.am2, this.al2);
        this.p1 = new Prescription(this.id1, this.pat1, this.c1, this.sd1,
                this.dd1, this.m1);
        this.id2 = UUID.randomUUID();
        this.patId2 = UUID.randomUUID();
        this.pat2 = new Patient(this.patId2);
        this.cId2 = UUID.randomUUID();
        this.c2 = new Clinician(this.cId2);
        this.sd2 = this.sd1.plusDays(1);
        this.dd2 = this.sd2.plusWeeks(1);
        this.m2 = new HashMap<>();
        this.m2.put(this.am1, this.al1);
        this.am3 = new ActivityMetric("sleeping", "rem");
        this.al3 = new DurationLevel(Duration.ofHours(1));
        this.m2.put(this.am3, this.al3);
        this.p2 = new Prescription(this.id2, this.pat2, this.c2, this.sd2,
                this.dd2, this.m2);
        this.id3 = UUID.randomUUID();
        this.patId3 = UUID.randomUUID();
        this.pat3 = new Patient(this.patId3);
        this.cId3 = UUID.randomUUID();
        this.c3 = new Clinician(this.cId3);
        this.sd3 = this.sd1.plusDays(2);
        this.dd3 = this.sd3.plusWeeks(1);
        this.m3 = new HashMap<>();
        this.m3.put(this.am2, this.al2);
        this.m3.put(this.am3, this.al3);
        this.p3 = new Prescription(this.id3, this.pat3, this.c3, this.sd3,
                this.dd3, this.m3);
    }

    /**
     */
    @After
    public void tearDown() {
        this.p1 = null;
        this.p2 = null;
        this.p3 = null;
        this.m1.clear();
        this.m1 = null;
        this.m2.clear();
        this.m2 = null;
        this.m3.clear();
        this.m3 = null;
    }

    /**
     * Test method for {@link eu.connecare.rs.redesign.Prescription#getUuid()}.
     */
    @Test
    public final void testGetUuid() {
        Assertions.assertThat(this.p1.getUuid()).isEqualTo(this.id1);
        Assertions.assertThat(this.p1.getUuid())
                .isNotEqualTo(this.p2.getUuid());
        Assertions.assertThat(this.p1.getUuid())
                .isNotEqualTo(this.p3.getUuid());
        Assertions.assertThat(this.p2.getUuid()).isEqualTo(this.id2);
        Assertions.assertThat(this.p2.getUuid())
                .isNotEqualTo(this.p3.getUuid());
        Assertions.assertThat(this.p3.getUuid()).isEqualTo(this.id3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Prescription#getSubject()}.
     */
    @Test
    public final void testGetSubject() {
        Assertions.assertThat(this.p1.getSubject()).isEqualTo(this.pat1);
        Assertions.assertThat(this.p1.getSubject())
                .isNotEqualTo(this.p2.getSubject());
        Assertions.assertThat(this.p1.getSubject())
                .isNotEqualTo(this.p3.getSubject());
        Assertions.assertThat(this.p2.getSubject()).isEqualTo(this.pat2);
        Assertions.assertThat(this.p2.getSubject())
                .isNotEqualTo(this.p3.getSubject());
        Assertions.assertThat(this.p3.getSubject()).isEqualTo(this.pat3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Prescription#getPrescriber()}.
     */
    @Test
    public final void testGetPrescriber() {
        Assertions.assertThat(((Prescription) this.p1).getPrescriber())
                .isEqualTo(this.c1);
        Assertions.assertThat(((Prescription) this.p1).getPrescriber())
                .isNotEqualTo(((Prescription) this.p2).getPrescriber());
        Assertions.assertThat(((Prescription) this.p1).getPrescriber())
                .isNotEqualTo(((Prescription) this.p3).getPrescriber());
        Assertions.assertThat(((Prescription) this.p2).getPrescriber())
                .isEqualTo(this.c2);
        Assertions.assertThat(((Prescription) this.p2).getPrescriber())
                .isNotEqualTo(((Prescription) this.p3).getPrescriber());
        Assertions.assertThat(((Prescription) this.p3).getPrescriber())
                .isEqualTo(this.c3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Prescription#getStartDate()}.
     */
    @Test
    public final void testGetStartDate() {
        Assertions.assertThat(((Prescription) this.p1).getStartDate())
                .isEqualTo(this.sd1);
        Assertions.assertThat(((Prescription) this.p1).getStartDate())
                .isNotEqualTo(((Prescription) this.p2).getStartDate());
        Assertions.assertThat(((Prescription) this.p1).getStartDate())
                .isNotEqualTo(((Prescription) this.p3).getStartDate());
        Assertions.assertThat(((Prescription) this.p2).getStartDate())
                .isEqualTo(this.sd2);
        Assertions.assertThat(((Prescription) this.p2).getStartDate())
                .isNotEqualTo(((Prescription) this.p3).getStartDate());
        Assertions.assertThat(((Prescription) this.p3).getStartDate())
                .isEqualTo(this.sd3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Prescription#getDueDate()}.
     */
    @Test
    public final void testGetDueDate() {
        Assertions.assertThat(((Prescription) this.p1).getDueDate())
                .isEqualTo(this.dd1);
        Assertions.assertThat(((Prescription) this.p1).getDueDate())
                .isNotEqualTo(((Prescription) this.p2).getDueDate());
        Assertions.assertThat(((Prescription) this.p1).getDueDate())
                .isNotEqualTo(((Prescription) this.p3).getDueDate());
        Assertions.assertThat(((Prescription) this.p2).getDueDate())
                .isEqualTo(this.dd2);
        Assertions.assertThat(((Prescription) this.p2).getDueDate())
                .isNotEqualTo(((Prescription) this.p3).getDueDate());
        Assertions.assertThat(((Prescription) this.p3).getDueDate())
                .isEqualTo(this.dd3);
    }

    /**
     * Test method for {@link eu.connecare.rs.redesign.Prescription#getActivityLevels()}.
     */
    @Test
    public final void testGetGoals() {
        Assertions.assertThat(this.p1.getActivityLevels()).isEqualTo(this.m1);
        Assertions.assertThat(this.p1.getActivityLevels())
                .isNotEqualTo(this.p2.getActivityLevels());
        Assertions.assertThat(this.p1.getActivityLevels())
                .isNotEqualTo(this.p3.getActivityLevels());
        Assertions.assertThat(this.p2.getActivityLevels()).isEqualTo(this.m2);
        Assertions.assertThat(this.p2.getActivityLevels())
                .isNotEqualTo(this.p3.getActivityLevels());
        Assertions.assertThat(this.p3.getActivityLevels()).isEqualTo(this.m3);
    }

}
