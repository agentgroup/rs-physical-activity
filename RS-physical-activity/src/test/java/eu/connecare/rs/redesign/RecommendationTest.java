/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 16, 2018 11:49:00 AM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import advice.connecare.rs.redesign.RecType;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class RecommendationTest {

    private RecType rt1;
    private RecType rt2;
    private RecType rt3;
    private User patient1; // this way doesn't work because a Recommendation can't target a Clinician
    private Patient patient2;
    private Patient patient3;
    private String msg1;
    private String msg2;
    private String msg3;
    private Duration d1;
    private Duration d2;
    private Duration d3;
    private ActivityMetric metric1;
    private ActivityMetric metric2;
    private ActivityMetric metric3;
    private Adherence ad1;
    private Adherence ad2;
    private Adherence ad3;
    private Map<ActivityMetric, Adherence> adherenceMap1;
    private Map<ActivityMetric, Adherence> adherenceMap2;
    private Map<ActivityMetric, Adherence> adherenceMap3;
    private ActivityLevel<?> al1;
    private ActivityLevel<?> al2;
    private ActivityLevel<?> al3;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> activityMap1; // with a simple <?> doesn't work, and it's ok
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> activityMap2;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> activityMap3;
    private IRsEvent<RecType> rec1; // RecType or FeedType; RecType in this case
    private IRsEvent<RecType> rec2;
    private IRsEvent<RecType> rec3;

    /**
     */
    @Before
    public void setUp() {
        this.rt1 = RecType.ALERT;
        this.patient1 = new Patient(UUID.randomUUID());
        this.msg1 = "msg1";
        this.d1 = Duration.ofDays(0);
        this.metric1 = new ActivityMetric("physical", "steps");
        this.metric2 = new ActivityMetric("physical", "vigorous");
        this.ad1 = new PercentageAdherence(10.0);
        this.ad2 = new PercentageAdherence(90.0);
        this.adherenceMap1 = new HashMap<>();
        this.adherenceMap1.put(this.metric1, this.ad1);
        this.adherenceMap1.put(this.metric2, this.ad2);
        this.al1 = new DoubleLevel(1000.0);
        this.al2 = new DurationLevel(Duration.ofHours(9));
        this.activityMap1 = new HashMap<>();
        this.activityMap1.put(this.metric1, this.al1);
        this.activityMap1.put(this.metric2, this.al2);
        this.rec1 = new Recommendation(this.rt1, (Patient) this.patient1,
                this.msg1, this.d1, this.adherenceMap1, this.activityMap1);
        this.rt2 = RecType.AWARD;
        this.patient2 = new Patient(UUID.randomUUID());
        this.msg2 = "msg2";
        this.d2 = Duration.ofDays(1);
        this.metric3 = new ActivityMetric("physical", "moderate");
        this.ad3 = new PercentageAdherence(50.0);
        this.adherenceMap2 = new HashMap<>();
        this.adherenceMap2.put(this.metric2, this.ad2);
        this.adherenceMap2.put(this.metric3, this.ad3);
        this.al3 = new DurationLevel(Duration.ofHours(15));
        this.activityMap2 = new HashMap<>();
        this.activityMap2.put(this.metric2, this.al2);
        this.activityMap2.put(this.metric3, this.al3);
        this.rec2 = new Recommendation(this.rt2, this.patient2, this.msg2,
                this.d2, this.adherenceMap2, this.activityMap2);
        this.rt3 = RecType.MOTIVATIONAL;
        this.patient3 = new Patient(UUID.randomUUID());
        this.msg3 = "msg3";
        this.d3 = Duration.ofDays(14);
        this.adherenceMap3 = new HashMap<>();
        this.adherenceMap3.put(this.metric1, this.ad1);
        this.adherenceMap3.put(this.metric3, this.ad3);
        this.activityMap3 = new HashMap<>();
        this.activityMap3.put(this.metric1, this.al1);
        this.activityMap3.put(this.metric3, this.al3);
        this.rec3 = new Recommendation(this.rt3, this.patient3, this.msg3,
                this.d3, this.adherenceMap3, this.activityMap3);
    }

    /**
     */
    @After
    public void tearDown() {
        this.rec1 = null;
        this.rec2 = null;
        this.rec3 = null;
        this.adherenceMap1.clear();
        this.adherenceMap2.clear();
        this.adherenceMap3.clear();
        this.adherenceMap1 = null;
        this.adherenceMap2 = null;
        this.adherenceMap3 = null;
        this.activityMap1.clear();
        this.activityMap2.clear();
        this.activityMap3.clear();
        this.activityMap1 = null;
        this.activityMap2 = null;
        this.activityMap3 = null;
    }

    /**
     * Test method for {@link eu.connecare.rs.redesign.RsEvent#getType()}.
     */
    @Test
    public final void testGetType() {
        Assertions.assertThat(this.rec1.getType()).isEqualTo(RecType.ALERT);
        Assertions.assertThat(this.rec1.getType())
                .isNotEqualTo(this.rec2.getType());
        Assertions.assertThat(this.rec1.getType())
                .isNotEqualTo(this.rec3.getType());
        Assertions.assertThat(this.rec2.getType()).isEqualTo(RecType.AWARD);
        Assertions.assertThat(this.rec2.getType())
                .isNotEqualTo(this.rec3.getType());
        Assertions.assertThat(this.rec3.getType())
                .isEqualTo(RecType.MOTIVATIONAL);
    }

    /**
     * Test method for {@link eu.connecare.rs.redesign.RsEvent#getSubject()}.
     */
    @Test
    public final void testGetSubject() {
        Assertions.assertThat(this.rec1.getSubject()).isEqualTo(this.patient1);
        Assertions.assertThat(this.rec1.getSubject())
                .isNotEqualTo(this.rec2.getSubject());
        Assertions.assertThat(this.rec1.getSubject())
                .isNotEqualTo(this.rec3.getSubject());
        Assertions.assertThat(this.rec2.getSubject()).isEqualTo(this.patient2);
        Assertions.assertThat(this.rec2.getSubject())
                .isNotEqualTo(this.rec3.getSubject());
        Assertions.assertThat(this.rec3.getSubject()).isEqualTo(this.patient3);
    }

    /**
     * Test method for {@link eu.connecare.rs.redesign.RsEvent#getText()}.
     */
    @Test
    public final void testGetText() {
        Assertions.assertThat(this.rec1.getText()).isEqualTo(this.msg1);
        Assertions.assertThat(this.rec1.getText())
                .isNotEqualTo(this.rec2.getText());
        Assertions.assertThat(this.rec1.getText())
                .isNotEqualTo(this.rec3.getText());
        Assertions.assertThat(this.rec2.getText()).isEqualTo(this.msg2);
        Assertions.assertThat(this.rec2.getText())
                .isNotEqualTo(this.rec3.getText());
        Assertions.assertThat(this.rec3.getText()).isEqualTo(this.msg3);
    }

    /**
     * Test method for {@link eu.connecare.rs.redesign.RsEvent#getExpiration()}.
     */
    @Test
    public final void testGetExpiration() {
        Assertions.assertThat(this.rec1.getExpiration()).isEqualTo(this.d1);
        Assertions.assertThat(this.rec1.getExpiration())
                .isNotEqualTo(this.rec2.getExpiration());
        Assertions.assertThat(this.rec1.getExpiration())
                .isNotEqualTo(this.rec3.getExpiration());
        Assertions.assertThat(this.rec2.getExpiration()).isEqualTo(this.d2);
        Assertions.assertThat(this.rec2.getExpiration())
                .isNotEqualTo(this.rec3.getExpiration());
        Assertions.assertThat(this.rec3.getExpiration()).isEqualTo(this.d3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.RsEvent#getAdherence(eu.connecare.rs.redesign.ActivityMetric)}.
     */
    @Test
    public final void testGetAdherence() {
        Assertions.assertThat(this.rec1.getAdherence(this.metric1))
                .isEqualTo(this.ad1);
        Assertions.assertThat(this.rec1.getAdherence(this.metric1))
                .isNotEqualTo(this.rec2.getAdherence(this.metric1)); // rec2 does NOT have metric1 available
        Assertions.assertThat(this.rec1.getAdherence(this.metric1))
                .isEqualTo(this.rec3.getAdherence(this.metric1)); // rec3 does have metric1 available
        Assertions.assertThat(this.rec2.getAdherence(this.metric2))
                .isEqualTo(this.ad2);
        Assertions.assertThat(this.rec2.getAdherence(this.metric3))
                .isEqualTo(this.rec3.getAdherence(this.metric3));
        Assertions.assertThat(this.rec3.getAdherence(this.metric3))
                .isEqualTo(this.ad3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.RsEvent#getAccomplishment(eu.connecare.rs.redesign.ActivityMetric)}.
     */
    @Test
    public final void testGetAccomplishment() {
        Assertions.assertThat(this.rec1.getAccomplishment(this.metric1))
                .isEqualTo(this.al1);
        Assertions.assertThat(this.rec1.getAccomplishment(this.metric1))
                .isNotEqualTo(this.rec2.getAccomplishment(this.metric1)); // rec2 does NOT have metric1 available
        Assertions.assertThat(this.rec1.getAccomplishment(this.metric1))
                .isEqualTo(this.rec3.getAccomplishment(this.metric1)); // rec3 does have metric1 available
        Assertions.assertThat(this.rec2.getAccomplishment(this.metric2))
                .isEqualTo(this.al2);
        Assertions.assertThat(this.rec2.getAccomplishment(this.metric3))
                .isEqualTo(this.rec3.getAccomplishment(this.metric3));
        Assertions.assertThat(this.rec3.getAccomplishment(this.metric3))
                .isEqualTo(this.al3);
    }

}
