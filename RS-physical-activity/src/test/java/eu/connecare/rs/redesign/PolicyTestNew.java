/**
 * Created by sm (stefano.mariani@unimore.it) on Feb 22, 2019 3:36:37 PM
 */
package eu.connecare.rs.redesign;

import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import advice.connecare.rs.redesign.RecType;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class PolicyTestNew {

    private Policy pol;

    /**
     */
    @BeforeEach
    void setUp() {
        this.pol = new Policy(Paths.get("sentence_templates.txt"),
                Paths.get("EN_pos.txt"), Paths.get("times.json"),
                Paths.get("activities.json"));
    }

    /**
     */
    @AfterEach
    void tearDown() {
        this.pol = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Policy#computeRecommendation(eu.connecare.rs.redesign.Prescription, java.util.Map, java.util.Map, eu.connecare.rs.redesign.Adherence, java.time.LocalDateTime)}.
     */
    @Disabled
    @Test
    final void testComputeRecommendation() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Policy#computeRecType(eu.connecare.rs.redesign.Adherence, java.time.LocalDateTime)}.
     */
//    @Test
//    final void testComputeRecType() {
//        Assertions
//                .assertThat(Policy.computeRecType(new PercentageAdherence(100),
//                        LocalDateTime.now()))
//                .isEqualTo(RecType.AWARD);
//        Assertions
//                .assertThat(Policy.computeRecType(new PercentageAdherence(100),
//                        LocalDateTime.of(LocalDate.now(), LocalTime.of(22, 1))))
//                .isEqualTo(RecType.AWARD);
//
//        Assertions
//                .assertThat(Policy.computeRecType(new PercentageAdherence(99),
//                        LocalDateTime.of(LocalDate.now(), LocalTime.of(22, 0))))
//                .isEqualTo(RecType.BLAME);
//        Assertions
//                .assertThat(Policy.computeRecType(new PercentageAdherence(99),
//                        LocalDateTime.of(LocalDate.now(), LocalTime.of(22, 1))))
//                .isEqualTo(RecType.BLAME);
//        Assertions
//                .assertThat(Policy.computeRecType(new PercentageAdherence(99),
//                        LocalDateTime.of(LocalDate.now(),
//                                LocalTime.of(21, 59))))
//                .isNotEqualTo(RecType.BLAME);
//
//        Assertions
//                .assertThat(Policy.computeRecType(new PercentageAdherence(99),
//                        LocalDateTime.of(LocalDate.now(),
//                                LocalTime.of(21, 59))))
//                .isEqualTo(RecType.MOTIVATIONAL);
//        Assertions
//                .assertThat(
//                        Policy.computeRecType(new PercentageAdherence(57.14), // optimal adherence for 16
//                                LocalDateTime.of(LocalDate.now(),
//                                        LocalTime.of(16, 0))))
//                .isEqualTo(RecType.MOTIVATIONAL);
//        Assertions
//                .assertThat(
//                        Policy.computeRecType(new PercentageAdherence(42.86), // > optimal * 0.75 (see Policy)
//                                LocalDateTime.of(LocalDate.now(),
//                                        LocalTime.of(16, 0))))
//                .isEqualTo(RecType.MOTIVATIONAL);
//        Assertions
//                .assertThat(
//                        Policy.computeRecType(new PercentageAdherence(42.84), // < optimal * 0.75 (see Policy)
//                                LocalDateTime.of(LocalDate.now(),
//                                        LocalTime.of(16, 0))))
//                .isNotEqualTo(RecType.MOTIVATIONAL);
//
//        Assertions
//                .assertThat(
//                        Policy.computeRecType(new PercentageAdherence(42.84), // < optimal * 0.75 (see Policy)
//                                LocalDateTime.of(LocalDate.now(),
//                                        LocalTime.of(16, 0))))
//                .isEqualTo(RecType.WARNING);
//        Assertions
//                .assertThat(
//                        Policy.computeRecType(new PercentageAdherence(28.58), // > optimal * 0.5 (see Policy)
//                                LocalDateTime.of(LocalDate.now(),
//                                        LocalTime.of(16, 0))))
//                .isEqualTo(RecType.WARNING);
//        Assertions
//                .assertThat(
//                        Policy.computeRecType(new PercentageAdherence(28.56), // < optimal * 0.5 (see Policy)
//                                LocalDateTime.of(LocalDate.now(),
//                                        LocalTime.of(16, 0))))
//                .isNotEqualTo(RecType.WARNING);
//
//        Assertions
//                .assertThat(
//                        Policy.computeRecType(new PercentageAdherence(28.56), // < optimal * 0.5 (see Policy)
//                                LocalDateTime.of(LocalDate.now(),
//                                        LocalTime.of(16, 0))))
//                .isEqualTo(RecType.ALERT);
//        Assertions
//                .assertThat(Policy.computeRecType(new PercentageAdherence(1),
//                        LocalDateTime.of(LocalDate.now(), LocalTime.of(16, 0))))
//                .isEqualTo(RecType.ALERT);
//    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Policy#computeFeedback()}.
     */
    @Disabled
    @Test
    final void testComputeFeedback() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Policy#shouldDeliver(advice.connecare.rs.redesign.RecType)}.
     */
    @Disabled
    @Test
    final void testShouldDeliver() {
        fail("Not yet implemented");
    }

}
