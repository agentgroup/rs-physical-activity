/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 16, 2018 11:51:27 AM
 */
package eu.connecare.rs.redesign;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class PercentageAdherenceTest {

    private Adherence a1;
    private Adherence a2;
    private Adherence a3;

    /**
     */
    @Before
    public void setUp() {
        this.a1 = new PercentageAdherence(Double.NaN);
        this.a2 = new PercentageAdherence(0.0);
        this.a3 = new PercentageAdherence(10.0);
    }

    /**
     */
    @After
    public void tearDown() {
        this.a1 = null;
        this.a2 = null;
        this.a3 = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.PercentageAdherence#getAsPercentage()}.
     */
    @Test
    public final void testGetAsPercentage() {
        Assertions.assertThat(this.a1.getAsPercentage()).isEqualTo(Double.NaN);
        Assertions.assertThat(this.a1.getAsPercentage())
                .isEqualTo(new Double(this.a1.getAsPercentage()));
        Assertions.assertThat(this.a1.getAsPercentage()).isEqualTo(
                this.a1.getAsPercentage() + this.a2.getAsPercentage());
        Assertions.assertThat(this.a1.getAsPercentage()).isEqualTo(
                this.a1.getAsPercentage() + this.a3.getAsPercentage());
        Assertions.assertThat(this.a1.getAsPercentage())
                .isNotEqualTo(this.a2.getAsPercentage());
        Assertions.assertThat(this.a1.getAsPercentage())
                .isNotEqualTo(this.a3.getAsPercentage());
        Assertions.assertThat(this.a2.getAsPercentage()).isEqualTo(0.0);
        Assertions.assertThat(this.a2.getAsPercentage())
                .isEqualTo(new Double(this.a2.getAsPercentage()));
        Assertions.assertThat(this.a2.getAsPercentage())
                .isNotEqualTo(this.a3.getAsPercentage());
        Assertions.assertThat(this.a3.getAsPercentage()).isEqualTo(10.0);
        Assertions.assertThat(this.a3.getAsPercentage())
                .isEqualTo(new Double(this.a3.getAsPercentage()));
        Assertions.assertThat(this.a3.getAsPercentage()).isEqualTo(
                this.a2.getAsPercentage() + this.a3.getAsPercentage());
    }

}
