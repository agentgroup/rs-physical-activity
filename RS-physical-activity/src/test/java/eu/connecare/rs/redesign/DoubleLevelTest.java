/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 9, 2018 12:15:09 PM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class DoubleLevelTest {

    private ActivityLevel<Double> al1;
    private ActivityLevel<Double> al2;
    private ActivityLevel<Double> al3;

    /**
     */
    @Before
    public void setUp() {
        this.al1 = new DoubleLevel(Double.NaN);
        this.al2 = new DoubleLevel(0.0);
        this.al3 = new DoubleLevel(10.0);
    }

    /**
     */
    @After
    public void tearDown() {
        this.al1 = null;
        this.al2 = null;
        this.al3 = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.DoubleLevel#accumulate(eu.connecare.rs.redesign.ActivityLevel)}.
     */
    @Test
    public final void testAccumulateActivityLevelOfDouble() {
        Assertions.assertThat(this.al1.accumulate(this.al2))
                .isEqualTo(this.al1);
        Assertions.assertThat(this.al2.accumulate(this.al3))
                .isEqualTo(this.al3);
        Assertions.assertThat(this.al2.accumulate(this.al1))
                .isEqualTo(this.al1);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.DoubleLevel#percentageDone(eu.connecare.rs.redesign.ActivityLevel)}.
     */
    @Test
    public final void testPercentageDoneActivityLevelOfDouble() {
        Assertions.assertThat(this.al1.percentageDone(this.al2))
                .isEqualTo(Double.NaN);
        Assertions.assertThat(this.al2.percentageDone(this.al3)).isEqualTo(0.0);
        Assertions.assertThat(this.al3.percentageDone(this.al2))
                .isEqualTo(Double.POSITIVE_INFINITY);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.AbstractActivityLevel#getValue()}.
     */
    @Test
    public final void testGetValue() {
        Assertions.assertThat(this.al1.getValue()).isEqualTo(Double.NaN);
        Assertions.assertThat(this.al3.getValue())
                .isEqualTo(this.al3.getValue() - this.al2.getValue());
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.DoubleLevel#redistribute(java.time.Duration, eu.connecare.rs.redesign.ActivityMetric)}.
     */
    @Test
    public final void testRedistributeActivityLevelOfDouble() {
        Assertions
                .assertThat(this.al3.redistribute(Duration.ofDays(10),
                        new ActivityMetric("physical", "steps")))
                .isEqualTo(new DoubleLevel(1.0));
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.DoubleLevel#remainingTo(eu.connecare.rs.redesign.ActivityLevel, eu.connecare.rs.redesign.ActivityMetric)}.
     */
    @Test
    public final void testRemainingToActivityLevelOfDouble() {
        Assertions
                .assertThat(this.al3.remainingTo(new DoubleLevel(20.0),
                        new ActivityMetric("physical", "steps")))
                .isEqualTo(new DoubleLevel(10.0));
    }

}
