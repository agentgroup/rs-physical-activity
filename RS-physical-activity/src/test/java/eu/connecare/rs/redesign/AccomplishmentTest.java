/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 9, 2018 12:12:10 PM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class AccomplishmentTest {

    private UUID id1;
    private UUID id2;
    private UUID id3;
    private ClinicalEvent a1;
    private ClinicalEvent a2;
    private ClinicalEvent a3;
    private UUID patId1;
    private UUID patId2;
    private UUID patId3;
    private Patient p1;
    private Patient p2;
    private Patient p3;
    private LocalDateTime d1;
    private LocalDateTime d2;
    private LocalDateTime d3;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> m1;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> m2;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> m3;
    private ActivityMetric am1;
    private ActivityMetric am2;
    private ActivityMetric am3;
    private ActivityLevel<? extends Comparable<?>> al1;
    private ActivityLevel<? extends Comparable<?>> al2;
    private ActivityLevel<? extends Comparable<?>> al3;

    /**
     */
    @Before
    public void setUp() {
        this.patId1 = UUID.randomUUID();
        this.p1 = new Patient(this.patId1);
        this.d1 = LocalDateTime.now();
        this.m1 = new HashMap<>();
        this.am1 = new ActivityMetric("physical", "steps");
        this.al1 = new DoubleLevel(1000.0);
        this.m1.put(this.am1, this.al1);
        this.am2 = new ActivityMetric("physical", "sedentary");
        this.al2 = new DurationLevel(Duration.ofHours(3));
        this.m1.put(this.am2, this.al2);
        this.id1 = UUID.randomUUID();
        this.a1 = new Accomplishment(this.id1, this.p1, this.d1, this.m1);
        this.patId2 = UUID.randomUUID();
        this.p2 = new Patient(this.patId2);
        this.d2 = this.d1.plusDays(1);
        this.m2 = new HashMap<>();
        this.m2.put(this.am1, this.al1);
        this.am3 = new ActivityMetric("sleeping", "rem");
        this.al3 = new DurationLevel(Duration.ofHours(1));
        this.m2.put(this.am3, this.al3);
        this.id2 = UUID.randomUUID();
        this.a2 = new Accomplishment(this.id2, this.p2, this.d2, this.m2);
        this.patId3 = UUID.randomUUID();
        this.p3 = new Patient(this.patId3);
        this.d3 = this.d1.plusDays(2);
        this.m3 = new HashMap<>();
        this.m3.put(this.am2, this.al2);
        this.m3.put(this.am3, this.al3);
        this.id3 = UUID.randomUUID();
        this.a3 = new Accomplishment(this.id3, this.p3, this.d3, this.m3);
    }

    /**
     */
    @After
    public void tearDown() {
        this.a1 = null;
        this.a2 = null;
        this.a3 = null;
        this.m1.clear();
        this.m1 = null;
        this.m2.clear();
        this.m2 = null;
        this.m3.clear();
        this.m3 = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Accomplishment#getUuid()}.
     */
    @Test
    public final void testGetUuid() {
        Assertions.assertThat(this.a1.getUuid()).isEqualTo(this.id1);
        Assertions.assertThat(this.a1.getUuid())
                .isNotEqualTo(this.a2.getUuid());
        Assertions.assertThat(this.a1.getUuid())
                .isNotEqualTo(this.a3.getUuid());
        Assertions.assertThat(this.a2.getUuid()).isEqualTo(this.id2);
        Assertions.assertThat(this.a2.getUuid())
                .isNotEqualTo(this.a3.getUuid());
        Assertions.assertThat(this.a3.getUuid()).isEqualTo(this.id3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Accomplishment#getSubject()}.
     */
    @Test
    public final void testGetSubject() {
        Assertions.assertThat(this.a1.getSubject()).isEqualTo(this.p1);
        Assertions.assertThat(this.a1.getSubject())
                .isNotEqualTo(this.a2.getSubject());
        Assertions.assertThat(this.a1.getSubject())
                .isNotEqualTo(this.a3.getSubject());
        Assertions.assertThat(this.a2.getSubject()).isEqualTo(this.p2);
        Assertions.assertThat(this.a2.getSubject())
                .isNotEqualTo(this.a3.getSubject());
        Assertions.assertThat(this.a3.getSubject()).isEqualTo(this.p3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Accomplishment#getDate()}.
     */
    @Test
    public final void testGetDate() {
        Assertions.assertThat(((Accomplishment) this.a1).getDate())
                .isEqualTo(this.d1);
        Assertions.assertThat(((Accomplishment) this.a1).getDate())
                .isNotEqualTo(((Accomplishment) this.a2).getDate());
        Assertions.assertThat(((Accomplishment) this.a1).getDate())
                .isNotEqualTo(((Accomplishment) this.a3).getDate());
        Assertions.assertThat(((Accomplishment) this.a2).getDate())
                .isEqualTo(this.d2);
        Assertions.assertThat(((Accomplishment) this.a2).getDate())
                .isNotEqualTo(((Accomplishment) this.a3).getDate());
        Assertions.assertThat(((Accomplishment) this.a3).getDate())
                .isEqualTo(this.d3);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Accomplishment#getActivityLevels()}.
     */
    @Test
    public final void testGetGoals() {
        Assertions.assertThat(this.a1.getActivityLevels()).isEqualTo(this.m1);
        Assertions.assertThat(this.a1.getActivityLevels())
                .isNotEqualTo(this.a2.getActivityLevels());
        Assertions.assertThat(this.a1.getActivityLevels())
                .isNotEqualTo(this.a3.getActivityLevels());
        Assertions.assertThat(this.a2.getActivityLevels()).isEqualTo(this.m2);
        Assertions.assertThat(this.a2.getActivityLevels())
                .isNotEqualTo(this.a3.getActivityLevels());
        Assertions.assertThat(this.a3.getActivityLevels()).isEqualTo(this.m3);
    }

}
