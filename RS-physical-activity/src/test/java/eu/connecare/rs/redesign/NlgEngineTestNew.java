/**
 * Created by sm (stefano.mariani@unimore.it) on Feb 22, 2019 4:15:06 PM
 */
package eu.connecare.rs.redesign;

import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Paths;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
class NlgEngineTestNew {

    private NlgEngine nlg;

    /**
     */
    @BeforeEach
    void setUp() {
        this.nlg = new NlgEngine(Paths.get("sentence_templates.txt"),
                Paths.get("EN_pos.txt"), Paths.get("times.json"),
                Paths.get("activities.json"));
    }

    /**
     */
    @AfterEach
    void tearDown() {
        this.nlg = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.NlgEngine#generateMessage(eu.connecare.rs.redesign.Prescription, java.util.Map, java.util.Map, advice.connecare.rs.redesign.RecType)}.
     */
    @Test
    final void testGenerateMessage() {
        
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.NlgEngine#findMetric(java.util.Map, java.util.function.BinaryOperator)}.
     */
    @Disabled
    @Test
    final void testFindMetric() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.NlgEngine#closestToAverageMetric(java.util.Map)}.
     */
    @Disabled
    @Test
    final void testClosestToAverageMetric() {
        fail("Not yet implemented");
    }

}
