/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 16, 2018 9:53:22 AM
 */
package eu.connecare.rs.redesign;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ PolicyTestNew.class, RsEngineTest.class, PolicyTest.class,
        NlgEngineTest.class, StrategyTest.class, RecommendationTest.class,
        PercentageAdherenceTest.class, PrescriptionTest.class,
        AccomplishmentTest.class, DoubleLevelTest.class,
        DurationLevelTest.class })
public class AllTests {

}
