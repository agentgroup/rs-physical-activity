/**
 * Created by sm (stefano.mariani@unimore.it) on Mar 9, 2018 2:22:20 PM
 */
package eu.connecare.rs.redesign;

import static org.junit.Assert.fail;

import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class RsEngineTest {

    private RsEngine rs1;
    private RsEngine rs2;
    private Prescription prescription;
    private Accomplishment accomplishment;
    private UUID pId;

    /**
     */
    @Before
    public void setUp() {
        this.rs1 = new RsEngine(new Strategy(),
                new Policy(
                        Paths.get("src/main/resources/sentence_templates.txt"),
                        Paths.get("src/main/resources/EN_pos.txt"),
                        Paths.get("src/main/resources/times.json"),
                        Paths.get("src/main/resources/activities.json")));
        this.rs2 = new RsEngine(new Strategy(),
                new Policy(
                        Paths.get("src/main/resources/sentence_templates.txt"),
                        Paths.get("src/main/resources/EN_pos.txt"),
                        Paths.get("src/main/resources/times.json"),
                        Paths.get("src/main/resources/activities.json")));
        /* */
        Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> goals = new HashMap<>();
        goals.put(new ActivityMetric("physical", "steps"),
                new DoubleLevel(10000.0));
        goals.put(new ActivityMetric("physical", "light"),
                new DurationLevel(Duration.ofHours(14)));
        goals.put(new ActivityMetric("physical", "moderate"),
                new DurationLevel(Duration.ofHours(4)));
        this.pId = UUID.randomUUID();
        this.prescription = new Prescription(this.pId,
                new Patient(UUID.randomUUID()),
                new Clinician(UUID.randomUUID()), LocalDate.now().minusDays(3),
                LocalDate.now().plusDays(4), goals);
        /* */
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> measurements = new HashMap<>();
        measurements.put(new ActivityMetric("physical", "steps"),
                new DoubleLevel(1000.0));
        measurements.put(new ActivityMetric("physical", "light"),
                new DurationLevel(Duration.ofHours(7)));
        measurements.put(new ActivityMetric("physical", "moderate"),
                new DurationLevel(Duration.ofHours(1)));
        this.accomplishment = new Accomplishment(UUID.randomUUID(),
                new Patient(UUID.randomUUID()), LocalDateTime.now(),
                measurements);
        /* */
        try {
            this.rs2.feed(this.prescription, this.accomplishment);
        } catch (UnknownActivityLevelException e) {
            // can't happen
            e.printStackTrace();
        }
    }

    /**
     */
    @After
    public void tearDown() {
        this.rs1 = null;
        this.rs2 = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.RsEngine#feed(java.util.UUID, eu.connecare.rs.redesign.Accomplishment)}.
     */
    @Test
    public final void testFeedUUIDAccomplishment() {
        Assertions.assertThatThrownBy(
                () -> this.rs1.feed(UUID.randomUUID(), this.accomplishment))
                .isInstanceOf(PrescriptionNotFoundException.class);
        Assertions.assertThat(this.rs1.getAccomplishments()).isEmpty();
        Assertions.assertThat(this.rs1.getPrescriptions()).isEmpty();
        Assertions
                .assertThat(this.rs2.getAccomplishments().get(this.pId).size())
                .isEqualTo(1);
        Assertions.assertThat(this.rs2.getPrescriptions().size()).isEqualTo(1);
        Assertions
                .assertThatCode(
                        () -> this.rs2.feed(this.pId, this.accomplishment))
                .doesNotThrowAnyException();
        Assertions
                .assertThat(this.rs2.getAccomplishments().get(this.pId).size())
                .isEqualTo(1);
        Assertions.assertThat(this.rs2.getPrescriptions().size()).isEqualTo(1);
        Assertions
                .assertThat(this.rs2.getAccomplishments().get(this.pId).get(0))
                .isEqualTo(this.accomplishment);
        Assertions.assertThat(this.rs2.getPrescriptions().get(this.pId))
                .isEqualTo(this.prescription);
        final Accomplishment a = new Accomplishment(UUID.randomUUID(),
                new Patient(UUID.randomUUID()), LocalDateTime.now(),
                this.accomplishment.getActivityLevels());
        Assertions.assertThatCode(() -> this.rs2.feed(this.pId, a))
                .doesNotThrowAnyException();
        Assertions
                .assertThat(this.rs2.getAccomplishments().get(this.pId).size())
                .isEqualTo(2);
        Assertions.assertThat(this.rs2.getAccomplishments()
                .get(this.prescription.getUuid()).size()).isEqualTo(2);
        Assertions.assertThat(this.rs2.getPrescriptions().size()).isEqualTo(1);
        Assertions.assertThat(this.rs2.getAccomplishments()
                .get(this.prescription.getUuid()).get(1)).isEqualTo(a);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.RsEngine#feed(eu.connecare.rs.redesign.Prescription, eu.connecare.rs.redesign.Accomplishment)}.
     */
    @Test
    public final void testFeedPrescriptionAccomplishment() {
        Assertions.assertThat(this.rs1.getAccomplishments()).isEmpty();
        Assertions.assertThat(this.rs1.getPrescriptions()).isEmpty();
        Assertions.assertThatCode(
                () -> this.rs1.feed(this.prescription, this.accomplishment))
                .doesNotThrowAnyException();
        Assertions
                .assertThat(this.rs1.getAccomplishments().get(this.pId).size())
                .isEqualTo(1);
        Assertions.assertThat(this.rs1.getPrescriptions().size()).isEqualTo(1);
        Assertions
                .assertThat(this.rs1.getAccomplishments().get(this.pId).get(0))
                .isEqualTo(this.accomplishment);
        Assertions.assertThat(this.rs1.getPrescriptions().get(this.pId))
                .isEqualTo(this.prescription);
        Assertions.assertThatCode(
                () -> this.rs2.feed(this.prescription, this.accomplishment))
                .doesNotThrowAnyException();
        Assertions
                .assertThat(this.rs2.getAccomplishments().get(this.pId).size())
                .isEqualTo(1);
        Assertions.assertThat(this.rs2.getPrescriptions().size()).isEqualTo(1);
        Assertions
                .assertThat(this.rs2.getAccomplishments().get(this.pId).get(0))
                .isEqualTo(this.accomplishment);
        Assertions.assertThat(this.rs2.getPrescriptions().get(this.pId))
                .isEqualTo(this.prescription);
        //        final Prescription p = new Prescription(UUID.randomUUID(),
        //                new Patient(UUID.randomUUID()),
        //                new Clinician(UUID.randomUUID()), LocalDate.now(),
        //                LocalDate.now().plusDays(7), this.prescription.getGoals());
        final Accomplishment a = new Accomplishment(UUID.randomUUID(),
                new Patient(UUID.randomUUID()), LocalDateTime.now(),
                this.accomplishment.getActivityLevels());
        Assertions.assertThatCode(() -> this.rs2.feed(this.prescription, a))
                .doesNotThrowAnyException();
        Assertions
                .assertThat(this.rs2.getAccomplishments().get(this.pId).size())
                .isEqualTo(2);
        Assertions.assertThat(this.rs2.getAccomplishments()
                .get(this.prescription.getUuid()).size()).isEqualTo(2);
        Assertions.assertThat(this.rs2.getPrescriptions().size()).isEqualTo(1);
        Assertions.assertThat(this.rs2.getAccomplishments()
                .get(this.prescription.getUuid()).get(1)).isEqualTo(a);
        Assertions
                .assertThat(this.rs2.getPrescriptions()
                        .get(this.prescription.getUuid()))
                .isEqualTo(this.prescription);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.RsEngine#getAdherenceDetail(java.util.UUID)}.
     */
    @Test
    public final void testGetAdherenceDetail() {
        Assertions
                .assertThatThrownBy(() -> this.rs1.getAdherenceDetail(this.pId))
                .isInstanceOf(PrescriptionNotFoundException.class);
        Map<ActivityMetric, Adherence> expected = new HashMap<>();
        expected.put(new ActivityMetric("physical", "steps"),
                new PercentageAdherence(10.0));
        expected.put(new ActivityMetric("physical", "light"),
                new PercentageAdherence(50.0));
        expected.put(new ActivityMetric("physical", "moderate"),
                new PercentageAdherence(25.0));
        try {
            Assertions.assertThat(this.rs2.getAdherenceDetail(this.pId))
                    .isEqualTo(expected);
        } catch (PrescriptionNotFoundException
                | UnknownActivityLevelException e) {
            // can't happen
            e.printStackTrace();
        }
        final Accomplishment a = new Accomplishment(UUID.randomUUID(),
                new Patient(UUID.randomUUID()), LocalDateTime.now(),
                this.accomplishment.getActivityLevels());
        try {
            this.rs2.feed(this.prescription, a);
        } catch (UnknownActivityLevelException e) {
            // can't happen
            e.printStackTrace();
        }
        expected = new HashMap<>();
        expected.put(new ActivityMetric("physical", "steps"),
                new PercentageAdherence(20.0));
        expected.put(new ActivityMetric("physical", "light"),
                new PercentageAdherence(100.0));
        expected.put(new ActivityMetric("physical", "moderate"),
                new PercentageAdherence(50.0));
        try {
            Assertions.assertThat(this.rs2.getAdherenceDetail(this.pId))
                    .isEqualTo(expected);
        } catch (PrescriptionNotFoundException
                | UnknownActivityLevelException e) {
            // can't happen
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.RsEngine#getAdherence(java.util.UUID)}.
     */
    @Test
    public final void testGetAdherence() {
        Assertions.assertThatThrownBy(() -> this.rs1.getAdherence(this.pId))
                .isInstanceOf(PrescriptionNotFoundException.class);
        try {
            Assertions
                    .assertThat(
                            this.rs2.getAdherence(this.pId).getAsPercentage())
                    .isEqualTo(new PercentageAdherence(28.3).getAsPercentage(),
                            Offset.offset(0.1));
        } catch (PrescriptionNotFoundException
                | UnknownActivityLevelException e) {
            // can't happen
            e.printStackTrace();
        }
        final Accomplishment a = new Accomplishment(UUID.randomUUID(),
                new Patient(UUID.randomUUID()), LocalDateTime.now(),
                this.accomplishment.getActivityLevels());
        try {
            this.rs2.feed(this.prescription, a);
        } catch (UnknownActivityLevelException e) {
            // can't happen
            e.printStackTrace();
        }
        try {
            Assertions
                    .assertThat(
                            this.rs2.getAdherence(this.pId).getAsPercentage())
                    .isEqualTo(new PercentageAdherence(56.6).getAsPercentage(),
                            Offset.offset(0.1));
        } catch (PrescriptionNotFoundException
                | UnknownActivityLevelException e) {
            // can't happen
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.RsEngine#getRecommendation(java.util.UUID)}.
     */
    @Test
    public final void testGetRecommendation() {
        Map<ActivityMetric, Adherence> adherenceToMetrics = new HashMap<>();
        adherenceToMetrics.put(new ActivityMetric("physical", "steps"),
                new PercentageAdherence(10.0));
        adherenceToMetrics.put(new ActivityMetric("physical", "light"),
                new PercentageAdherence(50.0));
        adherenceToMetrics.put(new ActivityMetric("physical", "moderate"),
                new PercentageAdherence(25.0));
        Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishmentOfMetrics = new HashMap<>();
        accomplishmentOfMetrics.put(new ActivityMetric("physical", "steps"),
                new DoubleLevel(1000.0));
        accomplishmentOfMetrics.put(new ActivityMetric("physical", "light"),
                new DurationLevel(Duration.ofHours(7)));
        accomplishmentOfMetrics.put(new ActivityMetric("physical", "moderate"),
                new DurationLevel(Duration.ofHours(1)));
        //        Recommendation expected = new Recommendation(RecType.WARNING,
        //                // Hi ${rec.target}, how is it going? You're almost done with ${rec.metric}, only ${rec.remaining} remaining :) Make sure to make it for ${rec.expiration}!
        //                this.prescription.getSubject(),
        //                String.format(
        //                        "Hi %s, how is it going? You're almost done with %s, only %s remaining :) Make sure to make it for %s!",
        //                        this.prescription.getSubject().getUuid(), "steps",
        //                        9000.0, this.prescription.getDueDate()),
        //                Duration.ofDays(1), adherenceToMetrics,
        //                accomplishmentOfMetrics);
        //        try {
        //            Assertions.assertThat(this.rs2.getRecommendation(this.pId))
        //                    .isEqualTo(expected);
        //        } catch (PrescriptionNotFoundException e) {
        //            // can't happen
        //            e.printStackTrace();
        //        }
        final Accomplishment a = new Accomplishment(UUID.randomUUID(),
                new Patient(UUID.randomUUID()), LocalDateTime.now(),
                this.accomplishment.getActivityLevels());
        try {
            this.rs2.feed(this.prescription, a);
        } catch (UnknownActivityLevelException e) {
            // can't happen
            e.printStackTrace();
        }
        adherenceToMetrics = new HashMap<>();
        adherenceToMetrics.put(new ActivityMetric("physical", "steps"),
                new PercentageAdherence(20.0));
        adherenceToMetrics.put(new ActivityMetric("physical", "light"),
                new PercentageAdherence(100.0));
        adherenceToMetrics.put(new ActivityMetric("physical", "moderate"),
                new PercentageAdherence(50.0));
        accomplishmentOfMetrics = new HashMap<>();
        accomplishmentOfMetrics.put(new ActivityMetric("physical", "steps"),
                new DoubleLevel(2000.0));
        accomplishmentOfMetrics.put(new ActivityMetric("physical", "light"),
                new DurationLevel(Duration.ofHours(14)));
        accomplishmentOfMetrics.put(new ActivityMetric("physical", "moderate"),
                new DurationLevel(Duration.ofHours(2)));
        //        expected = new Recommendation(RecType.AWARD,
        //                // Dear ${rec.target}, what an outstanding performance in ${rec.metric}: ${rec.level}! I'm very proud of you, congrats :)
        //                this.prescription.getSubject(),
        //                String.format(
        //                        "Dear %s, what an outstanding performance in %s: %s! I'm very proud of you, congrats :)",
        //                        this.prescription.getSubject().getUuid(), "light",
        //                        Duration.ofHours(14)),
        //                Duration.ofDays(1), adherenceToMetrics,
        //                accomplishmentOfMetrics);
        //        try {
        //            Assertions.assertThat(this.rs2.getRecommendation(this.pId))
        //                    .isEqualTo(expected);
        //        } catch (PrescriptionNotFoundException e) {
        //            // can't happen
        //            e.printStackTrace();
        //        }
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.RsEngine#getFeedback(java.util.UUID)}.
     */
    @Test
    @Ignore
    public final void testGetFeedback() {
        fail("Not yet implemented");
    }

}
