/**
 * Created by sm (stefano.mariani@unimore.it) on Mar 8, 2018 12:39:09 PM
 */
package eu.connecare.rs.redesign;

import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import advice.connecare.rs.redesign.RecType;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class NlgEngineTest {

    private NlgEngine nlg1;
    private Map<ActivityMetric, Adherence> adherence1;
    private ActivityMetric worst;
    private ActivityMetric best;
    private ActivityMetric closestToAvg;

    /**
     */
    @Before
    public void setUp() {
        this.nlg1 = new NlgEngine(
                Paths.get("src/main/resources/sentence_templates.txt"),
                Paths.get("src/main/resources/EN_pos.txt"),
                Paths.get("src/main/resources/times.json"),
                Paths.get("src/main/resources/activities.json"));
        this.adherence1 = new HashMap<>();
        this.worst = new ActivityMetric("physical", "steps");
        this.adherence1.put(this.worst, new PercentageAdherence(50.0));
        this.best = new ActivityMetric("physical", "light");
        this.adherence1.put(this.best, new PercentageAdherence(90.0));
        this.closestToAvg = new ActivityMetric("physical", "moderate");
        this.adherence1.put(this.closestToAvg, new PercentageAdherence(70.0));
    }

    /**
     */
    @After
    public void tearDown() {
        this.nlg1 = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.NlgEngine#generateMessage(eu.connecare.rs.redesign.Prescription, java.util.Map, java.util.Map, advice.connecare.rs.redesign.RecType)}.
     */
    @Test
    @Ignore
    public final void testGenerateMessage() {
        //        Logger.getAnonymousLogger().info(this.nlg1.toString());
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> goals = new HashMap<>();
        final DoubleLevel worstLvlGoal = new DoubleLevel(7000.0);
        goals.put(this.worst, worstLvlGoal);
        goals.put(this.best, new DurationLevel(Duration.ofHours(14)));
        final DurationLevel bestLvlMeasure = new DurationLevel(
                Duration.ofHours(7));
        final DurationLevel closestLvlGoal = bestLvlMeasure;
        goals.put(this.closestToAvg, closestLvlGoal);
        final Patient subject = new Patient(UUID.randomUUID());
        final Prescription p = new Prescription(UUID.randomUUID(), subject,
                new Clinician(UUID.randomUUID()), LocalDate.now(),
                LocalDate.now().plusDays(7), goals);
        final List<Accomplishment> accomplishments = new ArrayList<>();
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> measurements = new HashMap<>();
        final DoubleLevel worstLvlMeasure = new DoubleLevel(100.0);
        measurements.put(this.worst, worstLvlMeasure);
        final DurationLevel closestLvlMeasure = new DurationLevel(
                Duration.ofHours(1));
        measurements.put(this.best, bestLvlMeasure);
        measurements.put(this.closestToAvg, closestLvlMeasure);
        accomplishments.add(new Accomplishment(UUID.randomUUID(), subject,
                LocalDateTime.now().plusDays(1), measurements));
        final Strategy s = new Strategy();
        Map<ActivityMetric, Adherence> adherence = null;
        try {
            adherence = s.computeAdherenceDetail(p, accomplishments);
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
        String msg = this.nlg1.generateMessage(p,
                s.getAccomplishmentSummary().get(p.getUuid()), adherence,
                RecType.ALERT);
        Logger.getAnonymousLogger().info(msg);
        Assertions.assertThat(msg)
                .isEqualTo(String.format(
                        //                        Hi ${rec.target}, is everything alright? Your ${rec.level} of ${rec.metric} goal is still far away...time to move! ${rec.expiration} is approaching :)
                        "Hi %s, is everything alright? Your %s of %s goal is still far away...time to move! %s is approaching :)",
                        subject.getUuid().toString(),
                        goals.get(this.worst).getValue().toString(),
                        this.worst.getMetricName(), p.getDueDate().toString()));
        msg = this.nlg1.generateMessage(p,
                s.getAccomplishmentSummary().get(p.getUuid()), adherence,
                RecType.WARNING);
        Logger.getAnonymousLogger().info(msg);
        Assertions.assertThat(msg)
                .isEqualTo(String.format(
                        //                        Hi ${rec.target}, how is it going? You're almost done with {rec.metric}, only ${rec.remaining} remaining :) Make sure to make it for ${rec.expiration}!
                        "Hi %s, how is it going? You're almost done with %s, only %s remaining :) Make sure to make it for %s!",
                        subject.getUuid().toString(),
                        this.worst.getMetricName(),
                        worstLvlMeasure.remainingTo(worstLvlGoal, this.worst)
                                .getValue().toString(),
                        p.getDueDate().toString()));
        msg = this.nlg1.generateMessage(p,
                s.getAccomplishmentSummary().get(p.getUuid()), adherence,
                RecType.MOTIVATIONAL);
        Logger.getAnonymousLogger().info(msg);
        Assertions.assertThat(msg)
                .isEqualTo(String.format(
                        //                        Hey ${rec.target}, I see you are doing pretty good with your ${rec.level} of ${rec.metric} goal :) Good job, keep going!
                        "Hey %s, I see you are doing pretty good with your %s of %s goal :) Good job, keep going beyond your current %s!",
                        subject.getUuid().toString(), closestLvlGoal.getValue(),
                        this.closestToAvg.getMetricName(),
                        closestLvlMeasure.getValue()));
        msg = this.nlg1.generateMessage(p,
                s.getAccomplishmentSummary().get(p.getUuid()), adherence,
                RecType.AWARD);
        Logger.getAnonymousLogger().info(msg);
        Assertions.assertThat(msg)
                .isEqualTo(String.format(
                        //                        Dear ${rec.target}, what an outstanding performance in ${rec.metric}! I'm very proud of you, congrats :)
                        "Dear %s, what an outstanding performance in %s: %s! I'm very proud of you, congrats :)",
                        subject.getUuid().toString(), this.best.getMetricName(),
                        bestLvlMeasure.getValue()));
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.NlgEngine#findMetric(java.util.Map, java.util.function.BinaryOperator)}.
     */
    @Test
    public final void testFindMetric() {
        Assertions
                .assertThat(NlgEngine.findMetric(this.adherence1, Double::min))
                .isEqualTo(this.worst);
        Assertions
                .assertThat(NlgEngine.findMetric(this.adherence1, Double::max))
                .isEqualTo(this.best);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.NlgEngine#closestToAverageMetric(java.util.Map)}.
     */
    @Test
    public final void testClosestToAverageMetric() {
        Assertions.assertThat(NlgEngine.closestToAverageMetric(this.adherence1))
                .isEqualTo(this.closestToAvg);
        this.adherence1.put(new ActivityMetric("physical", "vigorous"),
                new PercentageAdherence(60.0));
        Assertions.assertThat(NlgEngine.closestToAverageMetric(this.adherence1))
                .isEqualTo(this.closestToAvg);
    }

}
