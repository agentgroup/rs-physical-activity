/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 9, 2018 12:16:01 PM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class DurationLevelTest {

    private ActivityLevel<Duration> al1;
    private ActivityLevel<Duration> al2;
    private ActivityLevel<Duration> al3;

    /**
     */
    @Before
    public void setUp() {
        this.al1 = new DurationLevel(Duration.ZERO);
        this.al2 = new DurationLevel(Duration.ofHours(10));
        this.al3 = new DurationLevel(Duration.ofHours(20));
    }

    /**
     */
    @After
    public void tearDown() {
        this.al1 = null;
        this.al2 = null;
        this.al3 = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.DurationLevel#accumulate(eu.connecare.rs.redesign.ActivityLevel)}.
     */
    @Test
    public final void testAccumulateActivityLevelOfDuration() {
        Assertions.assertThat(this.al1.accumulate(this.al2))
                .isEqualTo(this.al2);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.DurationLevel#percentageDone(eu.connecare.rs.redesign.ActivityLevel)}.
     */
    @Test
    public final void testPercentageDoneActivityLevelOfDuration() {
        Assertions.assertThat(this.al1.percentageDone(this.al2)).isEqualTo(0.0);
        Assertions.assertThat(this.al2.percentageDone(this.al3))
                .isEqualTo(50.0);
        Assertions.assertThat(this.al3.percentageDone(this.al2))
                .isEqualTo(200.0);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.AbstractActivityLevel#getValue()}.
     */
    @Test
    public final void testGetValue() {
        Assertions.assertThat(this.al1.getValue()).isEqualTo(Duration.ZERO);
        Assertions.assertThat(this.al2.getValue())
                .isEqualTo(this.al3.getValue().minus(this.al2.getValue()));
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.DurationLevel#redistribute(java.time.Duration, eu.connecare.rs.redesign.ActivityMetric)}.
     */
    @Test
    public final void testRedistributeActivityLevelOfDuration() {
        Assertions
                .assertThat(this.al3.redistribute(Duration.ofDays(2),
                        new ActivityMetric("physical", "moderate")))
                .isEqualTo(this.al2);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.DurationLevel#remainingTo(eu.connecare.rs.redesign.ActivityLevel, eu.connecare.rs.redesign.ActivityMetric)}.
     */
    @Test
    public final void testRemainingToActivityLevelOfDuration() {
        Assertions
                .assertThat(this.al2.remainingTo(this.al3,
                        new ActivityMetric("physical", "moderate")))
                .isEqualTo(this.al2);
    }

}
