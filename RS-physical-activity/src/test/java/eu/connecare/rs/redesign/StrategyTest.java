/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 16, 2018 3:43:42 PM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class StrategyTest {

    /*
     * prescription
     */
    private UUID pid1;
    private Patient pat1;
    private Clinician c1;
    private LocalDate sd1;
    private LocalDate dd1;
    private ActivityMetric am1;
    private ActivityMetric am2;
    private ActivityMetric am3;
    private ActivityLevel<? extends Comparable<?>> goal1;
    private ActivityLevel<? extends Comparable<?>> goal2;
    private ActivityLevel<? extends Comparable<?>> goal3;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> goals1;
    private Prescription p1;
    /*
     * accomplishments
     */
    private UUID aid1;
    private LocalDate d1;
    private ActivityLevel<Double> level11;
    private ActivityLevel<Duration> level12;
    private ActivityLevel<Duration> level13;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> measures1;
    private Accomplishment a1;
    /**/
    private UUID aid2;
    private LocalDate d2;
    private ActivityLevel<Double> level21;
    private ActivityLevel<Duration> level22;
    private ActivityLevel<Duration> level23;
    private Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> measures2;
    private Accomplishment a2;
    private List<Accomplishment> list;
    private Map<UUID, Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>>> summary1;
    private Map<UUID, Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>>> summary2;
    /*
     * strategy
     */
    private IStrategy strata;
    /*
     * adherences
     */
    private Adherence ad11;
    private Adherence ad12;
    private Adherence ad13;
    private Adherence avgAd;
    private ActivityMetric am4;
    private ActivityLevel<? extends Comparable<?>> goal4;
    private ActivityLevel<Duration> level14;
    private ActivityLevel<Duration> level24;
    private Adherence ad14;

    /**
     */
    @Before
    public void setUp() {
        /*
         * prescription
         */
        this.pid1 = UUID.randomUUID();
        this.pat1 = new Patient(UUID.randomUUID());
        this.c1 = new Clinician(UUID.randomUUID());
        this.sd1 = LocalDate.now();
        this.dd1 = this.sd1.plusDays(7);
        this.am1 = new ActivityMetric("physical", "steps");
        this.am2 = new ActivityMetric("physical", "vigorous");
        this.am3 = new ActivityMetric("physical", "moderate");
        this.am4 = new ActivityMetric("physical", "sedentary");
        this.goal1 = new DoubleLevel(10000.0);
        this.goal2 = new DurationLevel(Duration.ofHours(10));
        this.goal3 = new DurationLevel(Duration.ofHours(30));
        this.goal4 = new DurationLevel(Duration.ofHours(10));
        this.goals1 = new HashMap<>();
        this.goals1.put(this.am1, this.goal1);
        this.goals1.put(this.am2, this.goal2);
        this.goals1.put(this.am3, this.goal3);
        this.goals1.put(this.am4, this.goal4);
        this.p1 = new Prescription(this.pid1, this.pat1, this.c1, this.sd1,
                this.dd1, this.goals1);
        /*
         * accomplishments
         */
        this.aid1 = UUID.randomUUID();
        this.d1 = this.sd1.plusDays(1);
        this.level11 = new DoubleLevel(1000.0);
        this.level12 = new DurationLevel(
                Duration.ofHours(2).plus(Duration.ofMinutes(30))); // 2.5 hours
        this.level13 = new DurationLevel(Duration.ofHours(20));
        this.level14 = new DurationLevel(Duration.ofHours(1));
        this.measures1 = new HashMap<>();
        this.measures1.put(this.am1, this.level11);
        this.measures1.put(this.am2, this.level12);
        this.measures1.put(this.am3, this.level13);
        this.measures1.put(this.am4, this.level14);
        this.a1 = new Accomplishment(this.aid1, this.pat1,
                LocalDateTime.of(this.d1, LocalTime.now()), this.measures1);
        /**/
        this.aid2 = UUID.randomUUID();
        this.d2 = this.d1.plusDays(1);
        this.level21 = new DoubleLevel(1000.0);
        this.level22 = new DurationLevel(
                Duration.ofHours(2).plus(Duration.ofMinutes(30))); // 2.5 hours
        this.level23 = new DurationLevel(Duration.ofHours(20));
        this.level24 = new DurationLevel(Duration.ofHours(1));
        this.measures2 = new HashMap<>();
        this.measures2.put(this.am1, this.level21);
        this.measures2.put(this.am2, this.level22);
        this.measures2.put(this.am3, this.level23);
        this.measures2.put(this.am4, this.level24);
        this.a2 = new Accomplishment(this.aid2, this.pat1,
                LocalDateTime.of(this.d2, LocalTime.now()), this.measures2);
        this.list = new ArrayList<>();
        this.list.add(this.a1);
        this.list.add(this.a2);
        /*
         * strategy
         */
        this.strata = new Strategy();
        /*
         * adherences
         */
        this.ad11 = new PercentageAdherence(20.0);
        this.ad12 = new PercentageAdherence(50.0);
        this.ad13 = new PercentageAdherence(133.333333333);
        this.ad14 = new PercentageAdherence(80.0);
        this.avgAd = new PercentageAdherence(70.8325);
        /*
         * accomplishments summary
         */
        this.summary1 = new HashMap<>();
        this.summary2 = new HashMap<>();
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accSum1 = new HashMap<>();
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accSum2 = new HashMap<>();
        accSum1.put(this.am1, new DoubleLevel(2000.0));
        accSum2.put(this.am1, this.level11.accumulate(this.level21));
        accSum1.put(this.am2, new DurationLevel(Duration.ofHours(5)));
        accSum2.put(this.am2, this.level12.accumulate(this.level22));
        accSum1.put(this.am3, new DurationLevel(Duration.ofHours(40)));
        accSum2.put(this.am3, this.level13.accumulate(this.level23));
        accSum1.put(this.am4, new DurationLevel(Duration.ofHours(2)));
        accSum2.put(this.am4, this.level14.accumulate(this.level24));
        this.summary1.put(this.p1.getUuid(), accSum1);
        this.summary2.put(this.p1.getUuid(), accSum2);
    }

    /**
     */
    @After
    public void tearDown() {
        this.goals1.clear();
        this.goals1 = null;
        this.p1 = null;
        this.measures1.clear();
        this.measures1 = null;
        this.a1 = null;
        this.measures2.clear();
        this.measures2 = null;
        this.a2 = null;
        this.list.clear();
        this.list = null;
        this.strata = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Strategy#computeAdherenceDetail(eu.connecare.rs.redesign.Prescription, java.util.List)}.
     */
    @Test
    public final void testComputeAdherenceDetail() {
        try {
            final Map<ActivityMetric, Adherence> computedAdherence = this.strata
                    .computeAdherenceDetail(this.p1, this.list);
            Assertions.assertThat(computedAdherence.get(this.am1))
                    .isEqualTo(this.ad11);
            Assertions.assertThat(computedAdherence.get(this.am2))
                    .isEqualTo(this.ad12);
            Assertions.assertThat(computedAdherence.get(this.am3))
                    .isNotEqualTo(this.ad13);
            Assertions
                    .assertThat(
                            computedAdherence.get(this.am3).getAsPercentage())
                    .isEqualTo(this.ad13.getAsPercentage(),
                            Offset.offset(0.000000001));
            Assertions
                    .assertThat(
                            computedAdherence.get(this.am4).getAsPercentage())
                    .isEqualTo(this.ad14.getAsPercentage(),
                            Offset.offset(0.000000001));
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Strategy#computeAdherence(eu.connecare.rs.redesign.Prescription, java.util.List)}.
     */
    @Test
    public final void testComputeAdherence() {
        try {
            final Adherence globalAd = this.strata.computeAdherence(this.p1,
                    this.list);
            //            Assertions.assertThat(globalAd).isEqualTo(this.avgAd);
            Assertions.assertThat(globalAd.getAsPercentage()).isEqualTo(
                    this.avgAd.getAsPercentage(), Offset.offset(0.001));
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Strategy#getDetailedAdherence()}.
     */
    @Test
    public final void testGetDetailedAdherence() {
        try {
            final Map<ActivityMetric, Adherence> computedAdherence = this.strata
                    .computeAdherenceDetail(this.p1, this.list);
            final Map<UUID, Map<ActivityMetric, Adherence>> fetchedAdherence = new HashMap<>();
            fetchedAdherence.put(this.p1.getUuid(), computedAdherence);
            Assertions.assertThat(this.strata.getDetailedAdherence())
                    .isEqualTo(fetchedAdherence);
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
    }

    /**
     * Test method for {@link eu.connecare.rs.redesign.Strategy#getAdherence()}.
     */
    @Test
    public final void testGetAdherence() {
        try {
            final Adherence globalAd = this.strata.computeAdherence(this.p1,
                    this.list);
            final Map<UUID, Adherence> fetchedAd = new HashMap<>();
            fetchedAd.put(this.p1.getUuid(), globalAd);
            Assertions.assertThat(this.strata.getAdherence())
                    .isEqualTo(fetchedAd);
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Strategy#getAccomplishmentSummary()}.
     */
    @Test
    public final void testGetAccomplishmentSummary() {
        try {
            this.strata.computeAdherenceDetail(this.p1, this.list);
            Assertions.assertThat(this.strata.getAccomplishmentSummary())
                    .isEqualTo(this.summary1);
            Assertions.assertThat(this.strata.getAccomplishmentSummary())
                    .isEqualTo(this.summary2);
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
    }

}
