/**
 * Created by sm (stefano.mariani@unimore.it) on Mar 9, 2018 11:32:13 AM
 */
package eu.connecare.rs.redesign;

import static org.junit.Assert.fail;

import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import advice.connecare.rs.redesign.RecType;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class PolicyTest {

    private Policy pol;

    /**
     */
    @Before
    public void setUp() {
        this.pol = new Policy(
                Paths.get("src/main/resources/sentence_templates.txt"),
                Paths.get("src/main/resources/EN_pos.txt"),
                Paths.get("src/main/resources/times.json"),
                Paths.get("src/main/resources/activities.json"));
    }

    /**
     */
    @After
    public void tearDown() {
        this.pol = null;
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Policy#computeRecommendation(eu.connecare.rs.redesign.Prescription, java.util.Map, java.util.Map, eu.connecare.rs.redesign.Adherence)}.
     */
    @Test
    public final void testComputeRecommendation() {
        // goalss
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> goals = new HashMap<>();
        goals.put(new ActivityMetric("physical", "steps"),
                new DoubleLevel(10000.0));
        goals.put(new ActivityMetric("physical", "light"),
                new DurationLevel(Duration.ofHours(20)));
        goals.put(new ActivityMetric("physical", "moderate"),
                new DurationLevel(Duration.ofHours(10)));
        final UUID pId = UUID.randomUUID();
        final Patient pat = new Patient(pId);
        // prescription
        final Prescription prescription = new Prescription(pId, pat,
                new Clinician(pId), LocalDate.now().minusDays(5),
                LocalDate.now().plusDays(5), goals);
        // measurements
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishmentSummary = new HashMap<>();
        accomplishmentSummary.put(new ActivityMetric("physical", "steps"),
                new DoubleLevel(5000.0));
        accomplishmentSummary.put(new ActivityMetric("physical", "light"),
                new DurationLevel(Duration.ofHours(7)));
        accomplishmentSummary.put(new ActivityMetric("physical", "moderate"),
                new DurationLevel(Duration.ofHours(2)));
        // accomplishment
        final List<Accomplishment> accs = new ArrayList<>();
        Accomplishment accomp = new Accomplishment(pId, pat,
                LocalDateTime.now(), accomplishmentSummary);
        accs.add(accomp);
        // (detailed) adherence
        Map<ActivityMetric, Adherence> detailedAdherence = null;
        final Strategy strat = new Strategy();
        Adherence adherence = null;
        try {
            //            detailedAdherence = strat.computeAdherenceDetail(prescription,
            //                    accs);
            adherence = strat.computeAdherence(prescription, accs);
            detailedAdherence = strat.getDetailedAdherence().get(pId);
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
        // expected (steps: 50%, light: 35%, moderate: 20%,avg = 35%, 5 / 10 days)
        ActivityMetric metric = NlgEngine.findMetric(detailedAdherence,
                Double::min);
        ActivityLevel<? extends Comparable<?>> acc = accomplishmentSummary
                .get(metric);
        ActivityLevel<? extends Comparable<?>> rem = null;
        if (acc instanceof DoubleLevel) {
            rem = ((DoubleLevel) acc)
                    .remainingTo((DoubleLevel) goals.get(metric), metric);
        } else if (acc instanceof DurationLevel) {
            rem = ((DurationLevel) acc)
                    .remainingTo((DurationLevel) goals.get(metric), metric);
        }
        // Hi ${rec.target}, how is it going? You're almost done with ${rec.metric}, only ${rec.remaining} remaining :) Make sure to make it for ${rec.expiration}!
        //        String msg = String.format(
        //                "Hi %s, how is it going? You're almost done with %s, only %s remaining :) Make sure to make it for %s!",
        //                pat.getUuid(), metric.getMetricName(), rem.getValue(),
        //                prescription.getDueDate());
        //        Recommendation expected = new Recommendation(RecType.WARNING, pat, msg,
        //                Duration.ofDays(1), detailedAdherence, accomplishmentSummary);
        //        // ASSERTION
        //        Assertions.assertThat(this.pol.computeRecommendation(prescription,
        //                accomplishmentSummary, detailedAdherence, adherence,
        //                accomp.getDate())).isEqualTo(expected);
        // accomplishment
        accs.clear();
        accomp = new Accomplishment(pId, pat, LocalDateTime.now().minusDays(3),
                accomplishmentSummary);
        accs.add(accomp);
        // (detailed) adherence
        try {
            //            detailedAdherence = strat.computeAdherenceDetail(prescription,
            //                    accs);
            adherence = strat.computeAdherence(prescription, accs);
            detailedAdherence = strat.getDetailedAdherence().get(pId);
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
        // expected (steps: 50%, light: 35%, moderate: 20%, avg = 35%, 2 / 10 days)
        metric = NlgEngine.findMetric(detailedAdherence, Double::max);
        acc = accomplishmentSummary.get(metric);
        rem = null;
        if (acc instanceof DoubleLevel) {
            rem = ((DoubleLevel) acc)
                    .remainingTo((DoubleLevel) goals.get(metric), metric);
        } else if (acc instanceof DurationLevel) {
            rem = ((DurationLevel) acc)
                    .remainingTo((DurationLevel) goals.get(metric), metric);
        }
        // Dear ${rec.target}, what an outstanding performance in ${rec.metric}: ${rec.level}! I'm very proud of you, congrats :)
        //        msg = String.format(
        //                "Dear %s, what an outstanding performance in %s: %s! I'm very proud of you, congrats :)",
        //                pat.getUuid(), metric.getMetricName(), acc.getValue());
        //        expected = new Recommendation(RecType.AWARD, pat, msg,
        //                Duration.ofDays(1), detailedAdherence, accomplishmentSummary);
        //        // ASSERTION
        //        Assertions.assertThat(this.pol.computeRecommendation(prescription,
        //                accomplishmentSummary, detailedAdherence, adherence,
        //                accomp.getDate())).isEqualTo(expected);
        // accomplishment
        accs.clear();
        accomp = new Accomplishment(pId, pat, LocalDateTime.now().plusDays(4),
                accomplishmentSummary);
        accs.add(accomp);
        // (detailed) adherence
        try {
            //            detailedAdherence = strat.computeAdherenceDetail(prescription,
            //                    accs);
            adherence = strat.computeAdherence(prescription, accs);
            detailedAdherence = strat.getDetailedAdherence().get(pId);
        } catch (UnknownActivityLevelException e) {
            // TODO test exception
            e.printStackTrace();
        }
        // expected (steps: 50%, light: 35%, moderate: 20%, avg = 35%, 9 / 10 days)
        metric = NlgEngine.findMetric(detailedAdherence, Double::min);
        acc = accomplishmentSummary.get(metric);
        rem = null;
        if (acc instanceof DoubleLevel) {
            rem = ((DoubleLevel) acc)
                    .remainingTo((DoubleLevel) goals.get(metric), metric);
        } else if (acc instanceof DurationLevel) {
            rem = ((DurationLevel) acc)
                    .remainingTo((DurationLevel) goals.get(metric), metric);
        }
        //        // Hi ${rec.target}, is everything alright? Your ${rec.goal} of ${rec.metric} goal is still far away...time to move! ${rec.expiration} is approaching :)
        //        msg = String.format(
        //                "Hi %s, is everything alright? Your %s of %s goal is still far away...time to move! %s is approaching :)",
        //                pat.getUuid(), goals.get(metric).getValue(),
        //                metric.getMetricName(), prescription.getDueDate());
        //        expected = new Recommendation(RecType.ALERT, pat, msg,
        //                Duration.ofDays(1), detailedAdherence, accomplishmentSummary);
        //        // ASSERTION
        //        Assertions.assertThat(this.pol.computeRecommendation(prescription,
        //                accomplishmentSummary, detailedAdherence, adherence,
        //                accomp.getDate())).isEqualTo(expected);
    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Policy#computeRecType(java.time.LocalDate, java.time.LocalDate, eu.connecare.rs.redesign.Adherence)}.
     */
//    @SuppressWarnings("static-method")
//    @Test
//    public final void testComputeRecType() {
//        Assertions.assertThat(Policy.computeRecType(new PercentageAdherence(70),
//                LocalDateTime.now())).isEqualTo(RecType.AWARD);
//        Assertions.assertThat(Policy.computeRecType(new PercentageAdherence(40),
//                LocalDateTime.now())).isEqualTo(RecType.MOTIVATIONAL);
//        Assertions.assertThat(Policy.computeRecType(new PercentageAdherence(25),
//                LocalDateTime.now())).isEqualTo(RecType.WARNING);
//        Assertions.assertThat(Policy.computeRecType(new PercentageAdherence(24),
//                LocalDateTime.now())).isEqualTo(RecType.ALERT);
//    }

    /**
     * Test method for
     * {@link eu.connecare.rs.redesign.Policy#computeFeedback()}.
     */
    @Test
    @Ignore
    public final void testComputeFeedback() {
        fail("Not yet implemented");
    }

}
