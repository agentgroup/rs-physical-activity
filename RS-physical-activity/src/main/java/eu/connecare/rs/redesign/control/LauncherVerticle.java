package eu.connecare.rs.redesign.control;

import java.time.LocalDate;
import java.time.LocalTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.connecare.rs.redesign.rest.RsConfigEndpoint;
import eu.connecare.rs.redesign.smsclient.SmsClient;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class LauncherVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LogManager.getFormatterLogger();

    public static void main(String[] args) {
        VertxOptions options = new VertxOptions();
        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                Vertx v = res.result();
                EventBus eb = v.eventBus();
                LogManager.getFormatterLogger("eu.connecare.rs.dblog").info(
                        "Patient name, Patient id, Prescription id, Prescription goals, Summary id, Summary date, Summary measurements, Adherence detail, Recommendation type, Recommendation msg");
                /*
                 * perceive days passing
                 */
                final LocalDate dateNow = LocalDate.now();
                v.sharedData().getLocalMap("days").put("previous",
                        dateNow.toString());
                /*
                 * Config endpoint
                 */
                v.deployVerticle(RsConfigEndpoint.class,
                        new DeploymentOptions(), dRes -> {
                            if (dRes.succeeded()) {
                                LauncherVerticle.LOGGER.info(
                                        "Verticle deployed: " + dRes.result());
                            } else {
                                LauncherVerticle.LOGGER
                                        .info("Verticle NOT deployed: "
                                                + dRes.cause());
                            }
                        });
                /*
                 * RS cycle + connectivity + language configuration
                 */
                handleConfig(v, eb);
                cycle(v);
            } else {
                LauncherVerticle.LOGGER.fatal(
                        "Something went wrong while creating clustered Vertx instance: %s",
                        res.cause().getLocalizedMessage());
            }
        });
    }

    private static Future<Long> configCycle(final Vertx v) {
        Future<Buffer> fConfig = Future.future();
        v.executeBlocking(future -> {
            final Buffer b = v.fileSystem().readFileBlocking("RS-config.json");
            future.complete(b);
        }, result -> {
            if (result.succeeded()) {
                fConfig.complete((Buffer) result.result());
            } else {
                LauncherVerticle.LOGGER.fatal("Failed to read config file: %s",
                        result.cause().getLocalizedMessage());
                fConfig.fail(Buffer.buffer(result.cause().getLocalizedMessage())
                        .toString());
            }
        });
        return fConfig.compose(res -> {
            final JsonObject config = new JsonObject(res);
            return Future.succeededFuture(config.getLong("cycle"));
        });
    }

    /**
     * @param v
     */
    private static void cycle(Vertx v) {
        final LocalTime now = LocalTime.now();
        int hourNow = now.getHour();
        if (hourNow >= 8 && hourNow <= 22) {
            v.deployVerticle(SmsClient.class, new DeploymentOptions(), dRes -> {
                if (dRes.succeeded()) {
                    LauncherVerticle.LOGGER
                            .info("Verticle deployed: " + dRes.result());
                    v.undeploy(dRes.result(), uRes -> {
                        if (uRes.succeeded()) {
                            LauncherVerticle.LOGGER.info(
                                    "Verticle undeployed: " + dRes.result());
                            configCycle(v).compose(cycle -> {
                                LauncherVerticle.LOGGER.info(
                                        "Next deployment planned in %s minutes",
                                        cycle / 1000.0 / 60.0);
                                /*
                                 * schedule next deployment
                                 */
                                v.setTimer(cycle, id -> {
                                    cycle(v);
                                });
                                return Future.succeededFuture();
                            });
                        } else {
                            LauncherVerticle.LOGGER
                                    .info("Verticle NOT undeployed: "
                                            + dRes.result());
                        }
                    });
                } else {
                    LauncherVerticle.LOGGER
                            .info("Verticle NOT deployed: " + dRes.cause());
                }
            });
        } else {
            LauncherVerticle.LOGGER
                    .info("Skipping verticle deployment (time: %s)", now);
            configCycle(v).compose(cycle -> {
                LauncherVerticle.LOGGER.info(
                        "Next deployment planned in %s minutes",
                        cycle / 1000.0 / 60.0);
                /*
                 * schedule next deployment
                 */
                v.setTimer(cycle, id -> {
                    cycle(v);
                });
                return Future.succeededFuture();
            });
        }
    }

    /**
     * @param v
     * @param eb
     * @param timerStuff
     * @return
     */
    private static void handleConfig(Vertx v, EventBus eb) {
        eb.consumer("RS.physical.config", message -> {
            /*
             * "cycle" case
             */
            if (message.headers().contains("cycle")) {
                LauncherVerticle.LOGGER.info(
                        "Request to schedule next deployment in %s ms received",
                        message.body());
                LauncherVerticle.LOGGER.info("\tReading old config file...");
                v.fileSystem().readFile("RS-config.json", readRes -> {
                    if (readRes.succeeded()) {
                        LauncherVerticle.LOGGER
                                .info("\t...old config file read");
                        JsonObject config = readRes.result().toJsonObject();
                        config.put("cycle",
                                Long.parseLong(message.body().toString()));
                        LauncherVerticle.LOGGER
                                .info("\tWriting new config file...");
                        v.fileSystem().writeFile("RS-config.json",
                                config.toBuffer(), writeRes -> {
                                    if (writeRes.succeeded()) {
                                        LauncherVerticle.LOGGER.info(
                                                "\t...new config file written");
                                    } else {
                                        LauncherVerticle.LOGGER.error(
                                                "\t...error while writing new config file: %s",
                                                writeRes.cause()
                                                        .getLocalizedMessage());
                                    }
                                });
                    } else {
                        LauncherVerticle.LOGGER.error(
                                "\t...error while reading old config file: %s",
                                readRes.cause().getLocalizedMessage());
                    }
                });
            }
            /*
             * "alerts" case
             */
            if (message.headers().contains("alerts")) {
                LauncherVerticle.LOGGER.info(
                        "Request to set alerts as active to %s received",
                        message.body());
                LauncherVerticle.LOGGER.info("\tReading old config file...");
                v.fileSystem().readFile("RS-config.json", readRes -> {
                    if (readRes.succeeded()) {
                        LauncherVerticle.LOGGER
                                .info("\t...old config file read");
                        JsonObject config = readRes.result().toJsonObject();
                        config.put("sendAlerts", Boolean
                                .parseBoolean(message.body().toString()));
                        LauncherVerticle.LOGGER
                                .info("\tWriting new config file...");
                        v.fileSystem().writeFile("RS-config.json",
                                config.toBuffer(), writeRes -> {
                                    if (writeRes.succeeded()) {
                                        LauncherVerticle.LOGGER.info(
                                                "\t...new config file written");
                                    } else {
                                        LauncherVerticle.LOGGER.error(
                                                "\t...error while writing new config file: %s",
                                                writeRes.cause()
                                                        .getLocalizedMessage());
                                    }
                                });
                    } else {
                        LauncherVerticle.LOGGER.error(
                                "\t...error while reading old config file: %s",
                                readRes.cause().getLocalizedMessage());
                    }
                });
            }
            /*
             * "applicationId" case
             */
            if (message.headers().contains("applicationId")) {
                LauncherVerticle.LOGGER.info(
                        "Request to set config applicationId to %s received",
                        message.body());
                LauncherVerticle.LOGGER.info("\tReading old config file...");
                v.fileSystem().readFile("RS-config.json", readRes -> {
                    if (readRes.succeeded()) {
                        LauncherVerticle.LOGGER
                                .info("\t...old config file read");
                        JsonObject config = readRes.result().toJsonObject();
                        config.put("applicationId", message.body().toString());
                        LauncherVerticle.LOGGER
                                .info("\tWriting new config file...");
                        v.fileSystem().writeFile("RS-config.json",
                                config.toBuffer(), writeRes -> {
                                    if (writeRes.succeeded()) {
                                        LauncherVerticle.LOGGER.info(
                                                "\t...new config file written");
                                    } else {
                                        LauncherVerticle.LOGGER.error(
                                                "\t...error while writing new config file: %s",
                                                writeRes.cause()
                                                        .getLocalizedMessage());
                                    }
                                });
                    } else {
                        LauncherVerticle.LOGGER.error(
                                "\t...error while reading old config file: %s",
                                readRes.cause().getLocalizedMessage());
                    }
                });
            }
            /*
             * "userCtrl" case
             */
            if (message.headers().contains("userCtrl")) {
                LauncherVerticle.LOGGER.info(
                        "Request to set config userCtrl to %s received",
                        message.body());
                LauncherVerticle.LOGGER.info("\tReading old config file...");
                v.fileSystem().readFile("RS-config.json", readRes -> {
                    if (readRes.succeeded()) {
                        LauncherVerticle.LOGGER
                                .info("\t...old config file read");
                        JsonObject config = readRes.result().toJsonObject();
                        config.put("userCtrl", message.body().toString());
                        LauncherVerticle.LOGGER
                                .info("\tWriting new config file...");
                        v.fileSystem().writeFile("RS-config.json",
                                config.toBuffer(), writeRes -> {
                                    if (writeRes.succeeded()) {
                                        LauncherVerticle.LOGGER.info(
                                                "\t...new config file written");
                                    } else {
                                        LauncherVerticle.LOGGER.error(
                                                "\t...error while writing new config file: %s",
                                                writeRes.cause()
                                                        .getLocalizedMessage());
                                    }
                                });
                    } else {
                        LauncherVerticle.LOGGER.error(
                                "\t...error while reading old config file: %s",
                                readRes.cause().getLocalizedMessage());
                    }
                });
            }
            /*
             * "activityCtx" case
             */
            if (message.headers().contains("activityCtx")) {
                LauncherVerticle.LOGGER.info(
                        "Request to set config activityCtx to %s received",
                        message.body());
                LauncherVerticle.LOGGER.info("\tReading old config file...");
                v.fileSystem().readFile("RS-config.json", readRes -> {
                    if (readRes.succeeded()) {
                        LauncherVerticle.LOGGER
                                .info("\t...old config file read");
                        JsonObject config = readRes.result().toJsonObject();
                        config.put("activityCtx", message.body().toString());
                        LauncherVerticle.LOGGER
                                .info("\tWriting new config file...");
                        v.fileSystem().writeFile("RS-config.json",
                                config.toBuffer(), writeRes -> {
                                    if (writeRes.succeeded()) {
                                        LauncherVerticle.LOGGER.info(
                                                "\t...new config file written");
                                    } else {
                                        LauncherVerticle.LOGGER.error(
                                                "\t...error while writing new config file: %s",
                                                writeRes.cause()
                                                        .getLocalizedMessage());
                                    }
                                });
                    } else {
                        LauncherVerticle.LOGGER.error(
                                "\t...error while reading old config file: %s",
                                readRes.cause().getLocalizedMessage());
                    }
                });
            }
            /*
             * "alertCtx" case
             */
            if (message.headers().contains("alertCtx")) {
                LauncherVerticle.LOGGER.info(
                        "Request to set config alertCtx to %s received",
                        message.body());
                LauncherVerticle.LOGGER.info("\tReading old config file...");
                v.fileSystem().readFile("RS-config.json", readRes -> {
                    if (readRes.succeeded()) {
                        LauncherVerticle.LOGGER
                                .info("\t...old config file read");
                        JsonObject config = readRes.result().toJsonObject();
                        config.put("alertCtx", message.body().toString());
                        LauncherVerticle.LOGGER
                                .info("\tWriting new config file...");
                        v.fileSystem().writeFile("RS-config.json",
                                config.toBuffer(), writeRes -> {
                                    if (writeRes.succeeded()) {
                                        LauncherVerticle.LOGGER.info(
                                                "\t...new config file written");
                                    } else {
                                        LauncherVerticle.LOGGER.error(
                                                "\t...error while writing new config file: %s",
                                                writeRes.cause()
                                                        .getLocalizedMessage());
                                    }
                                });
                    } else {
                        LauncherVerticle.LOGGER.error(
                                "\t...error while reading old config file: %s",
                                readRes.cause().getLocalizedMessage());
                    }
                });
            }
            /*
             * "lang (post)" case
             */
            if (message.headers().contains("lang")) {
                LauncherVerticle.LOGGER.info(
                        "Request to add config lang to %s received",
                        message.body());
                LauncherVerticle.LOGGER.info("\tReading old config file...");
                v.fileSystem().readFile("RS-config.json", readRes -> {
                    if (readRes.succeeded()) {
                        LauncherVerticle.LOGGER
                                .info("\t...old config file read");
                        JsonObject config = readRes.result().toJsonObject();
                        JsonArray jArray = config.getJsonArray("lang");
                        jArray.add(message.body().toString());
                        config.put("lang", jArray);
                        LauncherVerticle.LOGGER
                                .info("\tWriting new config file...");
                        v.fileSystem().writeFile("RS-config.json",
                                config.toBuffer(), writeRes -> {
                                    if (writeRes.succeeded()) {
                                        LauncherVerticle.LOGGER.info(
                                                "\t...new config file written");
                                    } else {
                                        LauncherVerticle.LOGGER.error(
                                                "\t...error while writing new config file: %s",
                                                writeRes.cause()
                                                        .getLocalizedMessage());
                                    }
                                });
                    } else {
                        LauncherVerticle.LOGGER.error(
                                "\t...error while reading old config file: %s",
                                readRes.cause().getLocalizedMessage());
                    }
                });
            }
        });
    }

}
