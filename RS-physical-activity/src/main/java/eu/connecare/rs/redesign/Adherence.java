/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:45:53 AM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface Adherence extends Comparable<Adherence> {

    double getAsPercentage();

    /**
     * {@inheritDoc}
     */
    @Override
    default int compareTo(final Adherence other) {
        return Double.compare(this.getAsPercentage(), other.getAsPercentage());
    }

}
