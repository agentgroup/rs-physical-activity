/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 4:54:28 PM
 */
package eu.connecare.rs.redesign;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
/*
 * from
 * https://stackoverflow.com/questions/30362446/deserialize-json-with-jackson-
 * into-polymorphic-types-a-complete-example-is-giv/30386694#30386694
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = DoubleLevel.class, name = "DoubleLevel"),
        @JsonSubTypes.Type(value = DurationLevel.class, name = "DurationLevel") })
public abstract class AbstractActivityLevel<T extends Comparable<T>>
        implements ActivityLevel<T> {

    //    @XmlJavaTypeAdapter(DurationAdapter.class) // from https://stackoverflow.com/questions/27314093/how-to-map-java-time-duration-to-xml
    protected T value;

    /**
     *
     */
    //    @JsonCreator
    public AbstractActivityLevel(/* @JsonProperty("value") */final T value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getValue() {
        return this.value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.value == null) ? 0 : this.value.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AbstractActivityLevel)) {
            return false;
        }
        final AbstractActivityLevel<T> other = (AbstractActivityLevel<T>) obj;
        if (this.value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!this.value.equals(other.value)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @JsonValue // from http://www.baeldung.com/jackson-map
    public String toString() {
        //        return String.format("ActivityLevel [value=%s]", this.value);
        return String.format("%s", this.value);
    }

}