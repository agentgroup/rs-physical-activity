/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:27:51 AM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;
import java.util.Map;

import advice.connecare.rs.redesign.RecType;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class Recommendation extends RsEvent<RecType> {

    /**
     * @param type
     * @param patient
     * @param text
     * @param expirationTime
     * @param adherenceToMetrics
     * @param accomplishmentOfMetrics
     */
    public Recommendation(final RecType type, final Patient patient,
            final String text, final Duration expirationTime,
            final Map<ActivityMetric, Adherence> adherenceToMetrics,
            final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishmentOfMetrics) {
        super(type, patient, text, expirationTime, adherenceToMetrics,
                accomplishmentOfMetrics);
    }

}
