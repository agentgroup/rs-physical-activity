/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 10:56:19 PM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.time.DurationFormatUtils;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlJavaTypeAdapter(DurationLevelAdapter.class) // from https://stackoverflow.com/questions/27314093/how-to-map-java-time-duration-to-xml
//@JsonDeserialize(using = DurationLevelDeserializer.class)
//@JsonSerialize(using = DurationLevelSerializer.class)
public final class DurationLevel extends AbstractActivityLevel<Duration> {

    /**
     * @param value
     */
    //    @JsonCreator
    public DurationLevel(/* @JsonProperty("value") */final Duration value) {
        super(value);
    }

    /**
     * @param value
     * @param hours
     */
    @JsonCreator
    public DurationLevel(/* @JsonProperty("value") */final String value,
            ChronoUnit unit) {
        super(Duration.of(Long.parseLong(value), unit));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ActivityLevel<Duration> accumulate(
            final ActivityLevel<Duration> toAdd) {
        if (toAdd != null) { // fix for case in which a prescription metric is NOT in the accomplishments
            return new DurationLevel(this.value.plus(toAdd.getValue())); // otherwise original value is not immutable, see computeAdherenceDetail() in Strategy
        }
        return new DurationLevel(this.value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double percentageDone(final ActivityLevel<Duration> target) {
        /*
         * https://stackoverflow.com/questions/38239395/why-is-long-casted-to-
         * double-in-java
         */
        return new Long(this.value.toMinutes()).doubleValue() * 100
                / new Long(target.getValue().toMinutes()).doubleValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ActivityLevel<Duration> redistribute(final Duration available,
            final ActivityMetric metric) {
        switch (metric.getActivityType()) {
        case "physical":
            return new DurationLevel(Duration.ofMinutes(
                    Math.round(new Long(this.value.toMinutes()).doubleValue()
                            / new Long(available.toDays()).doubleValue())));
        default:
            // TODO throw
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ActivityLevel<Duration> remainingTo(
            final ActivityLevel<Duration> target, final ActivityMetric metric) {
        switch (metric.getActivityType()) {
        case "physical":
            return new DurationLevel(Duration.ofMinutes(
                    target.getValue().toMinutes() - this.value.toMinutes()));
        default:
            // TODO throw
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String stringify() {
        String s;
        if (this.value.isNegative()) {
            s = DurationFormatUtils.formatDurationHMS(Duration.ZERO.toMillis());
            return s.substring(0, s.length() - 4);
        }
        s = DurationFormatUtils.formatDurationHMS(this.value.toMillis());
        return s.substring(0, s.length() - 4); // cut fraction of seconds
    }

    //    /**
    //     * {@inheritDoc}
    //     */
    //    @Override
    //    @JsonValue
    //    public String toString() {
    //        return super.toString();
    //    }

}
