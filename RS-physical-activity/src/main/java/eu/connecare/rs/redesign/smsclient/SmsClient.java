package eu.connecare.rs.redesign.smsclient;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.connecare.rs.redesign.Accomplishment;
import eu.connecare.rs.redesign.ActivityLevel;
import eu.connecare.rs.redesign.ActivityMetric;
import eu.connecare.rs.redesign.Adherence;
import eu.connecare.rs.redesign.Clinician;
import eu.connecare.rs.redesign.DoubleLevel;
import eu.connecare.rs.redesign.DurationLevel;
import eu.connecare.rs.redesign.Gender;
import eu.connecare.rs.redesign.IRsEngine;
import eu.connecare.rs.redesign.Patient;
import eu.connecare.rs.redesign.Policy;
import eu.connecare.rs.redesign.Prescription;
import eu.connecare.rs.redesign.PrescriptionNotFoundException;
import eu.connecare.rs.redesign.Recommendation;
import eu.connecare.rs.redesign.RsEngine;
import eu.connecare.rs.redesign.Strategy;
import eu.connecare.rs.redesign.UnknownActivityLevelException;
import eu.connecare.rs.redesign.UnknownMetricException;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

public final class SmsClient extends AbstractVerticle {

    private static final Logger LOGGER = LogManager.getFormatterLogger();
    private String ip;
    private String applicationId;
    private Map<String, UserLogin> users = new HashMap<>();
    private String userCtrl;
    private String activityCtx;
    private String alertCtx;
    private List<String> languages = new ArrayList<>();
    private Map<String, IRsEngine> engines = new HashMap<>();
    private Boolean sendAlerts;

    @Override
    public void start(final Future<Void> startFuture) throws Exception {
        SmsClient.LOGGER.info("SmsClientTest started");
        final WebClient client = WebClient.create(this.vertx);
        Future<CompositeFuture> stepConfig = this.loadConfig();
        final Map<String, String> titles = loadTitlesTranslations();
        /*
         * PULL PART: fetch data from SMS
         * 
         * Step 0) get auth tokens
         */
        final Future<CompositeFuture> step0 = stepConfig.compose(res -> {
            SmsClient.LOGGER.info("USERS: %s", this.users);
            final Map<String, Future<String>> futureTokens = new HashMap<>();
            CompositeFuture stepAuth = null;
            SmsClient.LOGGER.info(
                    "# step 0) Sending #%s auth token requests...",
                    this.users.keySet().size());
            for (String tenantName : this.users.keySet()) {
                futureTokens.put(tenantName, Future.future());
                this.getToken(tenantName, client, startFuture, futureTokens);
            }
            stepAuth = CompositeFuture.join(Arrays
                    .asList(futureTokens.values().toArray(new Future[] {})));
            return stepAuth; // stepAuth completion propagated to step0!
        });
        /*
         * step0 THEN step1) fetch patients list
         */
        final Future<CompositeFuture> step1 = step0.compose(res0 -> { // res0 = step0 result
            // CHECK how to check which futures failed amongst stepAuth (res0 should be result) ISSUE #50
            final Map<String, Future<String>> futurePatients = new HashMap<>();
            CompositeFuture stepPatients = null;
            SmsClient.LOGGER.info(
                    "# step 1) Sending #%s patients list requests...",
                    this.users.keySet().size());
            for (String tenantName : this.users.keySet()) {
                futurePatients.put(tenantName, Future.future());
                final String authHeader = ((Buffer) this.vertx.sharedData()
                        .getLocalMap("authTokens").get(tenantName)).toString();
                this.getPatients(tenantName, client, startFuture, authHeader,
                        futurePatients);
            }
            stepPatients = CompositeFuture.join(Arrays // FIXME join() fails if any future fails! make them succeed with error status code instead?
                    .asList(futurePatients.values().toArray(new Future[] {})));
            return stepPatients; // stepPatients completion propagated to step1!
        });
        /*
         * step1 THEN step2) fetch active prescriptions
         */
        final Map<String, List<Patient>> patients = new HashMap<>();
        final Future<CompositeFuture> step2 = step1.compose(res1 -> { // res1 = step1 result
            // CHECK how to check which futures failed amongst stepPatients (res1 should be result) ISSUE #50
            /*
             * parse patients lists
             */
            for (String tenantName : this.users.keySet()) {
                final JsonArray patientsJson = ((Buffer) this.vertx.sharedData()
                        .getLocalMap("patientsLists").get(tenantName))
                                .toJsonArray();
                patients.put(tenantName, SmsClient.parsePatients(patientsJson));
            }
            /*
             * send requests
             */
            CompositeFuture stepPrescriptions = null;
            final Map<String, Future<String>> futurePrescriptions = new HashMap<>();
            for (String tenantName : this.users.keySet()) {
                final String authHeader = ((Buffer) this.vertx.sharedData()
                        .getLocalMap("authTokens").get(tenantName)).toString();
                for (final Patient patient : patients.get(tenantName)) {
                    futurePrescriptions.put(dehypenUuid(patient.getUuid()),
                            Future.future());
                }
                SmsClient.LOGGER.info(
                        "# step 2) Sending #%s prescription requests to tenant %s...",
                        patients.get(tenantName).size(), tenantName);
                this.getPrescriptions(client, startFuture, authHeader,
                        tenantName, patients.get(tenantName),
                        futurePrescriptions);
            }
            stepPrescriptions = CompositeFuture.join(Arrays.asList(
                    futurePrescriptions.values().toArray(new Future[] {})));
            return stepPrescriptions; // stepPrescriptions completion propagated to step2!
        });
        /*
         * step2 THEN step3) fetch prescriptions' summaries
         */
        final Future<CompositeFuture> step3 = step2.compose(res2 -> { // res2 = step2 result
            // CHECK how to check which futures failed amongst stepPrescriptions (res2 should be result) ISSUE #50
            CompositeFuture stepSummaries = null;
            final Map<String, Future<String>> futureSummaries = new HashMap<>();
            for (String tenantName : this.users.keySet()) {
                final String authHeader = ((Buffer) this.vertx.sharedData()
                        .getLocalMap("authTokens").get(tenantName)).toString();
                final LocalMap<String, Buffer> prescriptionsMap = this.vertx
                        .sharedData().getLocalMap(String
                                .format("prescriptionsMap-%s", tenantName));
                for (final String patientId : prescriptionsMap.keySet()) {
                    futureSummaries.put(patientId, Future.future());
                }
                SmsClient.LOGGER.info(
                        "# step 3) Sending #%s daily summary requests to tenant %s...",
                        prescriptionsMap.keySet().size(), tenantName);
                this.getSummaries(client, startFuture, authHeader, tenantName,
                        prescriptionsMap.keySet(), futureSummaries);
            }
            stepSummaries = CompositeFuture.join(Arrays
                    .asList(futureSummaries.values().toArray(new Future[] {})));
            return stepSummaries; // stepSummaries completion propagated to step3!
        });
        /*
         * COMPUTE part: generate recommendations TODO feedbacks
         * 
         * step3 THEN step4) trigger RsEngine computation (adherence, recommendation, feedback)
         */
        final Future<String> step4 = step3.compose(res3 -> { // res3 = step3 result
            // CHECK how to check which futures failed amongst stepSummaries (res3 should be result) ISSUE #50
            /*
             * one RsEngine for each supported language
             */
            for (String lang : this.languages) {
                this.engines.put(lang,
                        new RsEngine(new Strategy(),
                                new Policy(Paths.get("sentence_templates.txt"), // TODO put in RS-config.json, then in endpoint
                                        Paths.get(String.format("%s_pos.txt", // TODO put in RS-config.json, then in endpoint
                                                lang)),
                                        Paths.get("times.json"),
                                        Paths.get("activities.json"))));
            }
            LocalMap<String, Buffer> summariesMap;
            LocalMap<String, Buffer> prescriptionsMap;
            /*
             * iterate over tenants
             */
            for (String tenantName : this.users.keySet()) {
                /*
                 * get tenant's patients' prescriptions and summaries
                 */
                prescriptionsMap = this.vertx.sharedData().getLocalMap(
                        String.format("prescriptionsMap-%s", tenantName));
                summariesMap = this.vertx.sharedData().getLocalMap(
                        String.format("summariesMap-%s", tenantName));
                SmsClient.LOGGER.info(
                        "# step 4) Feeding recommender with #%s summaries from tenant %s (%s)",
                        summariesMap.keySet().size(), tenantName,
                        summariesMap.values());
                String patientName;
                Prescription p;
                Accomplishment a;
                JsonObject jsonR;
                Map<String, Buffer> recCache = this.vertx.sharedData()
                        .getLocalMap("recCache");
                final Map<String, String> days = this.vertx.sharedData()
                        .getLocalMap("days");
                days.put("current", LocalDate.now().toString());
                if (LocalDate.parse(days.get("current"))
                        .isAfter(LocalDate.parse(days.get("previous")))) {
                    SmsClient.LOGGER.info("\t #4) Clearing cache for %s",
                            LocalDate.parse(days.get("current")));
                    recCache.clear();
                    days.put("previous", LocalDate.now().toString());
                }
                final LocalMap<String, Buffer> recommendations = this.vertx
                        .sharedData().getLocalMap(String
                                .format("recommendations-%s", tenantName));
                /*
                 * for each patient in tenant
                 */
                for (final Patient patient : patients.get(tenantName)) {
                    patientName = patient.getFirstName();
                    if (patientName == null) { // workaround for missing names
                        patientName = dehypenUuid(patient.getUuid());
                    }
                    /*
                     * no prescription for patient? skip
                     */
                    if (prescriptionsMap
                            .get(dehypenUuid(patient.getUuid())) == null) {
                        SmsClient.LOGGER.trace(
                                "\t #4) No prescription for patient %s %s",
                                patient.getFirstName(), patient.getLastName());
                        continue;
                    }
                    p = SmsClient.parsePrescription(prescriptionsMap
                            .get(dehypenUuid(patient.getUuid())));
                    p.getSubject().setFirstName(patientName); // for NlgEngine
                    /*
                     * invalid language? skip
                     */
                    if (!this.languages.contains(patient.getLanguage())) {
                        SmsClient.LOGGER.warn(
                                "\t #4) Invalid language for patient %s %s",
                                patient.getFirstName(), patient.getLastName());
                        continue;
                    }
                    this.engines.get(patient.getLanguage()).feed(p);
                    /*
                     * no summary for patient? skip FIXME if prescription but no summary, recommendations could still be generated (i.e. deadline reached)
                     */
                    if (summariesMap
                            .get(dehypenUuid(patient.getUuid())) == null) {
                        SmsClient.LOGGER.trace(
                                "\t #4) No summary for patient %s %s",
                                patient.getFirstName(), patient.getLastName());
                        continue;
                    }
                    a = SmsClient.parseSummary(
                            summariesMap.get(dehypenUuid(patient.getUuid())));
                    a.getSubject().setFirstName(patientName); // for NlgEngine
                    Recommendation r;
                    try {
                        r = this.engines.get(patient.getLanguage())
                                .feed(p.getUuid(), a)
                                .getRecommendation(p.getUuid());
                    } catch (PrescriptionNotFoundException
                            | UnknownActivityLevelException e) {
                        e.printStackTrace();
                        continue; // TODO handle
                    }
                    r.getSubject().setFirstName(patientName); // for NlgEngine
                    r.getSubject().setLanguage(patient.getLanguage());
                    final Buffer cacheHit = recCache
                            .get(patient.getUuid().toString());
                    if (cacheHit != null) {
                        final JsonObject recSentJson = cacheHit.toJsonObject();
                        if (recSentJson.getString("type")
                                .equalsIgnoreCase(r.getType().toString())) { // FIXME cache on RecType is too little, should cache on chosen metric, too
                            SmsClient.LOGGER.info(
                                    "\t #4) Cache hit! Skipping recommendation for patient %s %s: %s == %s",
                                    patient.getFirstName(),
                                    patient.getLastName(),
                                    recSentJson.getString("type"),
                                    r.toString());
                            continue;
                        }
                    }
                    if (this.engines.get(patient.getLanguage()).getPolicy()
                            .shouldDeliver(r.getType())) {
                        Buffer b = SmsClient.jsonize(r).toBuffer();
                        SmsClient.LOGGER.info(
                                "\t #4) Caching recommendation for patient %s %s: %s",
                                patient.getFirstName(), patient.getLastName(),
                                b.toString());
                        recCache.put(patient.getUuid().toString(), b);
                        SmsClient.LOGGER.info(
                                "# step 4) Recommendation to tenant %s (TBD): %s (%s)",
                                tenantName, r, b);
                        jsonR = SmsClient.jsonize(r,
                                this.users.get(tenantName));
                        SmsClient.logJson(jsonR);
                        recommendations.put(dehypenUuid(patient.getUuid()),
                                jsonR.toBuffer());
                    } else {
                        SmsClient.LOGGER.info(
                                "# step 4) Recommendation to tenant %s (not TBD): %s",
                                tenantName, r);
                    }
                    final Map<ActivityMetric, Adherence> adh = p
                            .getActivityLevels().keySet().stream()
                            .collect(Collectors.toMap(m -> m,
                                    m -> r.getAdherence(m)));
                    LogManager.getFormatterLogger("eu.connecare.rs.dblog").info(
                            "%s %s, %s, %s, %s, %s, %s, %s, %s, %s, %s",
                            patientName, patient.getLastName(),
                            patient.getUuid(), p.getUuid(),
                            p.getActivityLevels(), a.getUuid(), a.getDate(),
                            a.getActivityLevels(), adh, r.getType(),
                            r.getText());
                }
            }
            return Future.succeededFuture("ok");
        });
        /*
         * PUSH PART: publish data to SMS
         * 
         * step4 THEN step5) push SMS alerts
         */
        final Future<CompositeFuture> step5 = step4.compose(res4 -> {
            CompositeFuture stepAlerts = null;
            final Map<String, Future<String>> futureAlerts = new HashMap<>();
            LocalMap<String, Buffer> recommendations;
            JsonObject jsonRec;
            SmsAlert alert;
            JsonObject jsonAlert;
            for (String tenantName : this.users.keySet()) {
                final String authHeader = ((Buffer) this.vertx.sharedData()
                        .getLocalMap("authTokens").get(tenantName)).toString();
                recommendations = this.vertx.sharedData().getLocalMap(
                        String.format("recommendations-%s", tenantName));
                if (this.sendAlerts) {
                    SmsClient.LOGGER.info(
                            "# step 5) Now pushing %s recommendations to tenant %s...",
                            recommendations.keySet().size(), tenantName);
                    for (final String prescriptionId : recommendations
                            .keySet()) {
                        futureAlerts.put(prescriptionId, Future.future());
                        jsonRec = recommendations.remove(prescriptionId)
                                .toJsonObject();
                        alert = SmsClient.recToAlert(jsonRec, titles);
                        jsonAlert = SmsClient.jsonize(alert);
                        SmsClient.LOGGER.debug("\t #5) SMS-shaped alert: %s",
                                alert);
                        SmsClient.logJson(jsonAlert);
                        sendAlert(this.users.get(tenantName), client,
                                authHeader, jsonAlert,
                                futureAlerts.get(prescriptionId));
                        LogManager
                                .getFormatterLogger("eu.connecare.rs.alertlog")
                                .info("%s", alert.toString());
                    }
                }
            }
            stepAlerts = CompositeFuture.join(Arrays
                    .asList(futureAlerts.values().toArray(new Future[] {})));
            return stepAlerts; // stepAlerts completion propagated to step5!
        });
        /*
         * FINAL step: end cycle
         */
        step5.compose(res5 -> {
            // CHECK how to check which futures failed amongst futureAlerts (res5 should be result) ISSUE #50
            SmsClient.LOGGER.info("SmsClientTest completed");
            startFuture.complete();
            return Future.succeededFuture();
        });
    }

    /**
     *
     */
    private Future<CompositeFuture> loadConfig() {
        Future<Buffer> fConfig = Future.future();
        this.vertx.executeBlocking(future -> {
            final Buffer b = this.vertx.fileSystem()
                    .readFileBlocking("RS-config.json");
            future.complete(b);
        }, result -> {
            if (result.succeeded()) {
                fConfig.complete((Buffer) result.result());
            } else {
                SmsClient.LOGGER.fatal("Failed to read config file: %s",
                        result.cause().getLocalizedMessage());
                fConfig.fail(Buffer.buffer(result.cause().getLocalizedMessage())
                        .toString());
            }
        });
        final Future<Boolean> jsonOk = fConfig.compose(res -> {
            final JsonObject config = new JsonObject(res);
            this.sendAlerts = config.getBoolean("sendAlerts");
            this.applicationId = config.getString("applicationId");
            this.userCtrl = config.getString("userCtrl");
            this.activityCtx = config.getString("activityCtx");
            this.alertCtx = config.getString("alertCtx");
            for (Object o : config.getJsonArray("lang")) {
                this.languages.add((String) o);
            }
            JsonObject json;
            for (Object o : config.getJsonArray("users")) {
                json = (JsonObject) o;
                this.users.put(json.getString("tenant-name"), null);
            }
            return Future.succeededFuture(true);
        });
        Future<Buffer> ipSecret = jsonOk.compose(res -> {
            Future<Buffer> ipOk = Future.future();
            this.vertx.executeBlocking(future -> {
                final Buffer b = this.vertx.fileSystem()
                        .readFileBlocking("/run/secrets/IP");
                future.complete(b);
            }, result -> {
                if (result.succeeded()) {
                    this.ip = result.result().toString().trim();
                    ipOk.complete((Buffer) result.result());
                } else {
                    SmsClient.LOGGER.fatal("Failed to read secret: %s",
                            result.cause().getLocalizedMessage());
                    ipOk.fail(
                            Buffer.buffer(result.cause().getLocalizedMessage())
                                    .toString());
                }
            });
            return ipOk;
        });
        return ipSecret.compose(res -> {
            final Map<String, Future<String>> futureTokens = new HashMap<>();
            for (String user : this.users.keySet()) {
                futureTokens.put(user, Future.future());
                this.vertx.executeBlocking(future -> {
                    final Buffer b = this.vertx.fileSystem().readFileBlocking(
                            "/run/secrets/" + user.toUpperCase());
                    future.complete(b);
                }, result -> {
                    if (result.succeeded()) {
                        String[] data = result.result().toString().split(" ");
                        this.users.put(user,
                                new UserLogin(hypenUuid(data[0].trim()),
                                        data[1].trim(), data[2].trim(),
                                        hypenUuid(data[3].trim()), user,
                                        hypenUuid(data[4].trim())));
                        Future<String> f = futureTokens.get(user);
                        f.complete("secret successful");
                    } else {
                        Future<String> f = futureTokens.get(user);
                        f.fail(String.format("Failed to read secret (%s): %s",
                                user, result.cause().getLocalizedMessage()));
                        SmsClient.LOGGER.fatal("Failed to read secret (%s): %s",
                                user, result.cause().getLocalizedMessage());
                    }
                });
            }
            CompositeFuture secrets = CompositeFuture.join(Arrays
                    .asList(futureTokens.values().toArray(new Future[] {})));
            return secrets;
        });
    }

    /**
     * @param ip
     * @param alertCtx
     * @param username
     * @param client
     * @param startFuture
     * @param authHeader
     * @param jsonAlert
     * @param futureAlert
     */
    private void sendAlert(UserLogin user, WebClient client, String authHeader,
            JsonObject jsonAlert, Future<String> futureAlert) { // WATCH OUT: removed by code cleaning!
        JsonObject postBody = new JsonObject();
        JsonArray alerts = new JsonArray();
        alerts.add(jsonAlert);
        postBody.put("alerts", alerts);
        logJson(postBody);
        final String path = String.format("/%s/advice", this.alertCtx);
        /*
         * NO port 8080! 1st header not used actually as per Postman collection
         */
        client.post(443, this.ip, path).ssl(true)
                .putHeader("Content-Type", "application/json")
                .putHeader("Authorization", authHeader)
                .putHeader("userName", user.getUsername())
                .sendJsonObject(postBody, postRes -> {
                    if (postRes.succeeded()) {
                        final HttpResponse<Buffer> response = postRes.result();
                        if (response.statusCode() == 200) { // resource created
                            // log
                            SmsClient.LOGGER.debug(
                                    "\t #5) Alert %s for tenant %s successfully published",
                                    postRes.result().bodyAsString(),
                                    user.getTenantName());
                            // CANNOT complete startFuture yet!
                            futureAlert.complete(String
                                    .valueOf(postRes.result().bodyAsString()));
                            SmsClient.LOGGER.trace(
                                    "\t #5) Future for tenant %s completed: %s (%s)",
                                    user.getTenantName(),
                                    futureAlert.succeeded(),
                                    futureAlert.result());
                        } else {
                            SmsClient.LOGGER.warn(
                                    "\t #5) Unexpected response code: %s %s",
                                    response.statusCode(),
                                    response.bodyAsString());
                            futureAlert.complete(response.bodyAsString());
                        }
                    } else {
                        SmsClient.LOGGER.fatal(
                                "\t #5) Something went wrong: %s",
                                postRes.cause());
                        futureAlert.fail(postRes.cause());
                    }
                });
    }

    /**
     * @param alert
     * @return
     */
    private static JsonObject jsonize(final SmsAlert alert) {
        JsonObject jsonAlert = null;
        try {
            jsonAlert = JsonObject.mapFrom(alert); // FIXME understand why Vertx swallows runtime exceptions
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        jsonAlert.put("creationDate", alert.getCreationDate()
                .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
        jsonAlert.put("expireDate",
                alert.getExpireDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
        jsonAlert.put("recipient", SmsClient.dehypenUuid(alert.getRecipient()));
        jsonAlert.put("target", SmsClient.dehypenUuid(alert.getTarget()));
        return jsonAlert;
    }

    /**
     * @param uuid
     * @return
     */
    private static String dehypenUuid(final UUID uuid) {
        return uuid.toString().replace("-", "");
    }

    /**
     * @param jsonRec
     * @return
     */
    private static SmsAlert recToAlert(final JsonObject jsonRec,
            Map<String, String> titles) {
        final Map<String, String> extras = new HashMap<>();
        extras.put("Description", jsonRec.getString("text"));
        extras.put("Title", titles.get(jsonRec.getString("lang")));
        final SmsAlert alert = new SmsAlert(jsonRec.getString("type"),
                OffsetDateTime.now()
                        .plus(SmsClient
                                .parseDuration(jsonRec.getValue("expiration"))),
                UUID.fromString(
                        jsonRec.getJsonObject("subject").getString("uuid")),
                UUID.fromString(
                        jsonRec.getJsonObject("subject").getString("uuid")),
                extras);
        return alert;
    }

    /**
     * @param jsonRec
     * @return
     */
    private static Map<String, String> loadTitlesTranslations() {
        final Map<String, String> titles = new HashMap<>();
        try (InputStream resourceAsStream = ClassLoader.getSystemClassLoader() // FIXME this.vertx.executeBlocking(future -> {
                .getResourceAsStream("rec-titles.json")) {
            JsonObject json = new JsonObject(
                    IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8));
            logJson(json);
            for (String lang : json.fieldNames()) {
                titles.put(lang, json.getJsonObject(lang).getString("title"));
            }
        } catch (IOException e) {
            SmsClient.LOGGER.fatal(
                    "Failed to read message title translations file: %s",
                    e.getLocalizedMessage());
        }
        return titles;
    }

    /**
     * @param object
     * @return
     */
    private static Duration parseDuration(final Object object) {
        final String[] parts = object.toString().split(":");
        return Duration.ofHours(Long.parseLong(parts[0]))
                .plusMinutes(Long.parseLong(parts[1]))
                .plusSeconds(Long.parseLong(parts[2]));
    }

    /**
     * @param r
     * @return
     */
    private static JsonObject jsonize(final Recommendation r) {
        JsonObject json = null;
        try {
            json = JsonObject.mapFrom(r); // FIXME understand why Vertx swallows runtime exceptions
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        final String s = DurationFormatUtils
                .formatDurationHMS(r.getExpiration().toMillis());
        json.put("expiration", s.substring(0, s.length() - 4));
        final JsonArray accomplishments = new JsonArray();
        final JsonArray adherence = new JsonArray();
        ActivityLevel<? extends Comparable<?>> al;
        for (final ActivityMetric metric : r) {
            al = r.getAccomplishment(metric);
            if (al != null) {
                accomplishments.add(new JsonObject().put(metric.toString(),
                        al.stringify()));
            } else {
                accomplishments
                        .add(new JsonObject().putNull(metric.toString()));
            }
            adherence.add(new JsonObject().put(metric.toString(),
                    r.getAdherence(metric).getAsPercentage()));
        }
        json.put("accomplishments", accomplishments);
        json.put("adherence", adherence);
        json.put("lang", r.getSubject().getLanguage());
        return json;
    }

    /**
     * @param r
     * @param userLogin
     * @return
     */
    private static JsonObject jsonize(final Recommendation r,
            final UserLogin userLogin) {
        JsonObject json = null;
        try {
            json = JsonObject.mapFrom(r); // FIXME understand why Vertx swallows runtime exceptions
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        json.put("type",
                String.format(
                        "advice.connecare.%s.AutomaticCustomRecommendations",
                        userLogin.getTenantName()));
        final String s = DurationFormatUtils
                .formatDurationHMS(r.getExpiration().toMillis());
        json.put("expiration", s.substring(0, s.length() - 4));
        final JsonArray accomplishments = new JsonArray();
        final JsonArray adherence = new JsonArray();
        ActivityLevel<? extends Comparable<?>> al;
        for (final ActivityMetric metric : r) {
            al = r.getAccomplishment(metric);
            if (al != null) {
                accomplishments.add(new JsonObject().put(metric.toString(),
                        al.stringify()));
            } else {
                accomplishments
                        .add(new JsonObject().putNull(metric.toString()));
            }
            adherence.add(new JsonObject().put(metric.toString(),
                    r.getAdherence(metric).getAsPercentage()));
        }
        json.put("accomplishments", accomplishments);
        json.put("adherence", adherence);
        json.put("lang", r.getSubject().getLanguage());
        return json;
    }

    /**
     * @param summary
     * @return
     */
    private static Accomplishment parseSummary(final Buffer summary) {
        final JsonObject json = summary.toJsonObject();
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> measurements = new HashMap<>();
        final UUID patientId = SmsClient.hypenUuid(json.getString("user")); // workaround from https://stackoverflow.com/questions/18986712/creating-a-uuid-from-a-string-with-no-dashes/19399768#19399768
        final JsonArray params = json.getJsonArray("params");
        ActivityLevel<? extends Comparable<?>> al;
        String key;
        Object val;
        for (int i = 0; i < params.size(); i++) {
            key = params.getJsonObject(i).getString("type");
            val = params.getJsonObject(i).getValue("value");
            try {
                al = SmsClient.parseActivityLevel(key, val, ChronoUnit.MINUTES);
                measurements.put(new ActivityMetric("physical", key), al);
            } catch (final UnknownMetricException e) {
                SmsClient.LOGGER.warn("\t\t %s: %s",
                        e.getClass().getSimpleName(), e.getMessage());
            }
        }
        OffsetDateTime date = null;
        try {
            date = OffsetDateTime.parse(json.getString("summaryDate"),
                    DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSSxx")); // mismatch between representations
        } catch (final DateTimeParseException e) {
            e.printStackTrace();
        }
        final Accomplishment a = new Accomplishment(patientId,
                new Patient(patientId), date.toLocalDateTime(), measurements);
        SmsClient.LOGGER.debug("\t #4) Parsed summary for patient %s (%s)",
                a.getSubject().getUuid(), a.toString());
        return a;
    }

    /**
     * @param key
     * @param val
     * @return
     * @throws UnknownMetricException
     */
    private static ActivityLevel<? extends Comparable<?>> parseActivityLevel(
            final String key, final Object val, ChronoUnit metric)
            throws UnknownMetricException {
        ActivityLevel<? extends Comparable<?>> al;
        switch (key) {
        case "steps":
            al = new DoubleLevel(Double.parseDouble(val.toString()));
            break;
        case "sedentary":
        case "light":
        case "moderate":
        case "vigorous":
            al = new DurationLevel(val.toString(), metric);
            break;
        default:
            throw new UnknownMetricException(String.format("metric: %s", key));
        }
        return al;
    }

    /**
     * @param prescription
     * @return
     */
    private static Prescription parsePrescription(final Buffer prescription) {
        final JsonObject json = prescription.toJsonObject();
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> goals = new HashMap<>();
        final UUID patientId = SmsClient.hypenUuid(json.getString("user")); // workaround from https://stackoverflow.com/questions/18986712/creating-a-uuid-from-a-string-with-no-dashes/19399768#19399768
        final JsonArray params = json.getJsonArray("params");
        ActivityLevel<? extends Comparable<?>> al;
        String key;
        Object val;
        for (int i = 0; i < params.size(); i++) {
            key = params.getJsonObject(i).getString("type");
            val = params.getJsonObject(i).getValue("value");
            try {
                al = SmsClient.parseActivityLevel(key, val, ChronoUnit.MINUTES);
                goals.put(new ActivityMetric("physical", key), al);
            } catch (final UnknownMetricException e) {
                SmsClient.LOGGER.warn("\t\t %s: %s", e.getClass(),
                        e.getMessage());
            }
        }
        OffsetDateTime start = null;
        OffsetDateTime end = null;
        try {
            start = OffsetDateTime.parse(json.getString("startDate"),
                    DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSSxx")); // mismatch between representations
            end = OffsetDateTime.parse(json.getString("endDate"),
                    DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSSxx")); // mismatch between representations
        } catch (final DateTimeParseException e) { // FIXME understand why Vertx swallows runtime exceptions
            e.printStackTrace();
        }
        final Prescription p = new Prescription(
                SmsClient.hypenUuid(json.getString("uuid")), // prescription id
                new Patient(patientId),
                new Clinician(
                        SmsClient.hypenUuid(json.getString("prescriber"))),
                start.toLocalDate(), end.toLocalDate(), goals);
        SmsClient.LOGGER.trace("\t #4) Parsed prescription for patient %s (%s)",
                p.getSubject().getUuid(), p.toString());
        return p;
    }

    /**
     * @param uuid
     * @return
     */
    private static UUID hypenUuid(final String uuid) {
        return UUID.fromString(uuid.replaceFirst(
                "(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)",
                "$1-$2-$3-$4-$5"));
    }

    private void getSummaries(final WebClient client,
            final Future<Void> startFuture, final String authHeader,
            String tenantName, Set<String> patients,
            final Map<String, Future<String>> summaries) {
        for (final String patientId : patients) {
            final String path = String.format(
                    "/%s/v1/summary/user/%s/date/%s/retrieve", this.activityCtx,
                    patientId,
                    OffsetDateTime
                            .now(ZoneId.ofOffset("", ZoneOffset.ofHours(1)))
                            .minusHours(1).withNano(0).format(DateTimeFormatter // FIXME .minusHours(1) is workaround for time zones issue
                                    .ofPattern("uuuu-MM-dd'T'HH:mm:ssxx")));
            final LocalMap<String, Buffer> summariesMap = this.vertx
                    .sharedData()
                    .getLocalMap(String.format("summariesMap-%s", tenantName));
            /*
             * NO port 8080! 1st header not used actually as per Postman
             * collection
             */
            client.get(443, this.ip, path).ssl(true)
                    .putHeader("Authorization", authHeader).send(getRes -> {
                        /*
                         * TODO what about timeouts?
                         * https://vertx.io/docs/vertx-core/java/#
                         * _making_requests -> "Request timeouts" or
                         * https://github.com/eclipse/vert.x/issues/2001
                         */
                        if (getRes.succeeded()) { // 404 error "No summaries found" still succeeds!
                            final HttpResponse<Buffer> response = getRes
                                    .result();
                            if (response.statusCode() == 200) {
                                // store
                                summariesMap.put(patientId,
                                        response.bodyAsBuffer()); // plain response (JSON object)
                                // log
                                SmsClient.LOGGER.debug(
                                        "\t #3) Received response #%s for patient %s with status code %s and body: %s",
                                        summariesMap.keySet().size(), patientId,
                                        response.statusCode(),
                                        response.bodyAsString());
                                // CANNOT complete startFuture yet!
                                final Future<String> f = summaries
                                        .get(patientId);
                                f.complete("summary found");
                                SmsClient.LOGGER.trace(
                                        "\t #3) Future for patient %s completed: %s (%s)",
                                        patientId, f.succeeded(), f.result());
                            } else if (response.statusCode() == 404
                                    && response.bodyAsString()
                                            .contains("No summaries found")) {
                                final Future<String> f = summaries
                                        .get(patientId);
                                f.complete("summary NOT found");
                                SmsClient.LOGGER.trace(
                                        "\t #3) Future for patient %s completed: %s (%s)",
                                        patientId, f.succeeded(), f.result());
                            } else {
                                SmsClient.LOGGER.warn(
                                        "\t #3) Unexpected response code: %s %s",
                                        response.statusCode(),
                                        response.bodyAsString());
                                final Future<String> f = summaries
                                        .get(patientId);
                                f.complete(response.bodyAsString()); // FIXME .getMessage() throws NPE
                            }
                        } else {
                            SmsClient.LOGGER.fatal(
                                    "\t #3) Something went wrong: %s",
                                    getRes.cause());
                            final Future<String> f = summaries.get(patientId);
                            f.fail(getRes.cause());
                            // startFuture.fail(getRes.cause()); // FIXME throws IllegalStateException
                            startFuture.tryFail(getRes.cause());
                        }
                    });
        }
    }

    private void getPrescriptions(final WebClient client,
            final Future<Void> startFuture, final String authHeader,
            final String tenantName, final List<Patient> patients,
            final Map<String, Future<String>> prescriptions) {
        final LocalMap<String, Buffer> prescriptionsMap = this.vertx
                .sharedData()
                .getLocalMap(String.format("prescriptionsMap-%s", tenantName));
        for (final Patient patient : patients) {
            final String path = String.format(
                    "/%s/v1/prescription/user/%s/date/%s/retrieve",
                    this.activityCtx, dehypenUuid(patient.getUuid()),
                    OffsetDateTime
                            .now(ZoneId.ofOffset("", ZoneOffset.ofHours(1)))
                            .minusHours(1).withNano(0).format(DateTimeFormatter // FIXME .minusHours(1) is workaround for time zones issue
                                    .ofPattern("uuuu-MM-dd'T'HH:mm:ssxx")));
            /*
             * NO port 8080! 1st header not used actually as per Postman
             * collection
             */
            client.get(443, this.ip, path).ssl(true)
                    .putHeader("Authorization", authHeader).send(getRes -> {
                        /*
                         * TODO what about timeouts?
                         * https://vertx.io/docs/vertx-core/java/#
                         * _making_requests -> "Request timeouts" or
                         * https://github.com/eclipse/vert.x/issues/2001
                         */
                        if (getRes.succeeded()) { // 404 error "No prescription found" still succeeds!
                            final HttpResponse<Buffer> response = getRes
                                    .result();
                            if (response.statusCode() == 200) {
                                // store
                                prescriptionsMap.put(
                                        dehypenUuid(patient.getUuid()),
                                        response.bodyAsBuffer()); // plain response (JSON object)
                                // log
                                SmsClient.LOGGER.debug(
                                        "\t #2) Received response #%s for patient %s with status code %s and body: %s",
                                        prescriptionsMap.keySet().size(),
                                        dehypenUuid(patient.getUuid()),
                                        response.statusCode(),
                                        response.bodyAsString());
                                // CANNOT complete startFuture yet!
                                final Future<String> f = prescriptions
                                        .get(dehypenUuid(patient.getUuid()));
                                f.complete("prescription found");
                                SmsClient.LOGGER.trace(
                                        "\t #2) Future for patient %s completed: %s (%s)",
                                        dehypenUuid(patient.getUuid()),
                                        f.succeeded(), f.result());
                            } else if (response.statusCode() == 404
                                    && response.bodyAsString().contains(
                                            "No prescription found")) {
                                final Future<String> f = prescriptions
                                        .get(dehypenUuid(patient.getUuid()));
                                f.complete("prescription NOT found");
                                SmsClient.LOGGER.trace(
                                        "\t #2) Future for patient %s completed: %s (%s)",
                                        dehypenUuid(patient.getUuid()),
                                        f.succeeded(), f.result());
                            } else {
                                SmsClient.LOGGER.warn(
                                        "\t #2) Unexpected response code: %s %s",
                                        response.statusCode(),
                                        response.bodyAsString());
                                final Future<String> f = prescriptions
                                        .get(dehypenUuid(patient.getUuid()));
                                f.complete(response.bodyAsString());
                            }
                        } else {
                            SmsClient.LOGGER.fatal(
                                    "\t #2) Something went wrong: %s",
                                    getRes.cause().getMessage());
                            final Future<String> f = prescriptions
                                    .get(dehypenUuid(patient.getUuid()));
                            f.fail(getRes.cause());
                            startFuture.fail(getRes.cause()); // CHECK will it be re-scheduled still?
                        }
                    });
        }
    }

    private static List<Patient> parsePatients(final JsonArray patientsJson) {
        final List<Patient> patients = new ArrayList<>();
        JsonObject json;
        UUID patientUuid;
        UUID roleUuid;
        String firstname;
        String lastname;
        Gender gender;
        String language;
        for (int i = 0; i < patientsJson.size(); i++) {
            json = patientsJson.getJsonObject(i);
            patientUuid = hypenUuid(json.getString("id"));
            roleUuid = hypenUuid(json.getJsonArray("roles").getString(0));
            firstname = json.getJsonObject("data").getString("firstname");
            lastname = json.getJsonObject("data").getString("lastname");
            if (json.getJsonObject("data").getString("gender") == null) {
                gender = Gender.UNKNOWN;
            } else {
                gender = Gender.valueOf(
                        json.getJsonObject("data").getString("gender"));
            }
            language = json.getJsonObject("data").getString("language");
            patients.add(new Patient(patientUuid, roleUuid, firstname, lastname,
                    gender, language));
        }
        return patients;
    }

    private void getPatients(String tenantName, final WebClient client,
            final Future<Void> startFuture, final String authHeader,
            final Map<String, Future<String>> futurePatients) {
        final String path = String.format("/%s/connecare/user/list/group/%s",
                this.userCtrl,
                dehypenUuid(this.users.get(tenantName).getGroupId()));
        final LocalMap<String, Buffer> patientsList = this.vertx.sharedData()
                .getLocalMap("patientsLists");
        /*
         * NO port 8080!
         */
        client.get(443, this.ip, path).ssl(true)
                .putHeader("Content-Type", "application/json")
                .putHeader("Authorization", authHeader).send(getRes -> {
                    if (getRes.succeeded()) {
                        HttpResponse<Buffer> response = getRes.result();
                        if (response.statusCode() == 200) {
                            // store
                            patientsList.put(tenantName,
                                    response.bodyAsBuffer()); // plain response (JSON array)
                            // CANNOT complete startFuture yet!
                            Future<String> f = futurePatients.get(tenantName);
                            f.complete("patients list found");
                            SmsClient.LOGGER.trace(
                                    "\t #1) Future for tenant %s completed: %s (%s)",
                                    tenantName, f.succeeded(), f.result());
                        } else {
                            SmsClient.LOGGER.warn(
                                    "\t #1) Unexpected response code: %s %s",
                                    response.statusCode(),
                                    response.bodyAsString());
                            Future<String> f = futurePatients.get(tenantName);
                            f.fail(response.bodyAsString());
                        }
                    } else {
                        SmsClient.LOGGER.fatal(
                                "\t #1) Something went wrong: %s",
                                getRes.cause().getMessage());
                        Future<String> f = futurePatients.get(tenantName);
                        f.fail(getRes.cause());
                        startFuture.fail(getRes.cause()); // CHECK will it be re-scheduled still?
                    }
                });
    }

    private static void logJson(final JsonArray bodyAsJsonArray) {
        for (int i = 0; i < bodyAsJsonArray.size(); i++) {
            SmsClient.LOGGER.trace("\t\t %s",
                    bodyAsJsonArray.getJsonObject(i).toString());
        }
    }

    /*
     * seems CORRECT
     */
    private static String parseToken(final JsonObject authJson) {
        return String.format("%s %s",
                WordUtils.capitalize(authJson.getString("token_type")),
                authJson.getString("access_token"));
    }

    private void getToken(final String tenantName, final WebClient client,
            final Future<Void> startFuture,
            final Map<String, Future<String>> futureTokens) {
        final String path = "/oauth/token";
        final UserLogin user = this.users.get(tenantName);
        final LocalMap<String, Buffer> authTokens = this.vertx.sharedData()
                .getLocalMap("authTokens");
        /*
         * NO port 8080!
         */
        client.post(443, this.ip, path).ssl(true)
                .putHeader("Content-Type", "application/x-www-form-urlencoded")
                .sendBuffer(Buffer.buffer(String.format(
                        "grant_type=password&username=%s&password=%s&tenant=%s&application=%s",
                        user.getUsername(), user.getPassword(),
                        dehypenUuid(user.getTenantId()), this.applicationId)),
                        postRes -> {
                            if (postRes.succeeded()) {
                                HttpResponse<Buffer> response = postRes
                                        .result();
                                if (response.statusCode() == 200) {
                                    // store
                                    authTokens.put(tenantName, Buffer.buffer(
                                            SmsClient.parseToken(response
                                                    .bodyAsJsonObject()))); // already parsed (String)
                                    Future<String> f = futureTokens
                                            .get(tenantName);
                                    f.complete("auth successful");
                                    SmsClient.LOGGER.trace(
                                            "\t #0) Future for tenant %s completed: %s (%s)",
                                            tenantName, f.succeeded(),
                                            f.result());
                                } else {
                                    SmsClient.LOGGER.warn(
                                            "\t #0) Unexpected response code: %s %s",
                                            response.statusCode(),
                                            response.bodyAsString());
                                    Future<String> f = futureTokens
                                            .get(tenantName);
                                    f.fail(response.bodyAsString());
                                }
                            } else {
                                SmsClient.LOGGER.fatal(
                                        "\t #0) Something went wrong: %s",
                                        postRes.cause().getMessage());
                                Future<String> f = futureTokens.get(tenantName);
                                f.fail(postRes.cause());
                                startFuture.fail(postRes.cause()); // CHECK will it be re-scheduled still?
                            }
                        });
    }

    private static void logJson(final JsonObject bodyAsJsonObject) {
        for (final String key : bodyAsJsonObject.fieldNames()) {
            if (bodyAsJsonObject.getValue(key) != null) {
                SmsClient.LOGGER.trace("\t\t %s : %s", key,
                        bodyAsJsonObject.getValue(key).toString());
            }
        }
    }

    @Override
    public void stop(final Future<Void> stopFuture) throws Exception {
        super.stop(stopFuture);
        SmsClient.LOGGER.info("SmsClientTest stopped");
    }

}
