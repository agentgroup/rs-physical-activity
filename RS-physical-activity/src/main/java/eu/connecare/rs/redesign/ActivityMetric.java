/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 4:54:35 PM
 */
package eu.connecare.rs.redesign;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlJavaTypeAdapter(ActivityMetricAdapter.class)
public final class ActivityMetric {

    private final String type;
    private final String name;

    /**
     *
     */
    @JsonCreator
    public ActivityMetric(@JsonProperty("activity") final String activityType,
            @JsonProperty("metric") final String metricName) {
        this.type = activityType;
        this.name = metricName;
    }

    /**
     * @return the type
     */
    public String getActivityType() {
        return this.type;
    }

    /**
     * @return the name
     */
    public String getMetricName() {
        return this.name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result
                + ((this.type == null) ? 0 : this.type.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ActivityMetric)) {
            return false;
        }
        final ActivityMetric other = (ActivityMetric) obj;
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!this.type.equals(other.type)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @JsonValue // from http://www.baeldung.com/jackson-map
    public String toString() {
        //        return String.format("ActivityMetric [type=%s, name=%s]", this.type,
        //                this.name);
        return String.format("%s_%s", this.type, this.name);
    }

}
