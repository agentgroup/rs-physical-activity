/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 3:56:36 PM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class PrescriptionNotFoundException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
