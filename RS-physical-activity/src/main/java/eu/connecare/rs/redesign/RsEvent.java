/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:08:41 AM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;
import java.util.Iterator;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public class RsEvent<T> implements IRsEvent<T> { // no uuid because goes into alert type which has uuid

    protected final T type;
    protected final Patient subject;
    protected final String text;
    protected final Duration expiration;
    protected final Map<ActivityMetric, Adherence> adherence;
    protected final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishment;

    /**
     *
     */
    public RsEvent(final T type, final Patient patient, final String text,
            final Duration expirationTime,
            final Map<ActivityMetric, Adherence> adherenceToMetrics,
            final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishmentOfMetrics) {
        this.type = type;
        this.subject = patient;
        this.text = text;
        this.expiration = expirationTime;
        this.adherence = ImmutableMap.copyOf(adherenceToMetrics);
        this.accomplishment = accomplishmentOfMetrics; // fix for case in which a prescription metric is NOT in the accomplishments
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<ActivityMetric> iterator() {
        return this.accomplishment.keySet().iterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getType() {
        return this.type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Patient getSubject() {
        return this.subject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getText() {
        return this.text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Duration getExpiration() {
        return this.expiration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Adherence getAdherence(final ActivityMetric metric) {
        return this.adherence.get(metric);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ActivityLevel<?> getAccomplishment(final ActivityMetric metric) {
        return this.accomplishment.get(metric);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.adherence == null) ? 0 : this.adherence.hashCode());
        result = prime * result
                + ((this.expiration == null) ? 0 : this.expiration.hashCode());
        result = prime * result + ((this.accomplishment == null) ? 0
                : this.accomplishment.hashCode());
        result = prime * result
                + ((this.subject == null) ? 0 : this.subject.hashCode());
        result = prime * result
                + ((this.text == null) ? 0 : this.text.hashCode());
        result = prime * result
                + ((this.type == null) ? 0 : this.type.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof RsEvent)) {
            return false;
        }
        final RsEvent<T> other = (RsEvent<T>) obj;
        if (this.adherence == null) {
            if (other.adherence != null) {
                return false;
            }
        } else if (!this.adherence.equals(other.adherence)) {
            return false;
        }
        if (this.expiration == null) {
            if (other.expiration != null) {
                return false;
            }
        } else if (!this.expiration.equals(other.expiration)) {
            return false;
        }
        if (this.accomplishment == null) {
            if (other.accomplishment != null) {
                return false;
            }
        } else if (!this.accomplishment.equals(other.accomplishment)) {
            return false;
        }
        if (this.subject == null) {
            if (other.subject != null) {
                return false;
            }
        } else if (!this.subject.equals(other.subject)) {
            return false;
        }
        if (this.text == null) {
            if (other.text != null) {
                return false;
            }
        } else if (!this.text.equals(other.text)) {
            return false;
        }
        if (this.type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!this.type.equals(other.type)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "AbstractRsEvent [type=%s, subject=%s, text=%s, expiration=%s, adherence=%s, accomplishment=%s]",
                this.type, this.subject, this.text, this.expiration,
                this.adherence, this.accomplishment);
    }

}
