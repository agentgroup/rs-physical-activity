/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 11:01:02 PM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class DoubleLevel extends AbstractActivityLevel<Double> {

    /**
     * @param value
     */
    //    @JsonCreator
    public DoubleLevel(/* @JsonProperty("value") */final Double value) {
        super(value);
    }

    /**
     * @param value
     */
    @JsonCreator
    public DoubleLevel(/* @JsonProperty("value") */final String value) {
        super(Double.parseDouble(value));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ActivityLevel<Double> accumulate(final ActivityLevel<Double> toAdd) {
        if (toAdd != null) { // fix for case in which a prescription metric is NOT in the accomplishments
            return new DoubleLevel(this.value + toAdd.getValue()); // otherwise original value is not immutable, see computeAdherenceDetail() in Strategy
        }
        return new DoubleLevel(this.value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double percentageDone(final ActivityLevel<Double> target) {
        return this.value * 100 / target.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ActivityLevel<Double> redistribute(final Duration available,
            final ActivityMetric metric) {
        switch (metric.getActivityType()) {
        case "physical":
            return new DoubleLevel(this.value / available.toDays());
        default:
            // TODO throw
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ActivityLevel<Double> remainingTo(final ActivityLevel<Double> target,
            final ActivityMetric metric) {
        return new DoubleLevel(target.getValue() - this.value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String stringify() {
        return this.value.toString();
    }

    //    /**
    //     * {@inheritDoc}
    //     */
    //    @Override
    //    @JsonValue
    //    public String toString() {
    //        return super.toString();
    //    }

}
