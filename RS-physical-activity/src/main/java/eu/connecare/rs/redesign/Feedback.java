/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:35:38 AM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;
import java.util.Map;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class Feedback extends RsEvent<FeedType> {

    private final Clinician recipient;

    /**
     * @param type
     * @param patient
     * @param text
     * @param expirationTime
     * @param adherenceToMetrics
     * @param accomplishmentOfMetrics
     */
    public Feedback(final FeedType type, final Patient patient,
            final String text, final Duration expirationTime,
            final Map<ActivityMetric, Adherence> adherenceToMetrics,
            final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishmentOfMetrics,
            final Clinician recipient) {
        super(type, patient, text, expirationTime, adherenceToMetrics,
                accomplishmentOfMetrics);
        this.recipient = recipient;
    }

    public Clinician getRecipient() {
        return this.recipient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((this.recipient == null) ? 0 : this.recipient.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof Feedback)) {
            return false;
        }
        final Feedback other = (Feedback) obj;
        if (this.recipient == null) {
            if (other.recipient != null) {
                return false;
            }
        } else if (!this.recipient.equals(other.recipient)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "Feedback [recipient=%s, type=%s, subject=%s, text=%s, expiration=%s, adherence=%s, accomplishment=%s]",
                this.recipient, this.type, this.subject, this.text,
                this.expiration, this.adherence, this.accomplishment);
    }

}
