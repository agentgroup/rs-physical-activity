/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 3:27:19 PM
 */
package eu.connecare.rs.redesign;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

import advice.connecare.rs.redesign.RecType;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface IPolicy {

    /**
     * @param latestAccomplishment
     * @return
     * @throws UnknownAdherenceException
     */
    Recommendation computeRecommendation(Prescription prescription,
            Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishmentSummary,
            Map<ActivityMetric, Adherence> detailedAdherence,
            Adherence adherence, LocalDateTime latestAccomplishment);

    /**
     * @return
     */
    Feedback computeFeedback();

    boolean shouldDeliver(/*String msg, */RecType rec/*, LocalDate deadline*/);

}
