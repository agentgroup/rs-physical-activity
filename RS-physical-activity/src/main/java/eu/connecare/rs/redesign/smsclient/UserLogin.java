/**
 * Created by sm (stefano.mariani@unimore.it) on Aug 29, 2018 11:58:40 AM
 */
package eu.connecare.rs.redesign.smsclient;

import java.util.UUID;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class UserLogin {

    private final UUID uuid;
    private final String username;
    private final String password;
    private final UUID tenantId;
    private final String tenantName;
    private final UUID groupId;

    /**
     * 
     * @param uuid
     * @param username
     * @param password
     * @param tenantId
     * @param tenantName
     * @param groupId
     */
    public UserLogin(UUID uuid, String username, String password, UUID tenantId,
            String tenantName, UUID groupId) {
        this.uuid = uuid;
        this.username = username;
        this.password = password;
        this.tenantId = tenantId;
        this.tenantName = tenantName;
        this.groupId = groupId;
    }

    /**
     * @return the uuid
     */
    public UUID getUuid() {
        return this.uuid;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @return the tenantId
     */
    public UUID getTenantId() {
        return this.tenantId;
    }

    /**
     * @return the tenantName
     */
    public String getTenantName() {
        return this.tenantName;
    }

    /**
     * @return the groupId
     */
    public UUID getGroupId() {
        return this.groupId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.uuid == null) ? 0 : this.uuid.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UserLogin)) {
            return false;
        }
        UserLogin other = (UserLogin) obj;
        if (this.uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "UserLogin [uuid=%s, username=%s, password=%s, tenantId=%s, tenantName=%s, groupId=%s]",
                this.uuid, this.username, this.password, this.tenantId,
                this.tenantName, this.groupId);
    }

}
