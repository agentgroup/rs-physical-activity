/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 18, 2018 11:46:16 AM
 */
package eu.connecare.rs.redesign;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BinaryOperator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.text.StringSubstitutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import advice.connecare.rs.redesign.RecType;
import io.vertx.core.json.JsonObject;

/**
 * @author sm (stefano.mariani@unimore.it)
 */
public final class NlgEngine {

    private static final Logger LOGGER = LogManager.getFormatterLogger();

    private final Map<RecType, Map<String, List<String>>> sentences;
    private final Map<PosType, List<String>> pos;
    private final Map<String, String> times;
    private final Map<String, String> activities;
    private final Random rand;

    /**
     * @param sentenceTemplates
     * @param timesFile
     * @param activitiesFile
     *
     */
    public NlgEngine(final Path sentenceTemplates, final Path posFile,
            final Path timesFile, final Path activitiesFile) {
        this.sentences = NlgEngine.loadSentences(sentenceTemplates);
        NlgEngine.LOGGER.trace("sentenceTemplates: %s", this.sentences);
        this.pos = NlgEngine.loadPos(posFile);
        NlgEngine.LOGGER.trace("posFile: %s", this.pos);
        this.times = loadTimes(timesFile, posFile.getFileName());
        NlgEngine.LOGGER.trace("timesFile: %s", this.times);
        this.activities = loadActivities(activitiesFile, posFile.getFileName());
        NlgEngine.LOGGER.trace("activitiesFile: %s", this.activities);
        this.rand = new Random();
    }

    /**
     * @param activitiesFile
     * @param fileName
     * @return
     */
    private static Map<String, String> loadActivities(Path activitiesFile,
            Path lang) {
        final Map<String, String> activities = new HashMap<>();
        try (InputStream resourceAsStream = ClassLoader.getSystemClassLoader()
                .getResourceAsStream(activitiesFile.toString())) {
            JsonObject json = new JsonObject(
                    IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8));
            JsonObject trans = json.getJsonObject(
                    lang.toString().substring(0, lang.toString().length() - 8)); // "_pos.txt"
            for (String field : trans.fieldNames()) {
                activities.put(field, trans.getString(field));
            }
        } catch (IOException e) {
            // TODO handle
            e.printStackTrace();
        }
        return activities;
    }

    /**
     * @param timesFile
     * @param lang
     * @return
     */
    private static Map<String, String> loadTimes(Path timesFile, Path lang) {
        final Map<String, String> times = new HashMap<>();
        try (InputStream resourceAsStream = ClassLoader.getSystemClassLoader()
                .getResourceAsStream(timesFile.toString())) {
            JsonObject json = new JsonObject(
                    IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8));
            JsonObject trans = json.getJsonObject(
                    lang.toString().substring(0, lang.toString().length() - 8)); // "_pos.txt"
            for (String field : trans.fieldNames()) {
                times.put(field, trans.getString(field));
            }
        } catch (IOException e) {
            // TODO handle
            e.printStackTrace();
        }
        return times;
    }

    /**
     * @param posFile
     * @return
     */
    private static Map<PosType, List<String>> loadPos(Path posFile) {
        final Map<PosType, List<String>> loaded = new HashMap<>();
        try (final BufferedReader br = new BufferedReader(
                new InputStreamReader(ClassLoader.getSystemClassLoader()
                        .getResourceAsStream(posFile.toString())))) {
            String line = br.readLine();
            List<String> pos = null;
            PosType pt;
            while (line != null) {
                if (!line.isEmpty()) {
                    /*
                     * TODO how to detect wrong enum (i.e. typo)? Give structure
                     * to file (JSON or XML)
                     */
                    if (EnumUtils.isValidEnum(PosType.class, line.trim())) {
                        pt = PosType.valueOf(line.trim());
                        pos = loaded.get(pt);
                        if (pos == null) {
                            pos = new ArrayList<>();
                            loaded.put(pt, pos);
                        }
                    } else {
                        pos.add(line.trim());
                    }
                }
                line = br.readLine();
            }
        } catch (final IOException e) {
            // TODO throw
            e.printStackTrace();
        }
        return loaded;
    }

    /**
     * @param prescription
     * @param accomplishmentSummary
     * @param detailedAdherence
     * @param rt
     * @return
     */
    @SuppressWarnings("null")
    public String generateMessage(final Prescription prescription,
            final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishmentSummary,
            final Map<ActivityMetric, Adherence> detailedAdherence,
            final RecType rt) {
        // get admissible sentence templates for given rec type
        final Map<String, List<String>> pool = this.sentences.get(rt);
        final Map<String, String> actuals = new HashMap<>();
        // handle posFile
        handleGreetings(actuals); // < 12 morning, > 12 < 18 afternoon, > 18 evening
        // perform substitutions according to fullfillment and adherence
        actuals.put("rec.target", prescription.getSubject().getFirstName());
        ActivityMetric metric = null;
        switch (rt) {
        case BLAME:
        case ALERT:
        case WARNING:
        case MOTIVATIONAL:
            metric = new ActivityMetric("physical", "steps");
            break;
        case AWARD:
            metric = NlgEngine.findMetric(detailedAdherence, Double::max);
            break;
        default:
            // TODO throw
            break;
        }
        String template;
        List<String> phrases;
        if ("sedentary".equals(metric.getMetricName())) {
            phrases = pool.get("NEG");
            template = phrases.get(this.rand.nextInt(phrases.size()));
        } else {
            phrases = pool.get("STD");
            template = phrases.get(this.rand.nextInt(phrases.size()));
        }
        ActivityLevel<? extends Comparable<?>> goal = prescription
                .getActivityLevels().get(metric);
        if (goal instanceof DurationLevel) {
            actuals.put("rec.goal", asNlgString((DurationLevel) goal));
        } else {
            actuals.put("rec.goal", goal.stringify());
        }
        ActivityLevel<? extends Comparable<?>> al = accomplishmentSummary
                .get(metric);
        String nlg;
        if (al == null) { // fix for case in which a prescription metric is NOT in the accomplishments
            actuals.put("rec.level", String.valueOf(0.0));
            if (al instanceof DurationLevel) {
                nlg = asNlgString((DurationLevel) goal);
            } else {
                nlg = goal.stringify();
            }
            actuals.put("rec.remaining", nlg);
        } else {
            if (al instanceof DoubleLevel) {
                actuals.put("rec.level", al.stringify());
                al = ((DoubleLevel) al).remainingTo(((DoubleLevel) prescription
                        .getActivityLevels().get(metric)), metric);
                nlg = al.stringify();
            } else {
                actuals.put("rec.level", asNlgString((DurationLevel) al));
                al = ((DurationLevel) al)
                        .remainingTo(
                                ((DurationLevel) prescription
                                        .getActivityLevels().get(metric)),
                                metric);
                nlg = asNlgString((DurationLevel) al);
            }
            actuals.put("rec.remaining", nlg);
        }
        actuals.put("rec.metric", this.activities.get("activity") + " "
                + this.activities.get(metric.getMetricName()));
        return new StringSubstitutor(actuals).replace(template);
    }

    /**
     * @param timeLeft
     * @return
     */
    private String asNlgString(DurationLevel timeLeft) {
        String nlg = "";
        boolean h = false;
        if (timeLeft.getValue().toHours() > 24) {
            nlg += (timeLeft.getValue().toDays() + " "
                    + this.times.get("days"));
        } else {
            if (timeLeft.getValue().toHours() > 0) {
                nlg += (timeLeft.getValue().toHours() + " "
                        + this.times.get("hours"));
                h = true;
            }
            Duration minutes = timeLeft.getValue()
                    .minusHours(timeLeft.getValue().toHours());
            if (minutes.toMinutes() > 0) {
                if (h) {
                    nlg += " ";
                }
                nlg += (minutes.toMinutes() + " " + this.times.get("minutes"));
            }
        }
        if (nlg.isEmpty()) {
            nlg += "0";
        }
        return nlg;
    }

    /**
     * @param actuals
     * @param template
     */
    private void handleGreetings(Map<String, String> actuals) {
        List<String> posPool;
        for (PosType p : PosType.values()) {
            posPool = this.pos.get(p);
            final String winPos = posPool
                    .get(this.rand.nextInt(posPool.size()));
            actuals.put(StringUtils.lowerCase(p.toString()), winPos);
        }
        posPool = this.pos.get(PosType.GREETING);
        if (OffsetTime.now()
                .isAfter(LocalTime.MIDNIGHT.atOffset(ZoneOffset.ofHours(1)))
                && OffsetTime.now().isBefore(
                        LocalTime.NOON.atOffset(ZoneOffset.ofHours(1)))) {
            posPool.addAll(this.pos.get(PosType.GREETING_MORNING));
            actuals.put(StringUtils.lowerCase(PosType.GREETING.toString()),
                    posPool.get(this.rand.nextInt(posPool.size())));
        } else if (OffsetTime.now()
                .isAfter(LocalTime.NOON.atOffset(ZoneOffset.ofHours(1)))
                && OffsetTime.now().isBefore(
                        LocalTime.of(18, 00).atOffset(ZoneOffset.ofHours(1)))) {
            posPool.addAll(this.pos.get(PosType.GREETING_AFTERNOON));
            actuals.put(StringUtils.lowerCase(PosType.GREETING.toString()),
                    posPool.get(this.rand.nextInt(posPool.size())));
        } else if (OffsetTime.now()
                .isAfter(LocalTime.of(18, 00).atOffset(ZoneOffset.ofHours(1)))
                && OffsetTime.now().isBefore(
                        LocalTime.MIDNIGHT.atOffset(ZoneOffset.ofHours(1)))) {
            posPool.addAll(this.pos.get(PosType.GREETING_EVENING));
            actuals.put(StringUtils.lowerCase(PosType.GREETING.toString()),
                    posPool.get(this.rand.nextInt(posPool.size())));
        }
    }

    /**
     * @param detailedAdherence
     * @param op
     * @return
     */
    public static ActivityMetric findMetric(
            final Map<ActivityMetric, Adherence> detailedAdherence,
            final BinaryOperator<Double> op) {
        final double result = detailedAdherence.values().stream()
                .map(a -> a.getAsPercentage()).reduce(op).get(); // combining https://stackoverflow.com/questions/37348462/find-minimum-value-in-a-map-java and https://docs.oracle.com/javase/8/docs/api/java/util/stream/DoubleStream.html#max--
        for (final ActivityMetric key : detailedAdherence.keySet()) {
            if (detailedAdherence.get(key).getAsPercentage() == result) {
                return key;
            }
        }
        // TODO throw
        return null;
    }

    /**
     * @param detailedAdherence
     * @return
     */
    public static ActivityMetric closestToAverageMetric(
            final Map<ActivityMetric, Adherence> detailedAdherence) {
        final Mean avg = new Mean();
        for (final ActivityMetric key : detailedAdherence.keySet()) {
            if (!key.getMetricName().equals("sedentary")) { // fix for sedentary activity reverted logic (https://gitlab.com/AgMore/Connecare-RS-physical/issues/25)
                avg.increment((detailedAdherence.get(key).getAsPercentage()));
            }
        }
        double minDiff = Double.MAX_VALUE;
        double absDiff = 0; // to silence compiler
        ActivityMetric minDiffMetric = null;
        for (final ActivityMetric key : detailedAdherence.keySet()) {
            if (!key.getMetricName().equals("sedentary")) { // fix for sedentary activity reverted logic (https://gitlab.com/AgMore/Connecare-RS-physical/issues/25)
                absDiff = Math.abs(detailedAdherence.get(key).getAsPercentage()
                        - avg.getResult());
                if (absDiff < minDiff) {
                    minDiff = absDiff;
                    minDiffMetric = key;
                }
            }
        }
        return minDiffMetric;
    }

    /**
     * @param sentenceTemplates
     * @return
     */
    private static Map<RecType, Map<String, List<String>>> loadSentences(
            final Path sentenceTemplates) {
        final Map<RecType, Map<String, List<String>>> loaded = new HashMap<>();
        try (final BufferedReader br = new BufferedReader(
                new InputStreamReader(ClassLoader.getSystemClassLoader()
                        .getResourceAsStream(sentenceTemplates.toString())))) {
            String line = br.readLine();
            Map<String, List<String>> multiPhrases = null;
            List<String> phrases = null;
            RecType rt;
            while (line != null) {
                if (!line.isEmpty()) {
                    /*
                     * TODO how to detect wrong enum (i.e. typo)? Give structure
                     * to rec-sentence-templates file (JSON or XML)
                     */
                    if (EnumUtils.isValidEnum(RecType.class, line.trim())) {
                        rt = RecType.valueOf(line.trim());
                        multiPhrases = loaded.get(rt);
                        if (multiPhrases == null) {
                            multiPhrases = new HashMap<>();
                            loaded.put(rt, multiPhrases);
                        }
                    } else {
                        if (line.startsWith("NEG")) {
                            phrases = multiPhrases.get("NEG");
                            if (phrases == null) {
                                phrases = new ArrayList<>();
                                multiPhrases.put("NEG", phrases);
                            }
                        } else {
                            phrases = multiPhrases.get("STD");
                            if (phrases == null) {
                                phrases = new ArrayList<>();
                                multiPhrases.put("STD", phrases);
                            }
                        }
                        phrases.add(line.substring(3).trim());
                    }
                }
                line = br.readLine();
            }
        } catch (final IOException e) {
            // TODO throw
            e.printStackTrace();
        }
        return loaded;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.sentences == null) ? 0 : this.sentences.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof NlgEngine)) {
            return false;
        }
        final NlgEngine other = (NlgEngine) obj;
        if (this.sentences == null) {
            if (other.sentences != null) {
                return false;
            }
        } else if (!this.sentences.equals(other.sentences)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("NlgEngine [sentences=%s]", this.sentences);
    }

}