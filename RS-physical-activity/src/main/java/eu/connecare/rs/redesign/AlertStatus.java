/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 11:31:03 PM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public enum AlertStatus {

    ACT("active"), DIS("dismissed"), EXP("dismissed"), RES("resolved");

    private final String value;

    /**
     * 
     */
    private AlertStatus(String label) {
        this.value = label;
    }

    public String fullString() {
        return this.value;
    }

}
