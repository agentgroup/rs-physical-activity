/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 8, 2018 3:34:58 PM
 */
package eu.connecare.rs.redesign;

import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import advice.connecare.rs.redesign.RecType;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class Policy implements IPolicy { // TODO equals, hashCode, toString

    private static final Logger LOGGER = LogManager.getFormatterLogger();
    private final NlgEngine nlg;
    private static final long availableMinutes = Duration.ofMinutes(60 * 14)
            .toMinutes(); // 8-22 time frame

    /**
     * @param timeWeight
     * @param sentenceTemplates
     * @param timesFile
     * @param activitiesFile
     */
    public Policy(final Path sentenceTemplates, final Path posFile,
            final Path timesFile, final Path activitiesFile) {
        this.nlg = new NlgEngine(sentenceTemplates, posFile, timesFile,
                activitiesFile);
    }

    /**
     * {@inheritDoc}
     *
     * @throws UnknownAdherenceException
     */
    @Override
    public Recommendation computeRecommendation(final Prescription prescription,
            final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> accomplishmentSummary,
            final Map<ActivityMetric, Adherence> detailedAdherence,
            final Adherence adherence,
            final LocalDateTime latestAccomplishment) {
        final RecType rt = Policy.computeRecType(detailedAdherence, adherence,
                latestAccomplishment);
        final String msg = this.nlg.generateMessage(prescription,
                accomplishmentSummary, detailedAdherence, rt);
        Recommendation r = new Recommendation(rt, prescription.getSubject(),
                msg, Duration.ofHours(24), detailedAdherence,
                accomplishmentSummary);
        return r;
    }

    /**
     * @param adherence
     * @param latestAccomplishment
     * @param weightedAd
     * @return
     */
    public static RecType computeRecType(
            final Map<ActivityMetric, Adherence> detailedAdherence,
            final Adherence adherence,
            final LocalDateTime latestAccomplishment) {
        final long elapsedMinutes = Duration
                .between(LocalTime.of(8, 0, 0), LocalTime.now())
                .toMinutes();
        final ActivityMetric m = NlgEngine.findMetric(detailedAdherence,
                Double::max);
        if (detailedAdherence.get(m)
                .compareTo(new PercentageAdherence(99)) == 1) {
            return RecType.AWARD;
        }
        if (elapsedMinutes >= availableMinutes) {
            return RecType.BLAME;
        }
        final double optimalAdherence = (100.0 / availableMinutes)
                * elapsedMinutes;
        final Adherence steps = detailedAdherence
                .get(new ActivityMetric("physical", "steps"));
        if (steps.compareTo(
                new PercentageAdherence(optimalAdherence * 0.75)) >= 0) {
            return RecType.MOTIVATIONAL;
        } else if (steps.compareTo(
                new PercentageAdherence(optimalAdherence * 0.5)) >= 0) {
            return RecType.WARNING;
        } else {
            return RecType.ALERT;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Feedback computeFeedback() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public boolean shouldDeliver(
            /*String msg, */RecType rec/*, LocalDate deadline*/) {
        int hourNow = LocalTime.now().getHour();
        return rec == RecType.AWARD || rec == RecType.BLAME || hourNow == 12
                || hourNow == 16 || hourNow == 20;
        //        if (msg.contains("sedentary")) {
        //            return true;
        //        }
        //        if (deadline.isAfter(LocalDate.now().plusDays(6))) { // TODO make configurable through endpoint (as part of Policy config)
        //            if (rec == RecType.MOTIVATIONAL || rec == RecType.AWARD) {
        //                return true;
        //            }
        //            return false;
        //        }
        //        return true;
    }

}
