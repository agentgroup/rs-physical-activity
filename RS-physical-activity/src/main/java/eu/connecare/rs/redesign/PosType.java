/**
 * Created by sm (stefano.mariani@unimore.it) on Jul 23, 2018 5:33:40 PM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public enum PosType {

    GREETING, GREETING_MORNING, GREETING_AFTERNOON, GREETING_EVENING, FILLER, ASSESSMENT_POSITIVE, ASSESSMENT_MOTIVATIONAL, ASSESSMENT_NEGATIVE, ASSESSMENT_FAILED, ASSESSMENT_TOO_MUCH, CALL_TO_ACTION, NEXT_TIME, WARNING, AWARD, ARGUMENT_DATE, ARGUMENT_TIME;

}
