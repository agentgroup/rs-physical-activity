/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 3:25:42 PM
 */
package eu.connecare.rs.redesign;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface IStrategy {

    /**
     * @param list
     * @param prescription
     * @return
     * @throws UnknownActivityLevelException
     */
    Map<ActivityMetric, Adherence> computeAdherenceDetail(
            Prescription prescription, List<Accomplishment> list)
            throws UnknownActivityLevelException;

    /**
     * @param list
     * @param prescription
     * @return
     * @throws UnknownActivityLevelException
     * @throws UnknownAdherenceException
     */
    Adherence computeAdherence(Prescription prescription,
            List<Accomplishment> list) throws UnknownActivityLevelException;

    /**
     * @return the adherence
     */
    Map<UUID, Adherence> getAdherence();

    /**
     * @return the detailedAdherence
     */
    Map<UUID, Map<ActivityMetric, Adherence>> getDetailedAdherence();

    /**
     * @return the fullfillmentsSummary
     */
    Map<UUID, Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>>> getAccomplishmentSummary();

}
