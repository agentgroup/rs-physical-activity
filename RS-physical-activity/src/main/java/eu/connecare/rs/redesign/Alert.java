/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 11:33:36 PM
 */
package eu.connecare.rs.redesign;

import java.time.LocalDate;
import java.util.UUID;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class Alert<T extends IRsEvent<?>> implements IAlert<T> {

    private final UUID uuid;
    private final T event;
    private final int priority;
    private final User recipient;
    private final Patient subject;
    private final LocalDate creation;
    private final LocalDate expiration;
    private final AlertStatus status;

    /**
     *
     */
    public Alert(final T event, final int priority, final User recipient,
            final Patient patient, final LocalDate creationDate,
            final LocalDate expirationDate, final AlertStatus alertStatus) {
        this.uuid = UUID.randomUUID();
        this.event = event;
        this.priority = priority;
        this.recipient = recipient;
        this.subject = patient;
        this.creation = creationDate;
        this.expiration = expirationDate;
        this.status = alertStatus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UUID getUuid() {
        return this.uuid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getEvent() {
        return this.event;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPriority() {
        return this.priority;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getRecipient() {
        return this.recipient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Patient getSubject() {
        return this.subject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDate getCreation() {
        return this.creation;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDate getExpiration() {
        return this.expiration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AlertStatus getStatus() {
        return this.status;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(final IAlert<T> other) {
        return this.priority - other.getPriority();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.uuid == null) ? 0 : this.uuid.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Alert)) {
            return false;
        }
        final Alert<T> other = (Alert<T>) obj;
        if (this.uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "Alert [uuid=%s, event=%s, priority=%s, recipient=%s, subject=%s, creation=%s, expiration=%s, status=%s]",
                this.uuid, this.event, this.priority, this.recipient,
                this.subject, this.creation, this.expiration, this.status);
    }

}
