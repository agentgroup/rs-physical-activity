/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 11:15:37 PM
 */
package eu.connecare.rs.redesign;

import java.time.LocalDate;
import java.util.UUID;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface IAlert<T extends IRsEvent<?>> extends Comparable<IAlert<T>> {

    UUID getUuid();

    T getEvent();

    int getPriority();

    User getRecipient();

    Patient getSubject();

    LocalDate getCreation();

    LocalDate getExpiration();

    AlertStatus getStatus();

}
