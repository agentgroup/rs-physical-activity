/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 5:27:44 PM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 * @param <T>
 */
@XmlRootElement
/*
 * from
 * https://stackoverflow.com/questions/30362446/deserialize-json-with-jackson-
 * into-polymorphic-types-a-complete-example-is-giv/30386694#30386694
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
//@JsonSubTypes({
//        @JsonSubTypes.Type(value = DoubleLevel.class, name = "DoubleLevel"),
//        @JsonSubTypes.Type(value = DurationLevel.class, name = "DurationLevel") })
@JsonSubTypes(@JsonSubTypes.Type(value = AbstractActivityLevel.class, name = "AbstractActivityLevel"))
public interface ActivityLevel<T extends Comparable<T>>
        extends Comparable<ActivityLevel<T>> {

    //    @XmlElement // from https://stackoverflow.com/questions/20755523/jersey-json-jaxb-ignoring-or-not-seeing-some-public-properties-in-pojo
    //    @XmlJavaTypeAdapter(DurationAdapter.class) // from https://stackoverflow.com/questions/27314093/how-to-map-java-time-duration-to-xml
    T getValue();

    ActivityLevel<T> accumulate(ActivityLevel<T> toAdd);

    /**
     * Redistributes {@code this} {@code ActivityLevel<T>} to the
     * {@code available} time on a basis depending on the metric (i.e.
     * "redistribute to a week a physical activity", given that physical
     * activity metric assumes a daily basis)
     *
     * @param available
     * @param unit
     * @return
     */
    ActivityLevel<T> redistribute(Duration available, ActivityMetric metric);

    ActivityLevel<T> remainingTo(ActivityLevel<T> target,
            ActivityMetric metric);

    double percentageDone(ActivityLevel<T> target);
    
    String stringify();

    /**
     * {@inheritDoc}
     */
    @Override
    default int compareTo(final ActivityLevel<T> other) {
        return this.getValue().compareTo(other.getValue());
    }

}