/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:57:03 AM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface AdherenceProfile extends Iterable<TimeWindow> {

    Adherence getAdherence(TimeWindow window);

}
