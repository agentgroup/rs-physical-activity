/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 4:47:08 PM
 */
package eu.connecare.rs.redesign;

import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class Clinician implements User {

    @JsonValue
    private final UUID uuid;
    @JsonValue
    private String firstName;

    /**
    *
    */
    public Clinician(/* @JsonProperty("uuid") */final UUID uuid) {
        this.uuid = uuid;
    }

    /**
     *
     */
    @JsonCreator
    public Clinician(/* @JsonProperty("uuid") */final UUID uuid,
            final String name) {
        this.uuid = uuid;
        this.firstName = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UUID getUuid() {
        return this.uuid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFirstName(String name) {
        this.firstName = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.uuid == null) ? 0 : this.uuid.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Clinician)) {
            return false;
        }
        final Clinician other = (Clinician) obj;
        if (this.uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("Clinician [uuid=%s, firstName=%s]", this.uuid,
                this.firstName);
    }

}
