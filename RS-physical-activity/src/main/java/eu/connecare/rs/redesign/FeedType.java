/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:35:48 AM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public enum FeedType {

    OUTSTANDING, IMPROVE, LOW, BAD
}
