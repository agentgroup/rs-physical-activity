/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 4:45:25 PM
 */
package eu.connecare.rs.redesign;

import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class Patient implements User {

    @JsonProperty
    private final UUID uuid;
    @JsonProperty
    private UUID role;
    @JsonProperty
    private String firstName;
    @JsonProperty
    private String lastName;
    //    @JsonProperty
    //    private OffsetDateTime birthDate;
    @JsonProperty
    private Gender gender;
    @JsonProperty
    private String language;

    /**
    *
    */
    public Patient(/* @JsonProperty("uuid") */final UUID uuid) {
        this.uuid = uuid;
    }

    /**
     *
     */
    public Patient(/* @JsonProperty("uuid") */final UUID uuid,
            final String name) {
        this.uuid = uuid;
        this.firstName = name;
    }

    /**
     * @param uuid
     * @param role
     * @param firstName
     * @param lastName
     * @param birthDate
     * @param gender
     * @param language
     */
    @JsonCreator
    public Patient(UUID uuid, UUID role, String firstName, String lastName,
            /*OffsetDateTime birthDate,*/ Gender gender, String language) {
        this.uuid = uuid;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        //        this.birthDate = birthDate;
        this.gender = gender;
        this.language = language;
    }

    /**
     * @return the uuid
     */
    @Override
    public UUID getUuid() {
        return this.uuid;
    }

    /**
     * @return the role
     */
    public UUID getRole() {
        return this.role;
    }

    /**
     * @return the firstName
     */
    @Override
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return this.lastName;
    }

    //    /**
    //     * @return the birthDate
    //     */
    //    public OffsetDateTime getBirthDate() {
    //        return this.birthDate;
    //    }

    /**
     * @return the gender
     */
    public Gender getGender() {
        return this.gender;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return this.language;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFirstName(String name) {
        this.firstName = name;
    }

    /**
     * @param language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.uuid == null) ? 0 : this.uuid.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Patient)) {
            return false;
        }
        final Patient other = (Patient) obj;
        if (this.uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "Patient [uuid=%s, role=%s, firstName=%s, lastName=%s, gender=%s, language=%s]",
                this.uuid, this.role, this.firstName, this.lastName,
                /*this.birthDate,*/ this.gender, this.language);
    }

}
