/**
 * Created by sm (stefano.mariani@unimore.it) on Jul 18, 2018 10:57:17 AM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class UnknownMetricException extends Exception {

    /**
     * @param format
     */
    public UnknownMetricException(String msg) {
        super(msg);
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
