/**
 * Created by sm (stefano.mariani@unimore.it) on Aug 30, 2018 3:28:08 PM
 */
package eu.connecare.rs.redesign;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public enum Gender {

    MALE, FEMALE, UNKNOWN

}
