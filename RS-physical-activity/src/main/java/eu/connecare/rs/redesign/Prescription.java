/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 4:49:53 PM
 */
package eu.connecare.rs.redesign;

import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class Prescription implements ClinicalEvent {

    private final UUID uuid; // comes from the SMS
    private final Patient subject;
    private final Clinician prescriber;
    private final LocalDate startDate;
    private final LocalDate dueDate;
    private final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> goals;

    /**
     *
     */
    @JsonCreator // from https://stackoverflow.com/questions/31094354/jersey-no-suitable-constructor-found-for-type-simple-type-class-thing-can-n
    public Prescription(@JsonProperty("uuid") final UUID uuid,
            @JsonProperty("subject") final Patient subject,
            @JsonProperty("prescriber") final Clinician prescriber,
            @JsonProperty("startDate") final LocalDate startDate,
            @JsonProperty("dueDate") final LocalDate dueDate,
            @JsonProperty("goals") final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> goals) {
        this.uuid = uuid;
        this.subject = subject;
        this.prescriber = prescriber;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.goals = ImmutableMap.copyOf(goals);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UUID getUuid() {
        return this.uuid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Patient getSubject() {
        return this.subject;
    }

    /**
     * @return the prescriber
     */
    public Clinician getPrescriber() {
        return this.prescriber;
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return this.startDate;
    }

    /**
     * @return the dueDate
     */
    public LocalDate getDueDate() {
        return this.dueDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> getActivityLevels() {
        return this.goals;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.uuid == null) ? 0 : this.uuid.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Prescription)) {
            return false;
        }
        final Prescription other = (Prescription) obj;
        if (this.uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "Prescription [uuid=%s, subject=%s, prescriber=%s, startDate=%s, dueDate=%s, goals=%s]",
                this.uuid, this.subject, this.prescriber, this.startDate,
                this.dueDate, this.goals);
    }

}
