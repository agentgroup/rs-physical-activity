/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 3:24:42 PM
 */
package eu.connecare.rs.redesign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class RsEngine implements IRsEngine {

    private static final Logger LOGGER = LogManager.getFormatterLogger();
    private final UUID uuid;
    private IStrategy strategy;
    private IPolicy policy;
    private final Map<UUID, Prescription> prescriptions;
    private final Map<UUID, List<Accomplishment>> accomplishments;
    private final Map<UUID, Recommendation> recommendations;
    private final Map<UUID, Feedback> feedbacks;

    /**
     * @param strategy
     * @param policy
     */
    public RsEngine(final IStrategy strategy, final IPolicy policy) {
        this.uuid = UUID.randomUUID();
        this.strategy = strategy;
        this.policy = policy;
        this.prescriptions = new HashMap<>();
        this.accomplishments = new HashMap<>();
        this.recommendations = new HashMap<>();
        this.feedbacks = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IRsEngine feed(final Prescription prescription) {
        if (!this.prescriptions.containsKey(prescription.getUuid())) {
            RsEngine.LOGGER.info(
                    "[id: %s]: Prescription %n\t %s %n successfully added",
                    this.uuid, prescription);
            this.prescriptions.put(prescription.getUuid(), prescription);
        } else {
            RsEngine.LOGGER.warn(String.format(
                    "[id: %s]: Prescription %n\t %s %n already present, ignored",
                    this.uuid, prescription));
        }
        return this;
    }

    /**
     * {@inheritDoc}
     *
     * @throws UnknownActivityLevelException
     */
    @Override
    public IRsEngine feed(final UUID prescriptionUuid,
            final Accomplishment accomplishment)
            throws PrescriptionNotFoundException,
            UnknownActivityLevelException {
        if (!this.prescriptions.containsKey(prescriptionUuid)) {
            RsEngine.LOGGER.fatal(
                    "[id: %s]: Prescription %s not found, raising exception",
                    this.uuid, prescriptionUuid);
            throw new PrescriptionNotFoundException();
        }
        // TODO what if Patient of Accomplishment is different from Patient of prescription relative to UUID?
        List<Accomplishment> l = this.accomplishments.get(prescriptionUuid);
        if (l == null) {
            RsEngine.LOGGER.debug(
                    "[id: %s]: First accomplishment for %s, creating list",
                    this.uuid, prescriptionUuid);
            l = new ArrayList<>();
            this.accomplishments.put(prescriptionUuid, l);
        }
        if (!l.contains(accomplishment)) {
            l.add(accomplishment);
            RsEngine.LOGGER.info(
                    "[id: %s]: Accomplishment %n\t %s %n successfully added to prescription %s",
                    this.uuid, accomplishment, prescriptionUuid);
            Map<ActivityMetric, Adherence> ad = this.strategy
                    .computeAdherenceDetail(
                            this.prescriptions.get(prescriptionUuid),
                            this.accomplishments.get(prescriptionUuid));
            RsEngine.LOGGER.debug("[id: %s] Adherence detail: %s", this.uuid,
                    ad);
            Adherence a = this.strategy.computeAdherence(
                    this.prescriptions.get(prescriptionUuid),
                    this.accomplishments.get(prescriptionUuid));
            RsEngine.LOGGER.debug("[id: %s] Adherence: %s", this.uuid, a);
            Recommendation r = this.policy.computeRecommendation(
                    this.prescriptions.get(prescriptionUuid),
                    this.strategy.getAccomplishmentSummary()
                            .get(prescriptionUuid),
                    this.strategy.getDetailedAdherence().get(prescriptionUuid),
                    this.strategy.getAdherence().get(prescriptionUuid),
                    this.accomplishments
                            .get(prescriptionUuid).get(this.accomplishments
                                    .get(prescriptionUuid).size() - 1)
                            .getDate());
            RsEngine.LOGGER.debug("[id: %s] Recommendation : %s", this.uuid, r);
            this.recommendations.put(prescriptionUuid, r);
        } else {
            RsEngine.LOGGER.warn(
                    "[id: %s]: Accomplishment %n\t %s %n already present in prescription %s, ignored",
                    this.uuid, accomplishment, prescriptionUuid);
        }
        return this;
    }

    /**
     * {@inheritDoc}
     *
     * @throws UnknownActivityLevelException
     */
    @Override
    public IRsEngine feed(final Prescription prescription,
            final Accomplishment accomplishment)
            throws UnknownActivityLevelException {
        if (!this.prescriptions.containsKey(prescription.getUuid())) {
            this.prescriptions.put(prescription.getUuid(), prescription); // TODO check duplicate
        }
        // TODO what if Patient of Accomplishment is different from Patient of prescription relative to UUID?
        try {
            this.feed(prescription.getUuid(), accomplishment);
        } catch (final PrescriptionNotFoundException e) {
            // can't happen
            e.printStackTrace();
        }
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<ActivityMetric, Adherence> getAdherenceDetail(
            final UUID prescriptionUuid) throws PrescriptionNotFoundException,
            UnknownActivityLevelException {
        if (!this.prescriptions.containsKey(prescriptionUuid)) {
            throw new PrescriptionNotFoundException();
        }
        Map<ActivityMetric, Adherence> m = this.strategy.getDetailedAdherence()
                .get(prescriptionUuid);
        if (m == null) {
            m = this.strategy.computeAdherenceDetail(
                    this.prescriptions.get(prescriptionUuid),
                    this.accomplishments.get(prescriptionUuid));
        }
        return m;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Adherence getAdherence(final UUID prescriptionUuid)
            throws PrescriptionNotFoundException,
            UnknownActivityLevelException {
        if (!this.prescriptions.containsKey(prescriptionUuid)) {
            throw new PrescriptionNotFoundException();
        }
        Adherence a = this.strategy.getAdherence().get(prescriptionUuid);
        if (a == null) {
            a = this.strategy.computeAdherence(
                    this.prescriptions.get(prescriptionUuid),
                    this.accomplishments.get(prescriptionUuid));
        }
        return a;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Recommendation getRecommendation(final UUID prescriptionUuid)
            throws PrescriptionNotFoundException {
        if (!this.prescriptions.containsKey(prescriptionUuid)) {
            RsEngine.LOGGER.fatal(
                    "[id: %s]: Prescription %s not found, raising exception",
                    this.uuid, prescriptionUuid);
            throw new PrescriptionNotFoundException();
        }
        Recommendation r = this.recommendations.get(prescriptionUuid);
        if (r == null) {
            RsEngine.LOGGER.warn(
                    "[id: %s]: No existing recommendation for prescription %s, now generating on-the-fly...",
                    this.uuid, prescriptionUuid);
            r = this.policy.computeRecommendation(
                    this.prescriptions.get(prescriptionUuid),
                    this.strategy.getAccomplishmentSummary()
                            .get(prescriptionUuid),
                    this.strategy.getDetailedAdherence().get(prescriptionUuid),
                    this.strategy.getAdherence().get(prescriptionUuid),
                    this.accomplishments
                            .get(prescriptionUuid).get(this.accomplishments
                                    .get(prescriptionUuid).size() - 1)
                            .getDate());
        }
        return r;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Feedback getFeedback(final UUID prescriptionUuid)
            throws PrescriptionNotFoundException {
        if (!this.prescriptions.containsKey(prescriptionUuid)) {
            throw new PrescriptionNotFoundException();
        }
        Feedback f = this.feedbacks.get(prescriptionUuid);
        if (f == null) {
            f = this.policy.computeFeedback();
        }
        return f;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IStrategy getStrategy() {
        return this.strategy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setStrategy(final IStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IPolicy getPolicy() {
        return this.policy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPolicy(final IPolicy policy) {
        this.policy = policy;
    }

    /**
     * @return the prescriptions
     */
    public Map<UUID, Prescription> getPrescriptions() {
        return this.prescriptions;
    }

    /**
     * @return the accomplishments
     */
    public Map<UUID, List<Accomplishment>> getAccomplishments() {
        return this.accomplishments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.uuid == null) ? 0 : this.uuid.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof RsEngine)) {
            return false;
        }
        final RsEngine other = (RsEngine) obj;
        if (this.uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "RsEngine [uuid=%s, strategy=%s, policy=%s, accomplishments=%s, recommendations=%s, feedbacks=%s]",
                this.uuid, this.strategy, this.policy, this.accomplishments,
                this.recommendations, this.feedbacks);
    }

}
