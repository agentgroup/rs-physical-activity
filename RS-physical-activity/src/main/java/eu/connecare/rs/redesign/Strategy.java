/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 4:51:33 PM
 */
package eu.connecare.rs.redesign;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class Strategy implements IStrategy { // TODO equals, hashCode, toString

    private static final Logger LOGGER = LogManager.getFormatterLogger();
    /*
     * should not have state beyond config params, move to RsEngine
     */
    private final Map<UUID, Map<ActivityMetric, Adherence>> detailedAdherence;
    private final Map<UUID, Adherence> adherence;
    private final Map<UUID, Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>>> accomplishmentsSummary;

    /**
     *
     */
    public Strategy() {
        this.detailedAdherence = new HashMap<>();
        this.adherence = new HashMap<>();
        this.accomplishmentsSummary = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     *
     * @throws UnknownActivityLevelException
     */
    @Override
    public Map<ActivityMetric, Adherence> computeAdherenceDetail(
            final Prescription prescription,
            final List<Accomplishment> accomplishments)
            throws UnknownActivityLevelException {
        // accumulate partial fullfillments
        final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> totalAccomplishments = new HashMap<>();
        int i;
        ActivityLevel<? extends Comparable<?>> al;
        for (final ActivityMetric m : prescription.getActivityLevels()
                .keySet()) {
            i = 0;
            al = accomplishments.get(i).getActivityLevels().get(m);
            totalAccomplishments.put(m,
                    accomplishments.get(i).getActivityLevels().get(m)); // TODO check same keys
            for (i = 1; i < accomplishments.size(); i++) {
                al = totalAccomplishments.get(m);
                Strategy.LOGGER.trace("PRE) activity level: %s", al);
                if (al == null) {
                    continue; // fix for case in which a prescription metric is NOT in the accomplishments
                }
                if (al instanceof DoubleLevel) {
                    totalAccomplishments.put(m,
                            ((DoubleLevel) al).accumulate(
                                    ((DoubleLevel) accomplishments.get(i)
                                            .getActivityLevels().get(m)))); // same metric, same cast
                } else if (totalAccomplishments
                        .get(m) instanceof DurationLevel) {
                    totalAccomplishments.put(m,
                            ((DurationLevel) al).accumulate(
                                    ((DurationLevel) accomplishments.get(i)
                                            .getActivityLevels().get(m)))); // same metric, same cast
                } else {
                    throw new UnknownActivityLevelException(
                            String.format("metric: %s, level: %s", m.toString(),
                                    al.toString()));
                }
                Strategy.LOGGER.trace("POST) activity level: %s", al);
            }
        }
        this.accomplishmentsSummary.put(prescription.getUuid(),
                totalAccomplishments); // TODO check same keys
        Strategy.LOGGER.debug("totalAccomplishments: %s", totalAccomplishments);
        // find differences w.r.t. prescription goals
        final Map<ActivityMetric, Adherence> adherencePerMetric = new HashMap<>();
        for (final ActivityMetric m : prescription.getActivityLevels()
                .keySet()) {
            al = totalAccomplishments.get(m);
            if (al == null) {
                if (m.getMetricName().equals("sedentary")) {
                    adherencePerMetric.put(m, new PercentageAdherence(100)); // fix for case in which a prescription metric is NOT in the accomplishments + fix for sedentary activity reverted logic (https://gitlab.com/AgMore/Connecare-RS-physical/issues/25)
                } else {
                    adherencePerMetric.put(m, new PercentageAdherence(0)); // fix for case in which a prescription metric is NOT in the accomplishments
                }
            } else if (al instanceof DoubleLevel) {
                adherencePerMetric.put(m,
                        new PercentageAdherence(
                                ((DoubleLevel) al).percentageDone(
                                        ((DoubleLevel) prescription
                                                .getActivityLevels().get(m)))));
            } else if (totalAccomplishments.get(m) instanceof DurationLevel) {
                PercentageAdherence adh;
                if (m.getMetricName().equals("sedentary")) { // fix for sedentary activity reverted logic (https://gitlab.com/AgMore/Connecare-RS-physical/issues/25)
                    DurationLevel g = (DurationLevel) prescription
                            .getActivityLevels().get(m);
                    adh = new PercentageAdherence(
                            (((DurationLevel) al).remainingTo(g, m))
                                    .percentageDone(
                                            g));
                    if (adh.getAsPercentage() < 0) {
                        adh = new PercentageAdherence(0); // fix for negative adherence, sedentary case
                    }
                    adherencePerMetric.put(m, adh);
                } else {
                    adh = new PercentageAdherence(
                            ((DurationLevel) al).percentageDone(
                                    ((DurationLevel) prescription
                                            .getActivityLevels().get(m))));
                    adherencePerMetric.put(m, adh);
                }
            } else {
                throw new UnknownActivityLevelException(String.format(
                        "metric: %s, level: %s", m.toString(), al.toString()));
            }
        }
        this.detailedAdherence.put(prescription.getUuid(), adherencePerMetric);
        return adherencePerMetric;
    }

    /**
     * {@inheritDoc}
     *
     * @throws UnknownActivityLevelException
     *
     * @throws UnknownAdherenceException
     */
    @Override
    public Adherence computeAdherence(final Prescription prescription,
            final List<Accomplishment> accomplishments)
            throws UnknownActivityLevelException {
        Adherence result;
        // if we do not already have details
        if (this.detailedAdherence.get(prescription.getUuid()) == null) {
            this.computeAdherenceDetail(prescription, accomplishments);
        }
        result = new PercentageAdherence(
                this.summarise(prescription).getResult());
        this.adherence.put(prescription.getUuid(), result);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<UUID, Map<ActivityMetric, Adherence>> getDetailedAdherence() {
        return this.detailedAdherence;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<UUID, Adherence> getAdherence() {
        return this.adherence;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<UUID, Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>>> getAccomplishmentSummary() {
        return this.accomplishmentsSummary;
    }

    /**
     * @param prescription
     * @return
     */
    private Mean summarise(final Prescription prescription) {
        final Mean avg = new Mean();
        final Map<ActivityMetric, Adherence> detailMap = this.detailedAdherence
                .get(prescription.getUuid());
        for (final ActivityMetric m : detailMap.keySet()) {
            avg.increment(detailMap.get(m).getAsPercentage());
        }
        return avg;
    }

}
