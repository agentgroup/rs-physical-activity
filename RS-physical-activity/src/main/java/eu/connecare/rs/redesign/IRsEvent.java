/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 11:20:37 PM
 */
package eu.connecare.rs.redesign;

import java.time.Duration;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface IRsEvent<T> extends Iterable<ActivityMetric> { // CHECK comparable on duration for sorting?

    T getType();

    Patient getSubject();

    String getText();

    Duration getExpiration();

    Adherence getAdherence(ActivityMetric metric);

    ActivityLevel<? extends Comparable<?>> getAccomplishment(
            ActivityMetric metric);

}
