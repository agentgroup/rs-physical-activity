/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 4:48:50 PM
 */
package eu.connecare.rs.redesign;

import java.util.UUID;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface User {

    /**
     * @return the uuid
     */
    UUID getUuid();

    /**
     * @return
     */
    String getFirstName();
    
    void setFirstName(String name);

}