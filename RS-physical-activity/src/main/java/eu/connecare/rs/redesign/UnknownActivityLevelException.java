/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 9, 2018 6:10:25 PM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class UnknownActivityLevelException extends Exception {

    /**
     * @param format
     */
    public UnknownActivityLevelException(String msg) {
        super(msg);
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
