/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:58:47 AM
 */
package eu.connecare.rs.redesign;

import java.time.LocalDate;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface TimeWindow extends Comparable<TimeWindow> {

    long getProgressiveId();

    LocalDate getStart();

    LocalDate getEnd();

}
