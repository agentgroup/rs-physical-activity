/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 5:03:49 PM
 */
package eu.connecare.rs.redesign;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class Accomplishment implements ClinicalEvent {

    private final UUID uuid; // comes from the SMS
    private final Patient subject;
    private final LocalDateTime date;
    private final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> measurements;

    /**
     *
     */
    @JsonCreator // from https://stackoverflow.com/questions/31094354/jersey-no-suitable-constructor-found-for-type-simple-type-class-thing-can-n
    public Accomplishment(@JsonProperty("uuid") final UUID uuid,
            @JsonProperty("patient") final Patient patient,
            @JsonProperty("date") final LocalDateTime date,
            @JsonProperty("measurements") final Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> measurements) {
        this.uuid = uuid;
        this.subject = patient;
        this.date = date;
        this.measurements = ImmutableMap.copyOf(measurements);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UUID getUuid() {
        return this.uuid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Patient getSubject() {
        return this.subject;
    }

    /**
     * @return the date
     */
    public LocalDateTime getDate() {
        return this.date;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> getActivityLevels() {
        return this.measurements;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.uuid == null) ? 0 : this.uuid.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Accomplishment)) {
            return false;
        }
        final Accomplishment other = (Accomplishment) obj;
        if (this.uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "Accomplishment [uuid=%s, subject=%s, date=%s, measurements=%s]",
                this.uuid, this.subject, this.date, this.measurements);
    }

}
