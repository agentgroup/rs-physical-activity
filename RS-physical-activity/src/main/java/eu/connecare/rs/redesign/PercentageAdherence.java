/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:52:30 AM
 */
package eu.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class PercentageAdherence implements Adherence {

    private final double value;

    /**
     *
     */
    public PercentageAdherence(final double percentageValue) {
        this.value = percentageValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getAsPercentage() {
        return this.value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(this.value);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof PercentageAdherence)) {
            return false;
        }
        final PercentageAdherence other = (PercentageAdherence) obj;
        if (Double.doubleToLongBits(this.value) != Double
                .doubleToLongBits(other.value)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("PercentageAdherence [value=%s]", this.value);
    }

}
