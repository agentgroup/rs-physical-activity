/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 4, 2018 5:09:51 PM
 */
package eu.connecare.rs.redesign;

import java.util.Map;
import java.util.UUID;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface ClinicalEvent {

    /**
     * @return the uuid
     */
    UUID getUuid();

    /**
     * @return the subject
     */
    Patient getSubject();

    /**
     * @return the measurements
     */
    Map<ActivityMetric, ActivityLevel<? extends Comparable<?>>> getActivityLevels();

}
