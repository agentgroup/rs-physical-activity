/**
 * Created by sm (stefano.mariani@unimore.it) on Jul 19, 2018 3:11:24 PM
 */
package eu.connecare.rs.redesign.smsclient;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.UUID;

import eu.connecare.rs.redesign.AlertStatus;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class SmsAlert {

    private final String alertType;
    private final OffsetDateTime creationDate;
    private final OffsetDateTime expireDate;
    private final int priorityLevel;
    private final UUID recipient;
    private final UUID target;
    private final AlertStatus status;
    private final Map<String, String> extraParameters;
    private String comments = "";

    /**
     * @param id
     * @param alertType
     * @param expireDate
     * @param recipient
     * @param target
     * @param extraParameters
     */
    public SmsAlert(final String alertType, final OffsetDateTime expireDate,
            final UUID recipient, final UUID target,
            final Map<String, String> extraParameters) {
        this.alertType = alertType;
        this.expireDate = expireDate;
        this.recipient = recipient;
        this.target = target;
        this.status = AlertStatus.ACT;
        this.extraParameters = extraParameters;
        this.creationDate = OffsetDateTime.now();
        this.priorityLevel = 1;
    }

    public SmsAlert appendComment(String comment) {
        this.comments += String.format("%n%s", comment);
        return this;
    }

    /**
     * @return the alertType
     */
    public String getAlertType() {
        return this.alertType;
    }

    /**
     * @return the creationDate
     */
    public OffsetDateTime getCreationDate() {
        return this.creationDate;
    }

    /**
     * @return the expireDate
     */
    public OffsetDateTime getExpireDate() {
        return this.expireDate;
    }

    /**
     * @return the priorityLevel
     */
    public int getPriorityLevel() {
        return this.priorityLevel;
    }

    /**
     * @return the recipient
     */
    public UUID getRecipient() {
        return this.recipient;
    }

    /**
     * @return the target
     */
    public UUID getTarget() {
        return this.target;
    }

    /**
     * @return the status
     */
    public AlertStatus getStatus() {
        return this.status;
    }

    /**
     * @return the extraParameters
     */
    public Map<String, String> getExtraParameters() {
        return this.extraParameters;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return this.comments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.alertType == null) ? 0 : this.alertType.hashCode());
        result = prime * result + ((this.creationDate == null) ? 0
                : this.creationDate.hashCode());
        result = prime * result
                + ((this.expireDate == null) ? 0 : this.expireDate.hashCode());
        result = prime * result + ((this.extraParameters == null) ? 0
                : this.extraParameters.hashCode());
        result = prime * result
                + ((this.recipient == null) ? 0 : this.recipient.hashCode());
        result = prime * result
                + ((this.status == null) ? 0 : this.status.hashCode());
        result = prime * result
                + ((this.target == null) ? 0 : this.target.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SmsAlert)) {
            return false;
        }
        SmsAlert other = (SmsAlert) obj;
        if (this.alertType == null) {
            if (other.alertType != null) {
                return false;
            }
        } else if (!this.alertType.equals(other.alertType)) {
            return false;
        }
        if (this.creationDate == null) {
            if (other.creationDate != null) {
                return false;
            }
        } else if (!this.creationDate.equals(other.creationDate)) {
            return false;
        }
        if (this.expireDate == null) {
            if (other.expireDate != null) {
                return false;
            }
        } else if (!this.expireDate.equals(other.expireDate)) {
            return false;
        }
        if (this.extraParameters == null) {
            if (other.extraParameters != null) {
                return false;
            }
        } else if (!this.extraParameters.equals(other.extraParameters)) {
            return false;
        }
        if (this.recipient == null) {
            if (other.recipient != null) {
                return false;
            }
        } else if (!this.recipient.equals(other.recipient)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (this.target == null) {
            if (other.target != null) {
                return false;
            }
        } else if (!this.target.equals(other.target)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "SmsAlert [alertType=%s, creationDate=%s, expireDate=%s, priorityLevel=%s, recipient=%s, target=%s, status=%s, extraParameters=%s, comments=%s]",
                this.alertType, this.creationDate, this.expireDate,
                this.priorityLevel, this.recipient, this.target, this.status,
                this.extraParameters, this.comments);
    }

}
