/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 9, 2018 6:15:52 PM
 */
package eu.connecare.rs.redesign;

import java.util.Map;
import java.util.UUID;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public interface IRsEngine {

    /**
     * @param prescription
     */
    IRsEngine feed(Prescription prescription);

    /**
     *
     * @param prescriptionUuid
     * @param accomplishment
     * @return
     * @throws PrescriptionNotFoundException
     * @throws UnknownActivityLevelException
     */
    IRsEngine feed(UUID prescriptionUuid, Accomplishment accomplishment)
            throws PrescriptionNotFoundException, UnknownActivityLevelException;

    /**
     * NOT USED ATM
     *
     * @param prescription
     * @param accomplishment
     * @return
     * @throws UnknownActivityLevelException
     */
    IRsEngine feed(Prescription prescription, Accomplishment accomplishment)
            throws UnknownActivityLevelException;

    /**
     *
     * @param prescriptionUuid
     * @return
     * @throws PrescriptionNotFoundException
     * @throws UnknownActivityLevelException
     */
    Map<ActivityMetric, Adherence> getAdherenceDetail(UUID prescriptionUuid)
            throws PrescriptionNotFoundException, UnknownActivityLevelException;

    /**
     *
     * @param prescriptionUuid
     * @return
     * @throws PrescriptionNotFoundException
     * @throws UnknownActivityLevelException
     */
    Adherence getAdherence(UUID prescriptionUuid)
            throws PrescriptionNotFoundException, UnknownActivityLevelException;

    /**
     *
     * @param prescriptionUuid
     * @return
     * @throws PrescriptionNotFoundException
     */
    Recommendation getRecommendation(UUID prescriptionUuid)
            throws PrescriptionNotFoundException;

    /**
     *
     * @param prescriptionUuid
     * @return
     * @throws PrescriptionNotFoundException
     */
    Feedback getFeedback(UUID prescriptionUuid)
            throws PrescriptionNotFoundException;

    /**
     * @return the strategy
     */
    IStrategy getStrategy();

    /**
     * @param strategy
     *            the strategy to set
     */
    void setStrategy(IStrategy strategy);

    /**
     * @return the policy
     */
    IPolicy getPolicy();

    /**
     * @param policy
     *            the policy to set
     */
    void setPolicy(IPolicy policy);

}