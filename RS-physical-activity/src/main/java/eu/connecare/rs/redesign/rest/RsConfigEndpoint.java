/**
 * Created by sm (stefano.mariani@unimore.it) on Sep 5, 2018 12:33:21 AM
 */
package eu.connecare.rs.redesign.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class RsConfigEndpoint extends AbstractVerticle {

    private static final Logger LOGGER = LogManager.getFormatterLogger();

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() throws Exception {
        RsConfigEndpoint.LOGGER.info(
                "RsConfigEndpoint started");
        HttpServer server = this.vertx.createHttpServer();
        Router router = Router.router(this.vertx);
        /*
         * RS send alerts configuration
         */
        Route route = router.route(HttpMethod.PATCH, "/config/alerts")
                .consumes("application/json").produces("text/plain");
        route.handler(routingContext -> {
            HttpServerRequest req = routingContext.request();
            req.bodyHandler(body -> {
                JsonObject json = body.toJsonObject();
                Boolean sendAlerts = json.getBoolean("sendAlerts");
                RsConfigEndpoint.LOGGER.info(
                        "Request to set alerts as active to %s received",
                        sendAlerts);
                HttpServerResponse response = routingContext.response();
                response.putHeader("content-type", "text/plain");
                EventBus eb = this.vertx.eventBus();
                DeliveryOptions options = new DeliveryOptions();
                options.addHeader("alerts", "alerts");
                RsConfigEndpoint.LOGGER.info(
                        "\tForwarding request to set alerts as active to %s to RS...",
                        sendAlerts);
                eb.publish("RS.physical.config", sendAlerts, options);
                response.end(String.format(
                        "Request to set alerts as active to %s received",
                        sendAlerts));
            });
        });
        /*
         * RS cycle period configuraiton
         */
        route = router.route(HttpMethod.PATCH, "/config/cycle")
                .consumes("application/json").produces("text/plain");
        route.handler(routingContext -> {
            HttpServerRequest req = routingContext.request();
            req.bodyHandler(body -> {
                JsonObject json = body.toJsonObject();
                Long cycle = json.getLong("cycle");
                RsConfigEndpoint.LOGGER.info(
                        "Request to set cycle time to %s received", cycle);
                HttpServerResponse response = routingContext.response();
                response.putHeader("content-type", "text/plain");
                EventBus eb = this.vertx.eventBus();
                DeliveryOptions options = new DeliveryOptions();
                options.addHeader("cycle", "cycle");
                RsConfigEndpoint.LOGGER.info(
                        "\tForwarding request to set cycle time to %s to RS...",
                        cycle);
                eb.publish("RS.physical.config", cycle, options);
                response.end(String.format(
                        "Request to set cycle time to %s received", cycle));
            });
        });
        // applicationId
        route = router.route(HttpMethod.PATCH, "/config/applicationId")
                .consumes("application/json").produces("text/plain");
        route.handler(routingContext -> {
            HttpServerRequest req = routingContext.request();
            req.bodyHandler(body -> {
                JsonObject json = body.toJsonObject();
                String applicationId = json.getString("applicationId");
                RsConfigEndpoint.LOGGER.info(
                        "Request to set config applicationId to %s received",
                        applicationId);
                HttpServerResponse response = routingContext.response();
                response.putHeader("content-type", "text/plain");
                EventBus eb = this.vertx.eventBus();
                DeliveryOptions options = new DeliveryOptions();
                options.addHeader("applicationId", "applicationId");
                RsConfigEndpoint.LOGGER.info(
                        "\tForwarding request to set config applicationId to %s to RS...",
                        applicationId);
                eb.publish("RS.physical.config", applicationId, options);
                response.end(String.format(
                        "Request to set config applicationId to %s received",
                        applicationId));
            });
        });
        // userCtrl
        route = router.route(HttpMethod.PATCH, "/config/userCtrl")
                .consumes("application/json").produces("text/plain");
        route.handler(routingContext -> {
            HttpServerRequest req = routingContext.request();
            req.bodyHandler(body -> {
                JsonObject json = body.toJsonObject();
                String userCtrl = json.getString("userCtrl");
                RsConfigEndpoint.LOGGER.info(
                        "Request to set config userCtrl path to %s received",
                        userCtrl);
                HttpServerResponse response = routingContext.response();
                response.putHeader("content-type", "text/plain");
                EventBus eb = this.vertx.eventBus();
                DeliveryOptions options = new DeliveryOptions();
                options.addHeader("userCtrl", "userCtrl");
                RsConfigEndpoint.LOGGER.info(
                        "\tForwarding request to set config userCtrl path to %s to RS...",
                        userCtrl);
                eb.publish("RS.physical.config", userCtrl, options);
                response.end(String.format(
                        "Request to set config userCtrl path to %s received",
                        userCtrl));
            });
        });
        // activityCtx
        route = router.route(HttpMethod.PATCH, "/config/activityCtx")
                .consumes("application/json").produces("text/plain");
        route.handler(routingContext -> {
            HttpServerRequest req = routingContext.request();
            req.bodyHandler(body -> {
                JsonObject json = body.toJsonObject();
                String activityCtx = json.getString("activityCtx");
                RsConfigEndpoint.LOGGER.info(
                        "Request to set config activityCtx path to %s received",
                        activityCtx);
                HttpServerResponse response = routingContext.response();
                response.putHeader("content-type", "text/plain");
                EventBus eb = this.vertx.eventBus();
                DeliveryOptions options = new DeliveryOptions();
                options.addHeader("activityCtx", "activityCtx");
                RsConfigEndpoint.LOGGER.info(
                        "\tForwarding request to set config activityCtx path to %s to RS...",
                        activityCtx);
                eb.publish("RS.physical.config", activityCtx, options);
                response.end(String.format(
                        "Request to set config activityCtx path to %s received",
                        activityCtx));
            });
        });
        // alertCtx
        route = router.route(HttpMethod.PATCH, "/config/alertCtx")
                .consumes("application/json").produces("text/plain");
        route.handler(routingContext -> {
            HttpServerRequest req = routingContext.request();
            req.bodyHandler(body -> {
                JsonObject json = body.toJsonObject();
                String alertCtx = json.getString("alertCtx");
                RsConfigEndpoint.LOGGER.info(
                        "Request to set config alertCtx path to %s received",
                        alertCtx);
                HttpServerResponse response = routingContext.response();
                response.putHeader("content-type", "text/plain");
                EventBus eb = this.vertx.eventBus();
                DeliveryOptions options = new DeliveryOptions();
                options.addHeader("alertCtx", "alertCtx");
                RsConfigEndpoint.LOGGER.info(
                        "\tForwarding request to set config alertCtx path to %s to RS...",
                        alertCtx);
                eb.publish("RS.physical.config", alertCtx, options);
                response.end(String.format(
                        "Request to set config alertCtx path to %s received",
                        alertCtx));
            });
        });
        // lang (post) TODO delete, put
        route = router.route(HttpMethod.POST, "/config/lang")
                .consumes("application/json").produces("text/plain");
        route.handler(routingContext -> {
            HttpServerRequest req = routingContext.request();
            req.bodyHandler(body -> {
                JsonObject json = body.toJsonObject();
                String lang = json.getString("lang");
                RsConfigEndpoint.LOGGER
                        .info("Request to add config lang %s received", lang);
                HttpServerResponse response = routingContext.response();
                response.putHeader("content-type", "text/plain");
                EventBus eb = this.vertx.eventBus();
                DeliveryOptions options = new DeliveryOptions();
                options.addHeader("lang", "lang");
                RsConfigEndpoint.LOGGER.info(
                        "\tForwarding request to add config lang %s to RS...",
                        lang);
                eb.publish("RS.physical.config", lang, options);
                response.end(String.format(
                        "Request to add config lang %s received", lang));
            });
        });
        server.requestHandler(router::accept).listen(9090);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop(Future<Void> stopFuture) throws Exception {
        RsConfigEndpoint.LOGGER.info(
                "RsConfigEndpoint stopped");
        super.stop(stopFuture);
    }

    public static void main(String[] args) {
        VertxOptions options = new VertxOptions();
        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                Vertx v = res.result();
                v.deployVerticle(RsConfigEndpoint.class,
                        new DeploymentOptions(), dRes -> {
                            if (dRes.succeeded()) {
                                RsConfigEndpoint.LOGGER.info(
                                        "Deployed verticle %s", dRes.result());
                            } else {
                                RsConfigEndpoint.LOGGER.info(
                                        "NOT deployed verticle %s",
                                        dRes.result());
                            }
                        });
            }
        });
    }

}
