/**
 * Created by sm (stefano.mariani@unimore.it) on Dec 1, 2017 10:05:47 AM
 */
package eu.connecare.rs.nlg.wrapper;

import simplenlg.features.Feature;
import simplenlg.features.InterrogativeType;
import simplenlg.features.Tense;
import simplenlg.framework.CoordinatedPhraseElement;
import simplenlg.framework.NLGFactory;
import simplenlg.lexicon.Lexicon;
import simplenlg.phrasespec.NPPhraseSpec;
import simplenlg.phrasespec.PPPhraseSpec;
import simplenlg.phrasespec.SPhraseSpec;
import simplenlg.phrasespec.VPPhraseSpec;
import simplenlg.realiser.english.Realiser;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class SimpleNlgTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        final Lexicon lexicon = Lexicon.getDefaultLexicon();
        final NLGFactory nlgFactory = new NLGFactory(lexicon);
        Realiser realiser = new Realiser(lexicon);

        SPhraseSpec p = nlgFactory.createClause();
        p.setSubject("Henry");
        p.setVerb("walk");
        p.setObject("a few miles");
        String output = realiser.realiseSentence(p);
        System.out.println(output);

        p.setFeature(Feature.TENSE, Tense.PAST);
        output = realiser.realiseSentence(p);
        System.out.println(output);

        p.setFeature(Feature.TENSE, Tense.FUTURE);
        output = realiser.realiseSentence(p);
        System.out.println(output);

        p.setFeature(Feature.NEGATED, true);
        output = realiser.realiseSentence(p);
        System.out.println(output);

        p.setFeature(Feature.INTERROGATIVE_TYPE, InterrogativeType.YES_NO);
        p.setFeature(Feature.NEGATED, false);
        output = realiser.realiseSentence(p);
        System.out.println(output);

        //        p.setFeature(Feature.INTERROGATIVE_TYPE, InterrogativeType.WHO_OBJECT);
        //        output = realiser.realiseSentence(p);
        //        System.out.println(output);

        NPPhraseSpec subject = nlgFactory.createNounPhrase("Mary");
        NPPhraseSpec object = nlgFactory.createNounPhrase("the monkey");
        VPPhraseSpec verb = nlgFactory.createVerbPhrase("chase");
        subject.addModifier("fast");
        p = nlgFactory.createClause();
        p.setSubject(subject);
        p.setObject(object);
        p.setVerb(verb);
        verb.addModifier("quickly");
        output = realiser.realiseSentence(p);
        System.out.println(output);

        NPPhraseSpec subject1 = nlgFactory.createNounPhrase("Mary");
        NPPhraseSpec subject2 = nlgFactory.createNounPhrase("your", "giraffe");

        CoordinatedPhraseElement subj = nlgFactory
                .createCoordinatedPhrase(subject1, subject2);
        // may revert to nlgFactory.createCoordinatedPhrase( subject1, subject2 ) ;
        p.setSubject(subj);
        output = realiser.realiseSentence(p);
        System.out.println(output);

        NPPhraseSpec object1 = nlgFactory.createNounPhrase("the monkey");
        NPPhraseSpec object2 = nlgFactory.createNounPhrase("George");

        CoordinatedPhraseElement obj = nlgFactory
                .createCoordinatedPhrase(object1, object2);
        // may revert to nlgFactory.createCoordinatedPhrase( subject1, subject2 ) ; ??
        obj.addCoordinate("Martha");
        p.setObject(obj);
        output = realiser.realiseSentence(p);
        System.out.println(output);

        obj.setFeature(Feature.CONJUNCTION, "or");
        output = realiser.realiseSentence(p);
        System.out.println(output);

        NPPhraseSpec place = nlgFactory.createNounPhrase("park");
        place.setDeterminer("the");
        PPPhraseSpec pp = nlgFactory.createPrepositionPhrase();
        pp.addComplement(place);
        pp.setPreposition("in");
        p.addComplement(pp);
        output = realiser.realiseSentence(p);
        System.out.println(output);
    }

}
