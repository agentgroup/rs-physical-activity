/**
 * Created by sm (stefano.mariani@unimore.it) on Jan 5, 2018 12:32:51 AM
 */
package advice.connecare.rs.redesign;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public enum RecType {

    AWARD, MOTIVATIONAL, WARNING, ALERT, BLAME

}
