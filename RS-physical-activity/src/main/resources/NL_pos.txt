GREETING

Hallo
Hallo
Beste

GREETING_MORNING

Goedemorgen

GREETING_AFTERNOON

Goedemiddag

GREETING_EVENING

Goedenavond

FILLER

hoe gaat het?
gaat alles goed?
hoe gaat het?
hopelijk gaat alles goed :)
hoe gaat het met ermee?

ASSESSMENT_POSITIVE

je hebt het gedaan
je hebt het gehaald

ASSESSMENT_MOTIVATIONAL

je bent op schema met
je bent bovengemiddeld met
je doet het goed met

ASSESSMENT_NEGATIVE

zijn nog steeds ver weg
zijn nog steeds veel over

ASSESSMENT_FAILED

je hebt gefaald om te bereiken
je hebt het doel gemist

ASSESSMENT_TOO_MUCH

zijn te veel :(
zijn zo veel :(

CALL_TO_ACTION

niet opgeven!
gewoon doen :)
het is de moeite waard!
kom op!
probeer het :)
laten we het doen :)

NEXT_TIME

probeer de volgende keer harder :(
laten we het morgen opnieuw proberen!

WARNING

kijk uit!
houd het laag
hou de controle!

AWARD

gefeliciteerd :)
indrukwekkend!
ik ben trots :)

ARGUMENT_DATE

het einde van de dag nadert ...

ARGUMENT_TIME

er is nog genoeg tijd!
resterende tijd is meer dan genoeg!