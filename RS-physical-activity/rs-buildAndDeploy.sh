AWK_OUT="$(docker build -t rs-physical-client . | grep -E "Successfully built .*" | awk 'NF>1{print $NF}')"
docker tag $AWK_OUT smariani/rs-physical-client
docker push smariani/rs-physical-client
docker stack deploy -c docker-compose.yml RS
sleep 5
docker stack ls
docker service ls
docker container ls -a
