/**
 * Created by sm (stefano.mariani@unimore.it) on Jul 13, 2018 9:07:17 AM
 */
package eu.connecare.rs.vertx.tests;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class FutureCompletionIssue extends AbstractVerticle {

    private final Random r = new Random();

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() throws Exception {
        Future<String> f1 = Future.future();
        doF1(f1); // do step 1
        Future<String> f2 = f1.compose(f1res -> { // after 1 do 2...
            if (f1.succeeded()) { // ...only if 1 ok
                Logger.getAnonymousLogger()
                        .info(String.format("f1res: %s", f1res));
                Map<String, Future<String>> map = fakeMap();
                doFs(map); // do step 2
                System.out.println("dude here");
                CompositeFuture cfJoin = CompositeFuture
                        .join(Arrays
                                .asList(map.values().toArray(new Future[] {})))
                        .setHandler(fJoinCompleted -> { // when ALL in map complete
                            Logger.getAnonymousLogger()
                                    .info(String.format(
                                            "fJoinCompleted: %s, %s",
                                            fJoinCompleted.succeeded(),
                                            fJoinCompleted.result()));
                        });
                System.out.println("dude also here");
                Future<String> fJoin = cfJoin.compose(fJoinRes -> { // when join completes complete f2
                    Logger.getAnonymousLogger() // in any case, even if failures (semantic of join)
                            .info(String.format("fJoinRes: %s", fJoinRes));
                    return Future.succeededFuture("seq2 ok");
                });
                return fJoin; // maps to f2
            } // if f1 NOT ok fail f2
            Logger.getAnonymousLogger()
                    .info(String.format("f1res: %s", f1.cause().toString()));
            return Future.failedFuture("seq1 NOT ok");
        });
        System.out.println("dude even here");
        f2.compose(f2res -> { // if f2 failed following doesn't execute
            if (f2.succeeded()) {
                Logger.getAnonymousLogger()
                        .info(String.format("f2res: %s, ", f2.result()));
                return Future.succeededFuture();
            }
            Logger.getAnonymousLogger()
                    .info(String.format("f2res: %s, ", f2.cause().toString()));
            return Future.failedFuture("");
        });
    }

    /**
     * @return
     */
    private static Map<String, Future<String>> fakeMap() {
        List<String> fs = Arrays.asList(new String[] { "one", "two", "three" });
        Map<String, Future<String>> map = new HashMap<>();
        for (String item : fs) {
            map.put(item, Future.future());
        }
        return map;
    }

    /**
     * @param f1
     * 
     */
    private void doF1(Future<String> f1) {
        this.vertx.executeBlocking(f -> {
            f.complete();
        }, res -> {
            if (res.succeeded()) {
                f1.complete("f1 ok");
            } else {
                f1.complete("can't happen");
            }
        });
    }

    /**
     * @param map
     */
    private void doFs(Map<String, Future<String>> map) {
        for (String key : map.keySet()) {
            Future<String> fM = map.get(key);
            this.vertx.executeBlocking(f -> { // simulate asynchrony of a GET, POST, etc.
                if (this.r.nextInt(2) < 1) {
                    f.complete();
                } else {
                    f.fail("");
                }
            }, res -> {
                if (res.succeeded()) {
                    System.out.printf("fs[%s] ok%n", key);
                    fM.complete(String.format("fs[%s] ok", key));
                } else {
                    System.out.printf("fs[%s] NOT ok%n", key);
                    fM.fail(String.format("fs[%s] NOT ok", key));
                }
            });
        }
    }

    /**
     * /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        Vertx v = Vertx.vertx(new VertxOptions());
        v.deployVerticle(FutureCompletionIssue.class, new DeploymentOptions(),
                dRes -> {
                    if (dRes.succeeded()) {
                        System.out
                                .println("Verticle deployed: " + dRes.result());
                    } else {
                        System.out.println(
                                "Verticle NOT deployed: " + dRes.cause());
                    }
                });
    }

}
