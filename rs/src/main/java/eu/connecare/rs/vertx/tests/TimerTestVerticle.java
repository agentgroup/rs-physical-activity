package eu.connecare.rs.vertx.tests;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;

public final class TimerTestVerticle extends AbstractVerticle {

	private HttpServer s;

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		s = vertx.createHttpServer().requestHandler(req -> {
			req.response().putHeader("content-type", "text/plain").end("Hello from Vert.x!");
		});
		s.listen(8080, lRes -> {
			if (lRes.succeeded()) {
				System.out.println("HTTP server started: " + lRes.result());
				startFuture.complete();
			} else {
				System.out.println("HTTP server NOT started: " + lRes.cause());
				startFuture.fail(lRes.cause());
			}
		});
		long timerID = vertx.setPeriodic(1000, id -> {
			System.out.println("Eventually every second this is printed");
		});
		System.out.println("First this is printed");
		vertx.setTimer(3000, id -> {
			vertx.cancelTimer(timerID);
			System.out.println("And finally this is printed");
		});
		System.out.println("Then this is printed");
	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception {
		// stopFuture.complete();
		s.close(cRes -> {
			if (cRes.succeeded()) {
				System.out.println("HTTP server stopped: " + cRes.result());
				stopFuture.complete();
			} else {
				System.out.println("HTTP server NOT stopped: " + cRes.cause());
				stopFuture.fail(cRes.cause());
			}
		});
	}

}
