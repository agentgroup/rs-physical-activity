package eu.connecare.rs.vertx.tests;

import java.nio.file.Paths;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpServer;

public final class BlockingVerticle extends AbstractVerticle {

	private HttpServer s;

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		s = vertx.createHttpServer().requestHandler(req -> {
			req.response().putHeader("content-type", "text/plain").end("Hello from Vert.x!");
		});
		s.listen(8080, lRes -> {
			if (lRes.succeeded()) {
				System.out.println("HTTP server started: " + lRes.result());
				startFuture.complete();
			} else {
				System.out.println("HTTP server NOT started: " + lRes.cause());
				startFuture.fail(lRes.cause());
			}
		});

		FileSystem fs = vertx.fileSystem();
		Future<Void> allGood = Future.future();
		Future<Void> fsC = Future.future(); // dir creation
		Future<Void> fsf1 = Future.future(); // file1 creation
		Future<Void> fsf11 = Future.future(); // file1 write
		Future<Void> fsf2 = Future.future(); // file2 creation
		Future<Void> fsf22 = Future.future(); // file2 write

		vertx.executeBlocking(future -> { // dir creation
			fs.mkdir("test", res -> {
				System.out.println("Dir 'test' creation ok: " + res.succeeded());
				if (res.succeeded()) {
					future.complete();
				} else {
					future.fail(res.cause());
					System.out.println("\t failure: " + res.cause());
				}
			});
		}, res -> {
			System.out.println("Blocking 'mkdir' ok: " + res.succeeded());
			if (res.succeeded()) {
				fsC.complete();
			} else {
				fsC.fail(res.cause());
				System.out.println("\t failure: " + res.cause());
			}
		});

		fsC.compose(v -> {
			vertx.executeBlocking(f -> { // file 'foo1' creation
				fs.createFile(Paths.get("test", "foo1").toString(), res -> {
					System.out.println("File 'foo1' creation ok: " + res.succeeded());
					if (res.succeeded()) {
						f.complete();
					} else {
						f.fail(res.cause());
						System.out.println("\t failure: " + res.cause());
					}
				});
			}, res -> {
				System.out.println("Blocking 'createFile' ok: " + res.succeeded());
				if (res.succeeded()) {
					fsf1.complete();
				} else {
					fsf1.fail(res.cause());
					System.out.println("\t failure: " + res.cause());
				}
			});
		}, fsf1).compose(v -> { // file write AFTER file creation
			vertx.executeBlocking(f -> { // string 'foo1' write
				fs.writeFile(Paths.get("test", "foo1").toString(), Buffer.buffer().appendString("foo1"), resW -> {
					System.out.println("String 'foo1' write ok: " + resW.succeeded());
					if (resW.succeeded()) {
						f.complete();
					} else {
						f.fail(resW.cause());
						System.out.println("\t failure: " + resW.cause());
					}
				});
			}, res -> {
				System.out.println("Blocking 'writeFile' ok: " + res.succeeded());
				if (res.succeeded()) {
					fsf11.complete();
				} else {
					fsf11.fail(res.cause());
					System.out.println("\t failure: " + res.cause());
				}
			});
		}, fsf11);

		fsC.compose(v -> {
			vertx.executeBlocking(f -> { // file 'foo2' creation
				fs.createFile(Paths.get("test", "foo2").toString(), res -> {
					System.out.println("File 'foo2' creation ok: " + res.succeeded());
					if (res.succeeded()) {
						f.complete();
					} else {
						f.fail(res.cause());
						System.out.println("\t failure: " + res.cause());
					}
				});
			}, res -> {
				System.out.println("Blocking 'createFile' ok: " + res.succeeded());
				if (res.succeeded()) {
					fsf2.complete();
				} else {
					fsf2.fail(res.cause());
					System.out.println("\t failure: " + res.cause());
				}
			});
		}, fsf2).compose(v -> { // file write AFTER file creation
			vertx.executeBlocking(f -> { // string 'foo2' write
				fs.writeFile(Paths.get("test", "foo2").toString(), Buffer.buffer().appendString("foo2"), resW -> {
					System.out.println("String 'foo2' write ok: " + resW.succeeded());
					if (resW.succeeded()) {
						f.complete();
					} else {
						f.fail(resW.cause());
						System.out.println("\t failure: " + resW.cause());
					}
				});
			}, res -> {
				System.out.println("Blocking 'writeFile' ok: " + res.succeeded());
				if (res.succeeded()) {
					fsf22.complete();
				} else {
					fsf22.fail(res.cause());
					System.out.println("\t failure: " + res.cause());
				}
			});
		}, fsf22);

		CompositeFuture.all(fsf1, fsf2, fsf11, fsf22).setHandler(all -> {
			System.out.println("All composite result: " + allGood.result());
			if (all.succeeded()) {
				allGood.complete();
			} else {
				allGood.fail(all.cause());
				System.out.println("\t failure: " + allGood.cause());
			}
		});

	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception {
		s.close(cRes -> {
			if (cRes.succeeded()) {
				System.out.println("HTTP server stopped: " + cRes.result());
				stopFuture.complete();
			} else {
				System.out.println("HTTP server NOT stopped: " + cRes.cause());
				stopFuture.fail(cRes.cause());
			}
		});
	}

}
