/**
 * Created by sm (stefano.mariani@unimore.it) on Jul 19, 2018 4:22:46 PM
 */
package eu.connecare.rs.vertx.tests;

import java.time.Duration;
import java.util.logging.Logger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;

/**
 * @author sm (stefano.mariani@unimore.it)
 *
 */
public final class JsonMapTest extends AbstractVerticle {

    public class DummyObject {
        private final String bar;
        private final Duration foo;

        /**
         * 
         */
        public DummyObject(String bar, Duration foo) {
            this.bar = bar;
            this.foo = foo;
        }

        /**
         * @return the bar
         */
        public String getBar() {
            return this.bar;
        }

        /**
         * @return the foo
         */
        public Duration getFoo() {
            return this.foo;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        JsonObject json = JsonObject
                .mapFrom(new DummyObject("bar", Duration.ofHours(1)));
        Logger.getAnonymousLogger()
                .info(String.format("json: %s", json.toString()));
        Duration d = json.getJsonObject("foo").mapTo(java.time.Duration.class);
        Logger.getAnonymousLogger().info(String.format("d: %s", d));
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        Vertx v = Vertx.vertx(new VertxOptions());
        v.deployVerticle(JsonMapTest.class, new DeploymentOptions(), dRes -> {
            if (dRes.succeeded()) {
                System.out.println("Verticle deployed: " + dRes.result());
            } else {
                System.out.println("Verticle NOT deployed: " + dRes.cause());
                dRes.cause().printStackTrace();
            }
        });
    }

}
