package eu.connecare.rs.vertx.tests;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class LauncherVerticle extends AbstractVerticle {

    public static void main(String[] args) {
        Vertx v = Vertx.vertx(new VertxOptions());
        // change verticle to launch here
        v.deployVerticle(FutureCompletionIssue.class, new DeploymentOptions(),
                dRes -> {
                    if (dRes.succeeded()) {
                        System.out
                                .println("Verticle deployed: " + dRes.result());
                        // v.setTimer(5000, id -> {
                        // v.undeploy(dRes.result(), uRes -> {
                        // if (uRes.succeeded()) {
                        // System.out.println("Verticle undeployed: " + uRes.result());
                        // v.close(cRes -> {
                        // if (cRes.succeeded()) {
                        // System.out.println("Verticle closed: " + cRes.result());
                        // } else {
                        // System.out.println("Verticle NOT closed: " + cRes.cause());
                        // }
                        // });
                        // } else {
                        // System.out.println("Verticle NOT undeployed: " + uRes.cause());
                        // }
                        // });
                        // });
                    } else {
                        System.out.println(
                                "Verticle NOT deployed: " + dRes.cause());
                    }
                });
    }

}
